
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-25T11:46:40+02:00
Command line: --no-dump --no-comp BIOMD0000000221 --cvc4 --pp
Flags: ppone ppsmt pp2x2 no-dump justone maxadd=20 solver=cvc4


Model: BIOMD0000000221
Logfile: db/BIOMD0000000221/ep11-osx20-cvc4-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^8.7 in 8 bags
Bag sizes: 3 45 16 8 44 5 6 23

Filtering bags...
Collecting...
Bag 1/8, 3 polyhedra
Bag 2/8, 45 polyhedra
Bag 3/8, 16 polyhedra
Bag 4/8, 8 polyhedra
Bag 5/8, 44 polyhedra
Bag 6/8, 5 polyhedra
Bag 7/8, 6 polyhedra
Bag 8/8, 23 polyhedra
Possible combinations after preprocessing: 10^8.7 in 9 bags
Bag sizes: 1 3 45 16 8 44 5 6 23
Time: 0.102 sec

Checking for superfluous polyhedra...
Bag 1/9, 45 polyhedra:
  Polyhedron 1... required, 0.030 sec
  Polyhedron 2... superfluous, 0.008 sec
  Polyhedron 3... required, 0.010 sec
  Polyhedron 4... superfluous, 0.006 sec
  Polyhedron 5... superfluous, 0.010 sec
  Polyhedron 6... superfluous, 0.007 sec
  Polyhedron 7... superfluous, 0.008 sec
  Polyhedron 8... superfluous, 0.010 sec
  Polyhedron 9... superfluous, 0.007 sec
  Polyhedron 10... superfluous, 0.008 sec
  Polyhedron 11... required, 0.011 sec
  Polyhedron 12... required, 0.016 sec
  Polyhedron 13... superfluous, 0.006 sec
  Polyhedron 14... required, 0.008 sec
  Polyhedron 15... required, 0.013 sec
  Polyhedron 16... required, 0.010 sec
  Polyhedron 17... superfluous, 0.007 sec
  Polyhedron 18... superfluous, 0.013 sec
  Polyhedron 19... superfluous, 0.011 sec
  Polyhedron 20... superfluous, 0.006 sec
  Polyhedron 21... superfluous, 0.006 sec
  Polyhedron 22... superfluous, 0.018 sec
  Polyhedron 23... superfluous, 0.006 sec
  Polyhedron 24... superfluous, 0.007 sec
  Polyhedron 25... superfluous, 0.008 sec
  Polyhedron 26... superfluous, 0.007 sec
  Polyhedron 27... superfluous, 0.006 sec
  Polyhedron 28... superfluous, 0.008 sec
  Polyhedron 29... superfluous, 0.006 sec
  Polyhedron 30... superfluous, 0.007 sec
  Polyhedron 31... superfluous, 0.012 sec
  Polyhedron 32... superfluous, 0.013 sec
  Polyhedron 33... superfluous, 0.006 sec
  Polyhedron 34... superfluous, 0.006 sec
  Polyhedron 35... superfluous, 0.006 sec
  Polyhedron 36... superfluous, 0.005 sec
  Polyhedron 37... superfluous, 0.011 sec
  Polyhedron 38... superfluous, 0.006 sec
  Polyhedron 39... superfluous, 0.011 sec
  Polyhedron 40... superfluous, 0.006 sec
  Polyhedron 41... superfluous, 0.017 sec
  Polyhedron 42... superfluous, 0.009 sec
  Polyhedron 43... superfluous, 0.009 sec
  Polyhedron 44... superfluous, 0.005 sec
  Polyhedron 45... superfluous, 0.005 sec
  => 7 polyhedra left
Bag 2/9, 44 polyhedra:
  Polyhedron 1... required, 0.011 sec
  Polyhedron 2... required, 0.010 sec
  Polyhedron 3... required, 0.016 sec
  Polyhedron 4... required, 0.020 sec
  Polyhedron 5... required, 0.012 sec
  Polyhedron 6... superfluous, 0.014 sec
  Polyhedron 7... required, 0.012 sec
  Polyhedron 8... required, 0.025 sec
  Polyhedron 9... required, 0.017 sec
  Polyhedron 10... superfluous, 0.009 sec
  Polyhedron 11... superfluous, 0.012 sec
  Polyhedron 12... required, 0.015 sec
  Polyhedron 13... superfluous, 0.008 sec
  Polyhedron 14... required, 0.015 sec
  Polyhedron 15... required, 0.010 sec
  Polyhedron 16... superfluous, 0.005 sec
  Polyhedron 17... required, 0.017 sec
  Polyhedron 18... required, 0.014 sec
  Polyhedron 19... superfluous, 0.006 sec
  Polyhedron 20... required, 0.025 sec
  Polyhedron 21... required, 0.011 sec
  Polyhedron 22... required, 0.017 sec
  Polyhedron 23... required, 0.014 sec
  Polyhedron 24... superfluous, 0.007 sec
  Polyhedron 25... required, 0.017 sec
  Polyhedron 26... superfluous, 0.006 sec
  Polyhedron 27... required, 0.013 sec
  Polyhedron 28... superfluous, 0.018 sec
  Polyhedron 29... superfluous, 0.020 sec
  Polyhedron 30... superfluous, 0.018 sec
  Polyhedron 31... superfluous, 0.021 sec
  Polyhedron 32... superfluous, 0.007 sec
  Polyhedron 33... required, 0.011 sec
  Polyhedron 34... superfluous, 0.005 sec
  Polyhedron 35... required, 0.016 sec
  Polyhedron 36... superfluous, 0.018 sec
  Polyhedron 37... superfluous, 0.007 sec
  Polyhedron 38... superfluous, 0.008 sec
  Polyhedron 39... superfluous, 0.007 sec
  Polyhedron 40... superfluous, 0.005 sec
  Polyhedron 41... superfluous, 0.006 sec
  Polyhedron 42... superfluous, 0.008 sec
  Polyhedron 43... superfluous, 0.005 sec
  Polyhedron 44... superfluous, 0.006 sec
  => 21 polyhedra left
Bag 3/9, 23 polyhedra:
  Polyhedron 1... required, 0.035 sec
  Polyhedron 2... required, 0.016 sec
  Polyhedron 3... superfluous, 0.009 sec
  Polyhedron 4... superfluous, 0.008 sec
  Polyhedron 5... required, 0.016 sec
  Polyhedron 6... superfluous, 0.008 sec
  Polyhedron 7... required, 0.016 sec
  Polyhedron 8... superfluous, 0.009 sec
  Polyhedron 9... superfluous, 0.004 sec
  Polyhedron 10... required, 0.020 sec
  Polyhedron 11... superfluous, 0.005 sec
  Polyhedron 12... superfluous, 0.008 sec
  Polyhedron 13... required, 0.012 sec
  Polyhedron 14... required, 0.018 sec
  Polyhedron 15... superfluous, 0.006 sec
  Polyhedron 16... required, 0.020 sec
  Polyhedron 17... superfluous, 0.011 sec
  Polyhedron 18... superfluous, 0.006 sec
  Polyhedron 19... required, 0.012 sec
  Polyhedron 20... superfluous, 0.007 sec
  Polyhedron 21... superfluous, 0.008 sec
  Polyhedron 22... superfluous, 0.006 sec
  Polyhedron 23... superfluous, 0.011 sec
  => 9 polyhedra left
Bag 4/9, 16 polyhedra:
  Polyhedron 1... required, 0.026 sec
  Polyhedron 2... required, 0.012 sec
  Polyhedron 3... superfluous, 0.005 sec
  Polyhedron 4... superfluous, 0.006 sec
  Polyhedron 5... superfluous, 0.005 sec
  Polyhedron 6... required, 0.023 sec
  Polyhedron 7... superfluous, 0.010 sec
  Polyhedron 8... superfluous, 0.007 sec
  Polyhedron 9... superfluous, 0.006 sec
  Polyhedron 10... superfluous, 0.006 sec
  Polyhedron 11... superfluous, 0.012 sec
  Polyhedron 12... superfluous, 0.008 sec
  Polyhedron 13... superfluous, 0.007 sec
  Polyhedron 14... superfluous, 0.004 sec
  Polyhedron 15... superfluous, 0.006 sec
  Polyhedron 16... superfluous, 0.004 sec
  => 3 polyhedra left
Bag 5/9, 8 polyhedra:
  Polyhedron 1... required, 0.019 sec
  Polyhedron 2... required, 0.013 sec
  Polyhedron 3... required, 0.007 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... required, 0.009 sec
  Polyhedron 6... superfluous, 0.009 sec
  Polyhedron 7... superfluous, 0.003 sec
  Polyhedron 8... superfluous, 0.003 sec
  => 4 polyhedra left
Bag 6/9, 6 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... superfluous, 0.003 sec
  Polyhedron 3... required, 0.021 sec
  Polyhedron 4... superfluous, 0.003 sec
  Polyhedron 5... required, 0.008 sec
  Polyhedron 6... superfluous, 0.003 sec
  => 2 polyhedra left
Bag 7/9, 5 polyhedra:
  Polyhedron 1... required, 0.015 sec
  Polyhedron 2... superfluous, 0.003 sec
  Polyhedron 3... required, 0.011 sec
  Polyhedron 4... superfluous, 0.004 sec
  Polyhedron 5... superfluous, 0.003 sec
  => 2 polyhedra left
Bag 8/9, 3 polyhedra:
  Polyhedron 1... required, 0.013 sec
  Polyhedron 2... required, 0.011 sec
  Polyhedron 3... required, 0.009 sec
  => 3 polyhedra left
Possible combinations after preprocessing: 10^5.3 in 9 bags
Bag sizes: 7 21 9 3 4 2 2 3 1
Time: 1.584 sec

Combining small bags...
Intersecting 2x21... 32 polyhedra
Intersecting 2x9... 13 polyhedra
Intersecting 3x7... 18 polyhedra
Intersecting 3x4... 12 polyhedra
Possible combinations after preprocessing: 10^5.0 in 5 bags
Bag sizes: 1 12 13 18 32
Time: 0.237 sec

Checking for superfluous polyhedra...
Bag 1/5, 32 polyhedra:
  Polyhedron 1... required, 0.013 sec
  Polyhedron 2... required, 0.009 sec
  Polyhedron 3... superfluous, 0.005 sec
  Polyhedron 4... required, 0.008 sec
  Polyhedron 5... required, 0.006 sec
  Polyhedron 6... required, 0.004 sec
  Polyhedron 7... required, 0.005 sec
  Polyhedron 8... required, 0.006 sec
  Polyhedron 9... required, 0.005 sec
  Polyhedron 10... required, 0.005 sec
  Polyhedron 11... required, 0.004 sec
  Polyhedron 12... required, 0.007 sec
  Polyhedron 13... required, 0.005 sec
  Polyhedron 14... required, 0.006 sec
  Polyhedron 15... superfluous, 0.003 sec
  Polyhedron 16... superfluous, 0.004 sec
  Polyhedron 17... superfluous, 0.003 sec
  Polyhedron 18... required, 0.004 sec
  Polyhedron 19... required, 0.004 sec
  Polyhedron 20... required, 0.005 sec
  Polyhedron 21... required, 0.007 sec
  Polyhedron 22... superfluous, 0.003 sec
  Polyhedron 23... required, 0.004 sec
  Polyhedron 24... superfluous, 0.004 sec
  Polyhedron 25... superfluous, 0.003 sec
  Polyhedron 26... superfluous, 0.003 sec
  Polyhedron 27... superfluous, 0.004 sec
  Polyhedron 28... required, 0.005 sec
  Polyhedron 29... required, 0.007 sec
  Polyhedron 30... required, 0.004 sec
  Polyhedron 31... superfluous, 0.004 sec
  Polyhedron 32... superfluous, 0.004 sec
  => 21 polyhedra left
Bag 2/5, 18 polyhedra:
  Polyhedron 1... required, 0.007 sec
  Polyhedron 2... required, 0.004 sec
  Polyhedron 3... required, 0.005 sec
  Polyhedron 4... required, 0.004 sec
  Polyhedron 5... required, 0.007 sec
  Polyhedron 6... required, 0.006 sec
  Polyhedron 7... required, 0.004 sec
  Polyhedron 8... required, 0.006 sec
  Polyhedron 9... superfluous, 0.003 sec
  Polyhedron 10... required, 0.005 sec
  Polyhedron 11... required, 0.004 sec
  Polyhedron 12... required, 0.005 sec
  Polyhedron 13... required, 0.004 sec
  Polyhedron 14... required, 0.004 sec
  Polyhedron 15... superfluous, 0.004 sec
  Polyhedron 16... superfluous, 0.002 sec
  Polyhedron 17... required, 0.003 sec
  Polyhedron 18... superfluous, 0.003 sec
  => 14 polyhedra left
Bag 3/5, 13 polyhedra:
  Polyhedron 1... required, 0.007 sec
  Polyhedron 2... required, 0.005 sec
  Polyhedron 3... required, 0.007 sec
  Polyhedron 4... required, 0.021 sec
  Polyhedron 5... required, 0.008 sec
  Polyhedron 6... required, 0.005 sec
  Polyhedron 7... required, 0.006 sec
  Polyhedron 8... required, 0.004 sec
  Polyhedron 9... required, 0.007 sec
  Polyhedron 10... superfluous, 0.003 sec
  Polyhedron 11... superfluous, 0.004 sec
  Polyhedron 12... required, 0.005 sec
  Polyhedron 13... superfluous, 0.003 sec
  => 10 polyhedra left
Bag 4/5, 12 polyhedra:
  Polyhedron 1... required, 0.006 sec
  Polyhedron 2... required, 0.005 sec
  Polyhedron 3... required, 0.005 sec
  Polyhedron 4... required, 0.004 sec
  Polyhedron 5... required, 0.004 sec
  Polyhedron 6... required, 0.005 sec
  Polyhedron 7... superfluous, 0.004 sec
  Polyhedron 8... superfluous, 0.003 sec
  Polyhedron 9... required, 0.004 sec
  Polyhedron 10... required, 0.005 sec
  Polyhedron 11... superfluous, 0.003 sec
  Polyhedron 12... superfluous, 0.003 sec
  => 8 polyhedra left
Possible combinations after preprocessing: 10^4.4 in 5 bags
Bag sizes: 21 14 10 8 1
Time: 0.395 sec

Minimizing (0,0)... (0,0)


Running SMT solver (1)... 0.014 sec
Checking... Minimizing (5,17)... (5,9), 0.005 sec
Inserting... 0.000 sec
[1 ph, smt 0.014, isect 0.005, ins 0.000, tot 0.032]

Running SMT solver (2)... 0.002 sec
Checking... Minimizing (5,17)... (5,9), 0.006 sec
Inserting... 0.000 sec
[2 ph, smt 0.016, isect 0.011, ins 0.000, tot 0.043]

Running SMT solver (3)... 0.003 sec
Checking... Minimizing (5,16)... (5,8), 0.005 sec
Inserting... 0.000 sec
[3 ph, smt 0.019, isect 0.015, ins 0.000, tot 0.054]

Running SMT solver (4)... 0.002 sec
Checking... Minimizing (5,15)... (5,9), 0.004 sec
Inserting... 0.000 sec
[4 ph, smt 0.021, isect 0.019, ins 0.000, tot 0.064]

Running SMT solver (5)... 0.002 sec
Checking... Minimizing (5,17)... (5,7), 0.005 sec
Inserting... 0.000 sec
[5 ph, smt 0.023, isect 0.024, ins 0.000, tot 0.074]

Running SMT solver (6)... 0.002 sec
Checking... Minimizing (5,15)... (5,9), 0.005 sec
Inserting... 0.000 sec
[6 ph, smt 0.025, isect 0.028, ins 0.000, tot 0.084]

Running SMT solver (7)... 0.002 sec
Checking... Minimizing (5,15)... (5,8), 0.004 sec
Inserting... 0.000 sec
[7 ph, smt 0.028, isect 0.032, ins 0.000, tot 0.093]

Running SMT solver (8)... 0.001 sec
Checking... Minimizing (5,16)... (5,8), 0.004 sec
Inserting... 0.002 sec
[8 ph, smt 0.029, isect 0.037, ins 0.002, tot 0.104]

Running SMT solver (9)... 0.002 sec
Checking... Minimizing (5,16)... (5,9), 0.004 sec
Inserting... 0.001 sec
[9 ph, smt 0.032, isect 0.041, ins 0.003, tot 0.115]

Running SMT solver (10)... 0.002 sec
Checking... Minimizing (5,17)... (5,8), 0.004 sec
Inserting... 0.001 sec
[10 ph, smt 0.034, isect 0.045, ins 0.004, tot 0.125]

Running SMT solver (11)... 0.002 sec
Checking... Minimizing (4,16)... (4,11), 0.005 sec
Inserting... 0.002 sec
[10 ph, smt 0.036, isect 0.050, ins 0.006, tot 0.138]

Running SMT solver (12)... 0.002 sec
Checking... Minimizing (4,16)... (4,11), 0.005 sec
Inserting... 0.000 sec
[11 ph, smt 0.037, isect 0.056, ins 0.006, tot 0.148]

Running SMT solver (13)... 0.002 sec
Checking... Minimizing (4,16)... (4,10), 0.005 sec
Inserting... 0.001 sec
[11 ph, smt 0.039, isect 0.060, ins 0.008, tot 0.159]

Running SMT solver (14)... 0.002 sec
Checking... Minimizing (4,16)... (4,10), 0.005 sec
Inserting... 0.000 sec
[12 ph, smt 0.040, isect 0.066, ins 0.008, tot 0.169]

Running SMT solver (15)... 0.003 sec
Checking... Minimizing (5,16)... (5,6), 0.004 sec
Inserting... 0.001 sec
[13 ph, smt 0.044, isect 0.070, ins 0.009, tot 0.180]

Running SMT solver (16)... 0.002 sec
Checking... Minimizing (5,15)... (5,11), 0.005 sec
Inserting... 0.001 sec
[14 ph, smt 0.045, isect 0.075, ins 0.010, tot 0.191]

Running SMT solver (17)... 0.002 sec
Checking... Minimizing (5,18)... (5,12), 0.006 sec
Inserting... 0.000 sec
[15 ph, smt 0.047, isect 0.081, ins 0.010, tot 0.202]

Running SMT solver (18)... 0.002 sec
Checking... Minimizing (5,17)... (5,8), 0.004 sec
Inserting... 0.000 sec
[16 ph, smt 0.049, isect 0.085, ins 0.010, tot 0.211]

Running SMT solver (19)... 0.001 sec
Checking... Minimizing (5,17)... (5,8), 0.004 sec
Inserting... 0.001 sec
[17 ph, smt 0.050, isect 0.089, ins 0.011, tot 0.220]

Running SMT solver (20)... 0.002 sec
Checking... Minimizing (5,18)... (5,8), 0.004 sec
Inserting... 0.000 sec
[18 ph, smt 0.052, isect 0.094, ins 0.011, tot 0.230]

Running SMT solver (21)... 0.001 sec
Checking... Minimizing (5,18)... (5,8), 0.004 sec
Inserting... 0.000 sec
[19 ph, smt 0.053, isect 0.098, ins 0.011, tot 0.239]

Running SMT solver (22)... 0.001 sec
Checking... Minimizing (4,16)... (4,11), 0.005 sec
Inserting... 0.001 sec
[19 ph, smt 0.054, isect 0.103, ins 0.012, tot 0.249]

Running SMT solver (23)... 0.002 sec
Checking... Minimizing (4,16)... (4,11), 0.005 sec
Inserting... 0.001 sec
[19 ph, smt 0.056, isect 0.107, ins 0.013, tot 0.259]

Running SMT solver (24)... 0.002 sec
Checking... Minimizing (5,17)... (5,9), 0.004 sec
Inserting... 0.000 sec
[20 ph, smt 0.058, isect 0.112, ins 0.014, tot 0.268]

Running SMT solver (25)... 0.001 sec
Checking... Minimizing (5,17)... (5,9), 0.005 sec
Inserting... 0.001 sec
[21 ph, smt 0.059, isect 0.116, ins 0.015, tot 0.278]

Running SMT solver (26)... 0.003 sec
Checking... Minimizing (5,15)... (5,10), 0.005 sec
Inserting... 0.000 sec
[22 ph, smt 0.062, isect 0.121, ins 0.015, tot 0.289]

Running SMT solver (27)... 0.002 sec
Checking... Minimizing (5,16)... (5,10), 0.005 sec
Inserting... 0.000 sec
[23 ph, smt 0.064, isect 0.126, ins 0.015, tot 0.299]

Running SMT solver (28)... 0.002 sec
Checking... Minimizing (5,15)... (5,7), 0.004 sec
Inserting... 0.000 sec
[24 ph, smt 0.065, isect 0.129, ins 0.015, tot 0.308]

Running SMT solver (29)... 0.002 sec
Checking... Minimizing (5,16)... (5,8), 0.004 sec
Inserting... 0.000 sec
[25 ph, smt 0.067, isect 0.133, ins 0.015, tot 0.317]

Running SMT solver (30)... 0.002 sec
Checking... Minimizing (5,16)... (5,9), 0.005 sec
Inserting... 0.000 sec
[26 ph, smt 0.069, isect 0.138, ins 0.016, tot 0.327]

Running SMT solver (31)... 0.002 sec
Checking... Minimizing (6,17)... (6,7), 0.004 sec
Inserting... 0.000 sec
[27 ph, smt 0.071, isect 0.142, ins 0.016, tot 0.337]

Running SMT solver (32)... 0.002 sec
Checking... Minimizing (6,18)... (6,6), 0.005 sec
Inserting... 0.000 sec
[28 ph, smt 0.073, isect 0.147, ins 0.016, tot 0.347]

Running SMT solver (33)... 0.002 sec
Checking... Minimizing (6,19)... (6,5), 0.004 sec
Inserting... 0.000 sec
[29 ph, smt 0.075, isect 0.151, ins 0.016, tot 0.356]

Running SMT solver (34)... 0.003 sec
Checking... Minimizing (5,16)... (5,10), 0.005 sec
Inserting... 0.000 sec
[30 ph, smt 0.077, isect 0.156, ins 0.016, tot 0.367]

Running SMT solver (35)... 0.002 sec
Checking... Minimizing (5,17)... (5,9), 0.005 sec
Inserting... 0.000 sec
[31 ph, smt 0.079, isect 0.161, ins 0.017, tot 0.377]

Running SMT solver (36)... 0.002 sec
Checking... Minimizing (5,16)... (5,11), 0.006 sec
Inserting... 0.001 sec
[32 ph, smt 0.081, isect 0.166, ins 0.018, tot 0.392]

Running SMT solver (37)... 0.002 sec
Checking... Minimizing (5,18)... (5,10), 0.005 sec
Inserting... 0.000 sec
[33 ph, smt 0.083, isect 0.172, ins 0.018, tot 0.402]

Running SMT solver (38)... 0.002 sec
Checking... Minimizing (5,19)... (5,10), 0.005 sec
Inserting... 0.000 sec
[34 ph, smt 0.084, isect 0.177, ins 0.019, tot 0.413]

Running SMT solver (39)... 0.001 sec
Checking... Minimizing (5,19)... (5,8), 0.005 sec
Inserting... 0.000 sec
[35 ph, smt 0.086, isect 0.181, ins 0.019, tot 0.422]

Running SMT solver (40)... 0.001 sec
Checking... Minimizing (5,18)... (5,7), 0.004 sec
Inserting... 0.000 sec
[36 ph, smt 0.087, isect 0.186, ins 0.019, tot 0.432]

Running SMT solver (41)... 0.001 sec
Checking... Minimizing (4,16)... (4,10), 0.005 sec
Inserting... 0.001 sec
[36 ph, smt 0.088, isect 0.191, ins 0.020, tot 0.442]

Running SMT solver (42)... 0.001 sec
Checking... Minimizing (4,16)... (4,10), 0.005 sec
Inserting... 0.000 sec
[37 ph, smt 0.090, isect 0.195, ins 0.020, tot 0.451]

Running SMT solver (43)... 0.002 sec
Checking... Minimizing (5,17)... (5,7), 0.005 sec
Inserting... 0.000 sec
[38 ph, smt 0.091, isect 0.201, ins 0.021, tot 0.461]

Running SMT solver (44)... 0.002 sec
Checking... Minimizing (5,18)... (5,8), 0.005 sec
Inserting... 0.000 sec
[39 ph, smt 0.093, isect 0.206, ins 0.021, tot 0.472]

Running SMT solver (45)... 0.001 sec
Checking... Minimizing (5,18)... (5,8), 0.005 sec
Inserting... 0.001 sec
[40 ph, smt 0.095, isect 0.211, ins 0.022, tot 0.483]

Running SMT solver (46)... 0.001 sec
Checking... Minimizing (5,17)... (5,7), 0.005 sec
Inserting... 0.000 sec
[41 ph, smt 0.096, isect 0.217, ins 0.023, tot 0.493]

Running SMT solver (47)... 0.002 sec
Checking... Minimizing (5,17)... (5,7), 0.004 sec
Inserting... 0.002 sec
[42 ph, smt 0.097, isect 0.221, ins 0.025, tot 0.504]

Running SMT solver (48)... 0.001 sec
Checking... Minimizing (5,17)... (5,7), 0.004 sec
Inserting... 0.001 sec
[43 ph, smt 0.099, isect 0.225, ins 0.026, tot 0.513]

Running SMT solver (49)... 0.002 sec
Checking... Minimizing (5,17)... (5,8), 0.004 sec
Inserting... 0.000 sec
[44 ph, smt 0.100, isect 0.230, ins 0.026, tot 0.523]

Running SMT solver (50)... 0.002 sec
Checking... Minimizing (5,16)... (5,8), 0.005 sec
Inserting... 0.000 sec
[45 ph, smt 0.102, isect 0.234, ins 0.026, tot 0.533]

Running SMT solver (51)... 0.002 sec
Checking... Minimizing (5,18)... (5,7), 0.005 sec
Inserting... 0.001 sec
[46 ph, smt 0.104, isect 0.239, ins 0.028, tot 0.543]

Running SMT solver (52)... 0.001 sec
Checking... Minimizing (5,17)... (5,9), 0.005 sec
Inserting... 0.000 sec
[47 ph, smt 0.105, isect 0.244, ins 0.028, tot 0.553]

Running SMT solver (53)... 0.002 sec
Checking... Minimizing (5,16)... (5,8), 0.005 sec
Inserting... 0.001 sec
[48 ph, smt 0.107, isect 0.249, ins 0.029, tot 0.564]

Running SMT solver (54)... 0.002 sec
Checking... Minimizing (5,16)... (5,8), 0.004 sec
Inserting... 0.001 sec
[49 ph, smt 0.109, isect 0.253, ins 0.030, tot 0.575]

Running SMT solver (55)... 0.001 sec
Checking... Minimizing (5,16)... (5,9), 0.005 sec
Inserting... 0.000 sec
[50 ph, smt 0.111, isect 0.258, ins 0.031, tot 0.585]

Running SMT solver (56)... 0.001 sec
No more solutions found

50 polyhedra, 56 rounds, smt 0.112 sec, isect 0.258 sec, insert 0.031 sec, total 0.586 sec
Preprocessing 2.323 sec, super total 2.911 sec
