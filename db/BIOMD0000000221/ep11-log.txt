
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.4 (default, Aug  9 2019, 18:34:13) [MSC v.1915 64 bit (AMD64)]
numpy 1.16.5, sympy 1.4, gmpy2 2.1.0a5
It is now 2020-04-04T21:12:30+02:00
Command line: ..\trop\ptcut.py BIOMD0000000221 -e11 --rat --no-cache=n --maxruntime 10800 --maxmem 11513M --comment "concurrent=6, timeout=10800"

----------------------------------------------------------------------
Solving BIOMD0000000221 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=10800
Random seed: 3EXPMIHP3UWMS
Effective epsilon: 1/11.0

Reading db\BIOMD0000000221\vector_field_q.txt
Reading db\BIOMD0000000221\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 4 with: k11*k4*k7*k9 + k11*k4*k7*x4 + k11*k4*k9*x4 + k11*k4*x4**2 + k11*k52*k7*k9 + k11*k52*k7*x4 + k4*k7*k9*x5 + k4*k9*x4*x5 + k52*k7*k9*x5

Multiplying equation 5 with: k11*k13*k15*k37*k39*k40*k9 + k11*k13*k15*k37*k39*k40*x4 + k11*k13*k15*k37*k39*k9*x11 + k11*k13*k15*k37*k39*x11*x4 + k11*k13*k15*k37*k40*k9*x8 + k11*k13*k15*k37*k40*x4*x8 + k11*k13*k15*k37*k9*x11*x8 + k11*k13*k15*k37*x11*x4*x8 + k11*k13*k15*k39*k40*k9*x5 + k11*k13*k15*k39*k40*x4*x5 + k11*k13*k15*k40*k9*x5*x8 + k11*k13*k15*k40*x4*x5*x8 + k11*k13*k37*k39*k40*k9*x6 + k11*k13*k37*k39*k40*x4*x6 + k11*k13*k37*k39*k9*x11*x6 + k11*k13*k37*k39*x11*x4*x6 + k11*k13*k37*k40*k9*x6*x8 + k11*k13*k37*k40*x4*x6*x8 + k11*k13*k37*k9*x11*x6*x8 + k11*k13*k37*x11*x4*x6*x8 + k11*k13*k39*k40*k9*x5*x6 + k11*k13*k39*k40*x4*x5*x6 + k11*k13*k40*k9*x5*x6*x8 + k11*k13*k40*x4*x5*x6*x8 + k11*k15*k37*k39*k40*k9*x5 + k11*k15*k37*k39*k40*x4*x5 + k11*k15*k37*k39*k9*x11*x5 + k11*k15*k37*k39*x11*x4*x5 + k11*k15*k37*k40*k9*x5*x8 + k11*k15*k37*k40*x4*x5*x8 + k11*k15*k37*k9*x11*x5*x8 + k11*k15*k37*x11*x4*x5*x8 + k11*k15*k39*k40*k9*x5**2 + k11*k15*k39*k40*x4*x5**2 + k11*k15*k40*k9*x5**2*x8 + k11*k15*k40*x4*x5**2*x8 + k13*k15*k37*k39*k40*k9*x5 + k13*k15*k37*k39*k9*x11*x5 + k13*k15*k37*k40*k9*x5*x8 + k13*k15*k37*k9*x11*x5*x8 + k13*k15*k39*k40*k9*x5**2 + k13*k15*k40*k9*x5**2*x8 + k13*k37*k39*k40*k9*x5*x6 + k13*k37*k39*k9*x11*x5*x6 + k13*k37*k40*k9*x5*x6*x8 + k13*k37*k9*x11*x5*x6*x8 + k13*k39*k40*k9*x5**2*x6 + k13*k40*k9*x5**2*x6*x8 + k15*k37*k39*k40*k9*x5**2 + k15*k37*k39*k9*x11*x5**2 + k15*k37*k40*k9*x5**2*x8 + k15*k37*k9*x11*x5**2*x8 + k15*k39*k40*k9*x5**3 + k15*k40*k9*x5**3*x8

Multiplying equation 6 with: k13*k15*k17*k19*k48*k50 + k13*k15*k17*k19*k48*x6 + k13*k15*k17*k19*k50*x5 + k13*k15*k17*k48*k50*x7 + k13*k15*k17*k48*x6*x7 + k13*k15*k17*k50*x5*x7 + k13*k15*k19*k48*k50*x6 + k13*k15*k19*k48*x6**2 + k13*k15*k19*k50*x5*x6 + k13*k17*k19*k48*k50*x6 + k13*k17*k19*k48*x6**2 + k13*k17*k19*k50*x5*x6 + k13*k17*k48*k50*x6*x7 + k13*k17*k48*x6**2*x7 + k13*k17*k50*x5*x6*x7 + k13*k19*k48*k50*x6**2 + k13*k19*k48*x6**3 + k13*k19*k50*x5*x6**2 + k15*k17*k19*k48*k50*x5 + k15*k17*k19*k48*x5*x6 + k15*k17*k19*k50*x5**2 + k15*k17*k48*k50*x5*x7 + k15*k17*k48*x5*x6*x7 + k15*k17*k50*x5**2*x7 + k15*k19*k48*k50*x5*x6 + k15*k19*k48*x5*x6**2 + k15*k19*k50*x5**2*x6

Multiplying equation 7 with: k17*k19*k21*k23 + k17*k19*k21*x8 + k17*k19*k23*x7 + k17*k21*k23*x7 + k17*k21*x7*x8 + k17*k23*x7**2 + k19*k21*k23*x6 + k19*k21*x6*x8 + k19*k23*x6*x7

Multiplying equation 8 with: k21*k23*k25*k27*k37*k39*k40 + k21*k23*k25*k27*k37*k39*x11 + k21*k23*k25*k27*k37*k40*x8 + k21*k23*k25*k27*k37*x11*x8 + k21*k23*k25*k27*k39*k40*x5 + k21*k23*k25*k27*k40*x5*x8 + k21*k23*k25*k37*k39*k40*x9 + k21*k23*k25*k37*k39*x11*x9 + k21*k23*k25*k37*k40*x8*x9 + k21*k23*k25*k37*x11*x8*x9 + k21*k23*k25*k39*k40*x5*x9 + k21*k23*k25*k40*x5*x8*x9 + k21*k23*k27*k37*k39*k40*x8 + k21*k23*k27*k37*k39*x11*x8 + k21*k23*k27*k37*k40*x8**2 + k21*k23*k27*k37*x11*x8**2 + k21*k23*k27*k39*k40*x5*x8 + k21*k23*k27*k40*x5*x8**2 + k21*k25*k27*k37*k39*k40*x8 + k21*k25*k27*k37*k39*x11*x8 + k21*k25*k27*k37*k40*x8**2 + k21*k25*k27*k37*x11*x8**2 + k21*k25*k27*k39*k40*x5*x8 + k21*k25*k27*k40*x5*x8**2 + k21*k25*k37*k39*k40*x8*x9 + k21*k25*k37*k39*x11*x8*x9 + k21*k25*k37*k40*x8**2*x9 + k21*k25*k37*x11*x8**2*x9 + k21*k25*k39*k40*x5*x8*x9 + k21*k25*k40*x5*x8**2*x9 + k21*k27*k37*k39*k40*x8**2 + k21*k27*k37*k39*x11*x8**2 + k21*k27*k37*k40*x8**3 + k21*k27*k37*x11*x8**3 + k21*k27*k39*k40*x5*x8**2 + k21*k27*k40*x5*x8**3 + k23*k25*k27*k37*k39*k40*x7 + k23*k25*k27*k37*k39*x11*x7 + k23*k25*k27*k37*k40*x7*x8 + k23*k25*k27*k37*x11*x7*x8 + k23*k25*k27*k39*k40*x5*x7 + k23*k25*k27*k40*x5*x7*x8 + k23*k25*k37*k39*k40*x7*x9 + k23*k25*k37*k39*x11*x7*x9 + k23*k25*k37*k40*x7*x8*x9 + k23*k25*k37*x11*x7*x8*x9 + k23*k25*k39*k40*x5*x7*x9 + k23*k25*k40*x5*x7*x8*x9 + k23*k27*k37*k39*k40*x7*x8 + k23*k27*k37*k39*x11*x7*x8 + k23*k27*k37*k40*x7*x8**2 + k23*k27*k37*x11*x7*x8**2 + k23*k27*k39*k40*x5*x7*x8 + k23*k27*k40*x5*x7*x8**2

Multiplying equation 9 with: k25*k27*k29*k31 + k25*k27*k29*x10 + k25*k27*k31*x9 + k25*k29*k31*x9 + k25*k29*x10*x9 + k25*k31*x9**2 + k27*k29*k31*x8 + k27*k29*x10*x8 + k27*k31*x8*x9

Multiplying equation 10 with: k29*k31*k33*k35*k42*k45 + k29*k31*k33*k35*k42*x10 + k29*k31*k33*k35*k45*x11 + k29*k31*k33*k42*k45*k52 + k29*k31*k33*k42*k52*x10 + k29*k31*k33*k45*k52*x11 + k29*k31*k35*k42*k45*x10 + k29*k31*k35*k42*x10**2 + k29*k31*k35*k45*x10*x11 + k29*k33*k35*k42*k45*x10 + k29*k33*k35*k42*x10**2 + k29*k33*k35*k45*x10*x11 + k29*k33*k42*k45*k52*x10 + k29*k33*k42*k52*x10**2 + k29*k33*k45*k52*x10*x11 + k29*k35*k42*k45*x10**2 + k29*k35*k42*x10**3 + k29*k35*k45*x10**2*x11 + k31*k33*k35*k42*k45*x9 + k31*k33*k35*k42*x10*x9 + k31*k33*k35*k45*x11*x9 + k31*k33*k42*k45*k52*x9 + k31*k33*k42*k52*x10*x9 + k31*k33*k45*k52*x11*x9 + k31*k35*k42*k45*x10*x9 + k31*k35*k42*x10**2*x9 + k31*k35*k45*x10*x11*x9

Multiplying equation 11 with: k37*k39*k40*k42*k45 + k37*k39*k40*k42*x10 + k37*k39*k40*k45*x11 + k37*k39*k42*k45*x11 + k37*k39*k42*x10*x11 + k37*k39*k45*x11**2 + k37*k40*k42*k45*x8 + k37*k40*k42*x10*x8 + k37*k40*k45*x11*x8 + k37*k42*k45*x11*x8 + k37*k42*x10*x11*x8 + k37*k45*x11**2*x8 + k39*k40*k42*k45*x5 + k39*k40*k42*x10*x5 + k39*k40*k45*x11*x5 + k40*k42*k45*x5*x8 + k40*k42*x10*x5*x8 + k40*k45*x11*x5*x8

System is rational in variable x4,x5,x6,x7,x8,x9,x10,x11.
System is rational in parameter k3,k4,k6,k7,k9,k11,k13,k15,k17,k19,k21,k23,k25,k27,k29,k31,k33,k35,k37,k39,k40,k42,k43,k45,k46,k48,k50,k51,k52,k53.
System is radical in parameter k3,k6,k43,k46,k51,k53.
Tropicalization time: 83.178 sec
Variables: x4-x11
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.
Creation time: 19.032 sec
Saving polyhedra...

Sorting ascending...
Dimension: 8
Formulas: 8
Order: 0 5 6 3 2 7 4 1
Possible combinations: 3 * 5 * 6 * 8 * 16 * 23 * 44 * 45 = 524,620,800 (10^9)
 Applying common planes (codim=0, hs=1)...
 [1/8 (#0)]: 3 => 3 (-), dim=7.0, hs=2.3
 [2/8 (#5)]: 5 => 5 (-), dim=7.0, hs=4.4
 [3/8 (#6)]: 6 => 6 (-), dim=7.0, hs=4.3
 [4/8 (#3)]: 8 => 8 (-), dim=7.0, hs=5.0
 [5/8 (#2)]: 16 => 16 (-), dim=7.0, hs=4.6
 [6/8 (#7)]: 23 => 23 (-), dim=7.0, hs=6.4
 [7/8 (#4)]: 44 => 44 (-), dim=7.0, hs=6.6
 [8/8 (#1)]: 45 => 15 (16 empty, 14 incl), dim=7.0, hs=5.6
  Removed: 1.128,1.135,1.139,1.141,1.144,1.146,1.30,1.165,1.38,1.167,1.46,1.175,1.179,1.180,1.53,1.183,1.186,1.188,1.189,1.190,1.191,1.192,1.193,1.196,1.199,1.105,1.113,1.115,1.119,1.127
 Savings: factor 3.0, 0 formulas
[1/7 (#5, #0)]: 5 * 3 = 15 => 15 (-), dim=6.0, hs=5.7
[2/7 (w1, #6)]: 15 * 6 = 90 => 12 (30 empty, 48 incl), dim=6.0, hs=7.3
 Applying common planes (codim=1, hs=3)...
 [1/6 (w2)]: 12 => 12 (-), dim=6.0, hs=7.3
 [2/6 (#3)]: 8 => 8 (-), dim=6.0, hs=8.0
 [3/6 (#2)]: 16 => 16 (-), dim=6.0, hs=7.6
 [4/6 (#7)]: 23 => 11 (12 empty), dim=6.0, hs=7.7
  Removed: 7.2,7.3,7.7,7.9,7.14,7.20,7.22,7.23,7.26,7.27,7.28,7.29
 [5/6 (#4)]: 44 => 31 (13 empty), dim=6.0, hs=8.6
  Removed: 4.34,4.66,4.261,4.231,4.8,4.141,4.271,4.178,4.277,4.120,4.90,4.251,4.221
 [6/6 (#1)]: 15 => 15 (-), dim=6.0, hs=8.6
 Savings: factor 2.967741935483871, 0 formulas
[3/7 (w2, #3)]: 12 * 8 = 96 => 72 (24 empty), dim=5.0, hs=10.5
[4/7 (w3, #7)]: 72 * 11 = 792 => 120 (551 empty, 121 incl), dim=4.0, hs=10.8
[5/7 (w4, #1)]: 120 * 15 = 1800 => 124 (1267 empty, 409 incl), dim=4.0, hs=11.0
[6/7 (w5, #2)]: 124 * 16 = 1984 => 83 (1627 empty, 274 incl), dim=3.5, hs=10.8
[7/7 (w6, #4)]: 83 * 31 = 2573 => 50 (2219 empty, 304 incl), dim=3.1, hs=10.7
Total time: 2.150 sec, total intersections: 7,618, total inclusion tests: 25,685
Common plane time: 0.112 sec
Solutions: 50, dim=3.1, hs=10.7, max=124, maxin=2573
f-vector: [0, 0, 3, 39, 8]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.079 GiB, pagefile 0.073 GiB
Memory: pmem(rss=79134720, vms=73150464, num_page_faults=24451, peak_wset=84684800, wset=79134720, peak_paged_pool=538552, paged_pool=538376, peak_nonpaged_pool=39024, nonpaged_pool=39024, pagefile=73150464, peak_pagefile=78880768, private=73150464)
