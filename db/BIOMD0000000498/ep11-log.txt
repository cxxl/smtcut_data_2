
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:31:25+02:00
Command line: ..\trop\ptcut.py BIOMD0000000498 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000498 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 26OQA69MBUBEZ
Effective epsilon: 1/11.0

Reading db\BIOMD0000000498\vector_field_q.txt
Reading db\BIOMD0000000498\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k30**k29*k32 + k30**k29*x19 + k32*x18**k29 + x18**k29*x19

Multiplying equation 3 with: k57**k56 + x9**k56

Multiplying equation 4 with: x3 + x4

Multiplying equation 5 with: k67 + x6

Multiplying equation 6 with: k64*k70 + k64*x6 + k70*x6 + x6**2

Multiplying equation 7 with: k6**k5*k70*x3 + k6**k5*k70*x4 + k6**k5*x3*x6 + k6**k5*x4*x6 + k70*x3*x8**k5 + k70*x4*x8**k5 + x3*x6*x8**k5 + x4*x6*x8**k5

Multiplying equation 8 with: k13**k12*k20**k19 + k13**k12*x1**k19 + k20**k19*x9**k12 + x1**k19*x9**k12

Multiplying equation 9 with: k16**k15 + x7**k15

Multiplying equation 11 with: k9**k8 + x9**k8

System is rational in variable x1,x3,x4,x6,x7,x8,x9,x18,x19.
System is rational in parameter k2,k3,k5,k6,k8,k9,k12,k13,k15,k16,k19,k20,k25,k26,k29,k30,k32,k56,k57,k62,k64,k67,k70,k71,k72.
System is exponential in parameter k5,k8,k12,k15,k19,k25,k29,k56.
System is radical in parameter k2,k3,k25,k26,k62,k71,k72.
Term in equation 1 is zero since k27=0
Term in equation 1 is zero since k27=0
Term in equation 1 is zero since k27=0
Term in equation 1 is zero since k27=0
Tropicalization time: 22.320 sec
Variables: x1-x9, x11-x20
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.
Creation time: 12.231 sec
Saving polyhedra...

Sorting ascending...
Dimension: 19
Formulas: 19
Order: 1 14 15 16 17 3 4 5 8 10 13 7 12 0 2 18 11 9 6
Possible combinations: 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 2 * 4 * 4 * 5 * 8 * 8 * 15 * 16 * 100 = 7,864,320,000 (10^10)
 Applying common planes (codim=6, hs=10)...
 [1/14 (#3)]: 2 => 2 (-), dim=13.0, hs=11.0
 [2/14 (#4)]: 2 => 2 (-), dim=12.0, hs=11.0
 [3/14 (#5)]: 2 => 1 (1 empty), dim=12.0, hs=11.0
  Removed: 5.0
 [4/14 (#8)]: 2 => 2 (-), dim=12.0, hs=12.0
 [5/14 (#10)]: 2 => 1 (1 empty), dim=12.0, hs=10.0
  Removed: 10.1
 [6/14 (#13)]: 2 => 1 (1 empty), dim=12.0, hs=10.0
  Removed: 13.1
 [7/14 (#7)]: 4 => 2 (2 incl), dim=12.0, hs=12.0
  Removed: 7.0,7.2
 [8/14 (#12)]: 4 => 2 (2 incl), dim=12.5, hs=12.0
  Removed: 12.0,12.3
 [9/14 (#0)]: 5 => 5 (-), dim=12.0, hs=12.0
 [10/14 (#2)]: 8 => 4 (4 incl), dim=12.5, hs=13.0
  Removed: 2.9,2.2,2.4,2.7
 [11/14 (#18)]: 8 => 2 (2 empty, 4 incl), dim=12.5, hs=12.0
  Removed: 18.0,18.1,18.3,18.5,18.6,18.7
 [12/14 (#11)]: 15 => 5 (10 incl), dim=12.4, hs=14.6
  Removed: 11.0,11.1,11.3,11.5,11.6,11.7,11.8,11.10,11.12,11.13
 [13/14 (#9)]: 16 => 8 (4 empty, 4 incl), dim=12.0, hs=14.0
  Removed: 9.2,9.8,9.9,9.13,9.19,9.25,9.26,9.27
 [14/14 (#6)]: 100 => 44 (20 empty, 36 incl), dim=12.2, hs=17.2
  Removed: 6.0,6.2,6.130,6.131,6.6,6.7,6.392,6.396,6.269,6.145,6.273,6.401,6.405,6.408,6.412,6.158,6.417,6.37,6.421,6.39,6.168,6.169,6.44,6.45,6.434,6.307,6.436,6.311,6.191,6.195,6.72,6.328,6.457,6.459,6.206,6.207,6.84,6.346,6.474,6.92,6.93,6.350,6.351,6.479,6.355,6.228,6.235,6.492,6.495,6.240,6.373,6.118,6.247,6.121,6.378,6.379
 Applying common planes (codim=9, hs=12)...
 [1/11 (#3)]: 2 => 2 (-), dim=10.0, hs=13.0
 [2/11 (#4)]: 2 => 1 (1 incl), dim=9.0, hs=12.0
  Removed: 4.1
 [3/11 (#8)]: 2 => 2 (-), dim=9.0, hs=14.0
 [4/11 (#7)]: 2 => 2 (-), dim=9.0, hs=13.0
 [5/11 (#12)]: 2 => 2 (-), dim=9.5, hs=14.0
 [6/11 (#0)]: 5 => 5 (-), dim=9.0, hs=14.0
 [7/11 (#2)]: 4 => 4 (-), dim=9.5, hs=15.0
 [8/11 (#18)]: 2 => 2 (-), dim=10.0, hs=14.0
 [9/11 (#11)]: 5 => 5 (-), dim=9.4, hs=16.6
 [10/11 (#9)]: 8 => 6 (2 incl), dim=9.3, hs=16.0
  Removed: 9.0,9.17
 [11/11 (#6)]: 44 => 21 (23 empty), dim=9.2, hs=17.3
  Removed: 6.258,6.4,6.389,6.266,6.278,6.279,6.292,6.41,6.297,6.303,6.443,6.318,6.319,6.448,6.199,6.465,6.469,6.216,6.220,6.223,6.252,6.125,6.382
 Applying common planes (codim=10, hs=12)...
 [1/10 (#3)]: 2 => 2 (-), dim=9.0, hs=13.0
 [2/10 (#8)]: 2 => 2 (-), dim=8.0, hs=14.0
 [3/10 (#7)]: 2 => 2 (-), dim=8.0, hs=13.0
 [4/10 (#12)]: 2 => 2 (-), dim=8.5, hs=14.0
 [5/10 (#0)]: 5 => 5 (-), dim=8.0, hs=14.0
 [6/10 (#2)]: 4 => 4 (-), dim=8.5, hs=15.0
 [7/10 (#18)]: 2 => 2 (-), dim=9.0, hs=14.0
 [8/10 (#11)]: 5 => 5 (-), dim=8.4, hs=16.6
 [9/10 (#9)]: 6 => 6 (-), dim=8.3, hs=16.0
 [10/10 (#6)]: 21 => 18 (3 empty), dim=8.2, hs=17.0
  Removed: 6.503,6.501,6.391
 Savings: factor 22,755 (10^4), 9 formulas
[1/9 (#3, #8)]: 2 * 2 = 4 => 3 (1 empty), dim=8.0, hs=14.3
[2/9 (w1, #7)]: 3 * 2 = 6 => 3 (3 incl), dim=7.0, hs=14.3
 Applying common planes (codim=1, hs=1)...
 [1/8 (w2)]: 3 => 3 (-), dim=7.0, hs=14.3
 [2/8 (#12)]: 2 => 2 (-), dim=7.5, hs=14.0
 [3/8 (#0)]: 5 => 5 (-), dim=7.0, hs=14.0
 [4/8 (#2)]: 4 => 4 (-), dim=7.5, hs=15.0
 [5/8 (#18)]: 2 => 2 (-), dim=8.0, hs=14.0
 [6/8 (#11)]: 5 => 5 (-), dim=7.4, hs=16.6
 [7/8 (#9)]: 6 => 6 (-), dim=7.3, hs=16.0
 [8/8 (#6)]: 18 => 14 (3 empty, 1 incl), dim=7.3, hs=16.8
  Removed: 6.384,6.496,6.498,6.380
 Savings: factor 1.2857142857142858, 0 formulas
[3/9 (w2, #12)]: 3 * 2 = 6 => 6 (-), dim=6.5, hs=16.3
[4/9 (w3, #18)]: 6 * 2 = 12 => 9 (3 empty), dim=6.7, hs=17.7
[5/9 (w4, #2)]: 9 * 4 = 36 => 15 (21 empty), dim=6.5, hs=18.9
[6/9 (w5, #0)]: 15 * 5 = 75 => 55 (15 empty, 5 incl), dim=5.3, hs=19.7
[7/9 (w6, #11)]: 55 * 5 = 275 => 95 (75 empty, 105 incl), dim=5.3, hs=20.9
[8/9 (w7, #9)]: 95 * 6 = 570 => 125 (388 empty, 57 incl), dim=5.2, hs=21.8
[9/9 (w8, #6)]: 125 * 14 = 1750 => 214 (1381 empty, 155 incl), dim=5.0, hs=22.9
Total time: 3.331 sec, total intersections: 3,080, total inclusion tests: 31,967
Common plane time: 0.309 sec
Solutions: 214, dim=5.0, hs=22.9, max=214, maxin=1750
f-vector: [0, 0, 0, 0, 51, 119, 44]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.117 GiB, pagefile 0.112 GiB
Memory: pmem(rss=88723456, vms=81813504, num_page_faults=34580, peak_wset=125366272, wset=88723456, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=52336, nonpaged_pool=47304, pagefile=81813504, peak_pagefile=120270848, private=81813504)
