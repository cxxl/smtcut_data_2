
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.4 (default, Aug  9 2019, 18:34:13) [MSC v.1915 64 bit (AMD64)]
numpy 1.16.5, sympy 1.4, gmpy2 2.1.0a5
It is now 2020-04-04T22:04:21+02:00
Command line: ..\trop\ptcut.py BIOMD0000000477 -e11 --rat --no-cache=n --maxruntime 10800 --maxmem 11513M --comment "concurrent=6, timeout=10800"

----------------------------------------------------------------------
Solving BIOMD0000000477 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=10800
Random seed: 16BFGMFW2QX7U
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 43
Formulas: 37
Order: 0 1 12 13 14 15 6 7 9 11 18 20 21 23 25 26 27 30 31 32 33 34 35 2 3 10 16 22 24 4 5 17 19 28 29 8 36
Possible combinations: 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 3 * 3 * 3 * 3 * 3 * 3 * 9 * 9 * 9 * 9 * 9 * 9 * 23 * 43 = 50,221,398,572,531,712 (10^17)
 Applying common planes (codim=7, hs=43)...
 [1/31 (#6)]: 2 => 2 (-), dim=35.0, hs=43.5
 [2/31 (#7)]: 2 => 2 (-), dim=35.0, hs=43.5
 [3/31 (#9)]: 2 => 2 (-), dim=35.0, hs=43.5
 [4/31 (#11)]: 2 => 2 (-), dim=35.0, hs=43.5
 [5/31 (#18)]: 2 => 2 (-), dim=35.0, hs=43.5
 [6/31 (#20)]: 2 => 1 (1 incl), dim=36.0, hs=43.0
  Removed: 20.15
 [7/31 (#21)]: 2 => 1 (1 incl), dim=35.0, hs=43.0
  Removed: 21.1
 [8/31 (#23)]: 2 => 2 (-), dim=35.0, hs=43.5
 [9/31 (#25)]: 2 => 1 (1 incl), dim=35.0, hs=43.0
  Removed: 25.1
 [10/31 (#26)]: 2 => 2 (-), dim=35.0, hs=43.5
 [11/31 (#27)]: 2 => 2 (-), dim=35.0, hs=43.5
 [12/31 (#30)]: 2 => 1 (1 incl), dim=35.0, hs=43.0
  Removed: 30.1
 [13/31 (#31)]: 2 => 2 (-), dim=35.0, hs=43.5
 [14/31 (#32)]: 2 => 2 (-), dim=35.0, hs=43.5
 [15/31 (#33)]: 2 => 2 (-), dim=35.0, hs=43.5
 [16/31 (#34)]: 2 => 2 (-), dim=35.0, hs=43.5
 [17/31 (#35)]: 2 => 2 (-), dim=35.0, hs=43.5
 [18/31 (#2)]: 3 => 3 (-), dim=35.0, hs=43.7
 [19/31 (#3)]: 3 => 3 (-), dim=35.0, hs=43.7
 [20/31 (#10)]: 3 => 3 (-), dim=35.0, hs=43.7
 [21/31 (#16)]: 3 => 3 (-), dim=35.0, hs=43.7
 [22/31 (#22)]: 3 => 3 (-), dim=35.0, hs=43.7
 [23/31 (#24)]: 3 => 3 (-), dim=35.0, hs=43.7
 [24/31 (#4)]: 9 => 9 (-), dim=35.0, hs=44.1
 [25/31 (#5)]: 9 => 9 (-), dim=35.0, hs=44.1
 [26/31 (#17)]: 9 => 9 (-), dim=35.0, hs=44.3
 [27/31 (#19)]: 9 => 9 (-), dim=35.0, hs=44.3
 [28/31 (#28)]: 9 => 9 (-), dim=35.0, hs=44.3
 [29/31 (#29)]: 9 => 9 (-), dim=35.0, hs=44.3
 [30/31 (#8)]: 23 => 23 (-), dim=35.0, hs=44.8
 [31/31 (#36)]: 43 => 29 (14 empty), dim=35.0, hs=43.0
  Removed: 36.0,36.33,36.36,36.41,36.42,36.17,36.20,36.21,36.26,36.27,36.28,36.29,36.30,36.31
 Applying common planes (codim=10, hs=43)...
 [1/27 (#6)]: 2 => 2 (-), dim=32.0, hs=43.5
 [2/27 (#7)]: 2 => 2 (-), dim=32.0, hs=43.5
 [3/27 (#9)]: 2 => 2 (-), dim=32.0, hs=43.5
 [4/27 (#11)]: 2 => 2 (-), dim=32.0, hs=43.5
 [5/27 (#18)]: 2 => 2 (-), dim=32.0, hs=43.5
 [6/27 (#23)]: 2 => 2 (-), dim=32.0, hs=43.5
 [7/27 (#26)]: 2 => 1 (1 empty), dim=32.0, hs=43.0
  Removed: 26.1
 [8/27 (#27)]: 2 => 2 (-), dim=32.0, hs=43.5
 [9/27 (#31)]: 2 => 1 (1 empty), dim=32.0, hs=43.0
  Removed: 31.1
 [10/27 (#32)]: 2 => 2 (-), dim=32.0, hs=43.5
 [11/27 (#33)]: 2 => 2 (-), dim=32.0, hs=43.5
 [12/27 (#34)]: 2 => 2 (-), dim=32.0, hs=43.5
 [13/27 (#35)]: 2 => 2 (-), dim=32.0, hs=43.5
 [14/27 (#2)]: 3 => 3 (-), dim=32.0, hs=43.7
 [15/27 (#3)]: 3 => 3 (-), dim=32.0, hs=43.7
 [16/27 (#10)]: 3 => 3 (-), dim=32.0, hs=43.7
 [17/27 (#16)]: 3 => 3 (-), dim=32.0, hs=43.7
 [18/27 (#22)]: 3 => 1 (2 empty), dim=32.0, hs=43.0
  Removed: 22.0,22.2
 [19/27 (#24)]: 3 => 3 (-), dim=32.0, hs=43.7
 [20/27 (#4)]: 9 => 9 (-), dim=32.0, hs=44.1
 [21/27 (#5)]: 9 => 9 (-), dim=32.0, hs=44.1
 [22/27 (#17)]: 9 => 9 (-), dim=32.0, hs=44.3
 [23/27 (#19)]: 9 => 9 (-), dim=32.0, hs=44.3
 [24/27 (#28)]: 9 => 9 (-), dim=32.0, hs=44.3
 [25/27 (#29)]: 9 => 9 (-), dim=32.0, hs=44.3
 [26/27 (#8)]: 23 => 23 (-), dim=32.0, hs=44.8
 [27/27 (#36)]: 29 => 26 (3 empty), dim=32.0, hs=43.0
  Removed: 36.10,36.19,36.15
 Applying common planes (codim=13, hs=43)...
 [1/24 (#6)]: 2 => 2 (-), dim=29.0, hs=43.5
 [2/24 (#7)]: 2 => 2 (-), dim=29.0, hs=43.5
 [3/24 (#9)]: 2 => 2 (-), dim=29.0, hs=43.5
 [4/24 (#11)]: 2 => 2 (-), dim=29.0, hs=43.5
 [5/24 (#18)]: 2 => 2 (-), dim=29.0, hs=43.5
 [6/24 (#23)]: 2 => 1 (1 empty), dim=29.0, hs=43.0
  Removed: 23.0
 [7/24 (#27)]: 2 => 2 (-), dim=29.0, hs=43.5
 [8/24 (#32)]: 2 => 2 (-), dim=29.0, hs=43.5
 [9/24 (#33)]: 2 => 2 (-), dim=29.0, hs=43.5
 [10/24 (#34)]: 2 => 2 (-), dim=29.0, hs=43.5
 [11/24 (#35)]: 2 => 2 (-), dim=29.0, hs=43.5
 [12/24 (#2)]: 3 => 3 (-), dim=29.0, hs=43.7
 [13/24 (#3)]: 3 => 3 (-), dim=29.0, hs=43.7
 [14/24 (#10)]: 3 => 3 (-), dim=29.0, hs=43.7
 [15/24 (#16)]: 3 => 3 (-), dim=29.0, hs=43.7
 [16/24 (#24)]: 3 => 3 (-), dim=29.0, hs=43.7
 [17/24 (#4)]: 9 => 9 (-), dim=29.0, hs=44.1
 [18/24 (#5)]: 9 => 9 (-), dim=29.0, hs=44.1
 [19/24 (#17)]: 9 => 9 (-), dim=29.0, hs=44.3
 [20/24 (#19)]: 9 => 9 (-), dim=29.0, hs=44.3
 [21/24 (#28)]: 9 => 9 (-), dim=29.0, hs=44.3
 [22/24 (#29)]: 9 => 9 (-), dim=29.0, hs=44.3
 [23/24 (#8)]: 23 => 23 (-), dim=29.0, hs=44.8
 [24/24 (#36)]: 26 => 23 (3 empty), dim=29.0, hs=43.0
  Removed: 36.9,36.18,36.14
 Applying common planes (codim=14, hs=43)...
 [1/23 (#6)]: 2 => 2 (-), dim=28.0, hs=43.5
 [2/23 (#7)]: 2 => 2 (-), dim=28.0, hs=43.5
 [3/23 (#9)]: 2 => 2 (-), dim=28.0, hs=43.5
 [4/23 (#11)]: 2 => 2 (-), dim=28.0, hs=43.5
 [5/23 (#18)]: 2 => 2 (-), dim=28.0, hs=43.5
 [6/23 (#27)]: 2 => 2 (-), dim=28.0, hs=43.5
 [7/23 (#32)]: 2 => 2 (-), dim=28.0, hs=43.5
 [8/23 (#33)]: 2 => 2 (-), dim=28.0, hs=43.5
 [9/23 (#34)]: 2 => 2 (-), dim=28.0, hs=43.5
 [10/23 (#35)]: 2 => 2 (-), dim=28.0, hs=43.5
 [11/23 (#2)]: 3 => 3 (-), dim=28.0, hs=43.7
 [12/23 (#3)]: 3 => 3 (-), dim=28.0, hs=43.7
 [13/23 (#10)]: 3 => 3 (-), dim=28.0, hs=43.7
 [14/23 (#16)]: 3 => 3 (-), dim=28.0, hs=43.7
 [15/23 (#24)]: 3 => 1 (2 empty), dim=28.0, hs=43.0
  Removed: 24.0,24.9
 [16/23 (#4)]: 9 => 9 (-), dim=28.0, hs=44.1
 [17/23 (#5)]: 9 => 9 (-), dim=28.0, hs=44.1
 [18/23 (#17)]: 9 => 9 (-), dim=28.0, hs=44.3
 [19/23 (#19)]: 9 => 9 (-), dim=28.0, hs=44.3
 [20/23 (#28)]: 9 => 9 (-), dim=28.0, hs=44.3
 [21/23 (#29)]: 9 => 9 (-), dim=28.0, hs=44.3
 [22/23 (#8)]: 23 => 23 (-), dim=28.0, hs=44.8
 [23/23 (#36)]: 23 => 23 (-), dim=28.0, hs=43.0
 Applying common planes (codim=15, hs=43)...
 [1/22 (#6)]: 2 => 2 (-), dim=27.0, hs=43.5
 [2/22 (#7)]: 2 => 2 (-), dim=27.0, hs=43.5
 [3/22 (#9)]: 2 => 2 (-), dim=27.0, hs=43.5
 [4/22 (#11)]: 2 => 2 (-), dim=27.0, hs=43.5
 [5/22 (#18)]: 2 => 2 (-), dim=27.0, hs=43.5
 [6/22 (#27)]: 2 => 2 (-), dim=27.0, hs=43.5
 [7/22 (#32)]: 2 => 2 (-), dim=27.0, hs=43.5
 [8/22 (#33)]: 2 => 2 (-), dim=27.0, hs=43.5
 [9/22 (#34)]: 2 => 2 (-), dim=27.0, hs=43.5
 [10/22 (#35)]: 2 => 2 (-), dim=27.0, hs=43.5
 [11/22 (#2)]: 3 => 3 (-), dim=27.0, hs=43.7
 [12/22 (#3)]: 3 => 3 (-), dim=27.0, hs=43.7
 [13/22 (#10)]: 3 => 3 (-), dim=27.0, hs=43.7
 [14/22 (#16)]: 3 => 3 (-), dim=27.0, hs=43.7
 [15/22 (#4)]: 9 => 9 (-), dim=27.0, hs=44.1
 [16/22 (#5)]: 9 => 4 (5 empty), dim=27.0, hs=43.8
  Removed: 5.0,5.1,5.2,5.8,5.9
 [17/22 (#17)]: 9 => 9 (-), dim=27.0, hs=44.3
 [18/22 (#19)]: 9 => 9 (-), dim=27.0, hs=44.3
 [19/22 (#28)]: 9 => 3 (6 empty), dim=27.0, hs=43.7
  Removed: 28.36,28.37,28.9,28.16,28.25,28.26
 [20/22 (#29)]: 9 => 3 (6 empty), dim=27.0, hs=43.7
  Removed: 29.1,29.7,29.8,29.10,29.13,29.14
 [21/22 (#8)]: 23 => 23 (-), dim=27.0, hs=44.8
 [22/22 (#36)]: 23 => 22 (1 empty), dim=27.0, hs=43.0
  Removed: 36.16
 Savings: factor 45,595 (10^5), 15 formulas
[1/21 (#6, #7)]: 2 * 2 = 4 => 4 (-), dim=26.0, hs=44.0
[2/21 (w1, #9)]: 4 * 2 = 8 => 8 (-), dim=25.0, hs=44.5
[3/21 (w2, #11)]: 8 * 2 = 16 => 16 (-), dim=24.0, hs=45.0
[4/21 (w3, #18)]: 16 * 2 = 32 => 32 (-), dim=23.0, hs=45.5
[5/21 (w4, #27)]: 32 * 2 = 64 => 64 (-), dim=22.0, hs=46.0
[6/21 (w5, #32)]: 64 * 2 = 128 => 64 (64 incl), dim=21.5, hs=46.5
[7/21 (w6, #33)]: 64 * 2 = 128 => 64 (64 incl), dim=21.0, hs=47.0
[8/21 (w7, #34)]: 64 * 2 = 128 => 64 (64 incl), dim=20.5, hs=47.5
[9/21 (w8, #35)]: 64 * 2 = 128 => 64 (64 incl), dim=19.5, hs=47.5
[10/21 (w9, #2)]: 64 * 3 = 192 => 48 (32 empty, 112 incl), dim=19.3, hs=48.0
[11/21 (w10, #3)]: 48 * 3 = 144 => 144 (-), dim=18.3, hs=48.7
[12/21 (w11, #10)]: 144 * 3 = 432 => 192 (96 empty, 144 incl), dim=17.5, hs=48.9
[13/21 (w12, #16)]: 192 * 3 = 576 => 576 (-), dim=16.5, hs=49.6
[14/21 (w13, #28)]: 576 * 3 = 1728 => 576 (576 empty, 576 incl), dim=15.5, hs=49.6
[15/21 (w14, #29)]: 576 * 3 = 1728 => 576 (1152 empty), dim=14.5, hs=49.6
[16/21 (w15, #5)]: 576 * 4 = 2304 => 384 (528 empty, 1392 incl), dim=13.9, hs=49.4
[17/21 (w16, #4)]: 384 * 9 = 3456 => 320 (1776 empty, 1360 incl), dim=13.4, hs=49.2
[18/21 (w17, #17)]: 320 * 9 = 2880 => 192 (2624 empty, 64 incl), dim=12.4, hs=48.7
[19/21 (w18, #19)]: 192 * 9 = 1728 => 96 (1008 empty, 624 incl), dim=12.3, hs=49.2
[20/21 (w19, #36)]: 96 * 22 = 2112 => 1020 (1092 empty), dim=11.8, hs=49.2
[21/21 (w20, #8)]: 1020 * 23 = 23460 => 467 (21866 empty, 1127 incl), dim=10.2, hs=48.1
Total time: 570.995 sec, total intersections: 42,105, total inclusion tests: 1,226,608
Common plane time: 5.952 sec
Solutions: 467, dim=10.2, hs=48.1, max=1020, maxin=23460
f-vector: [0, 0, 0, 0, 0, 0, 60, 0, 0, 0, 63, 344]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.903 GiB, pagefile 0.945 GiB
Memory: pmem(rss=176537600, vms=173879296, num_page_faults=524927, peak_wset=969637888, wset=176537600, peak_paged_pool=538552, paged_pool=538376, peak_nonpaged_pool=139312, nonpaged_pool=91632, pagefile=173879296, peak_pagefile=1014611968, private=173879296)
