
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-03T22:34:21+02:00
Command line: ..\trop\ptcut.py BIOMD0000000102 -e11 --no-cache=n --maxruntime 3600 --maxmem 41393M --comment "concurrent=1, timeout=3600"

----------------------------------------------------------------------
Solving BIOMD0000000102 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=False, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=1, timeout=3600
Random seed: YIVAD6MYN9JZ
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 13
Formulas: 13
Order: 8 4 12 2 9 6 5 11 10 7 1 0 3
Possible combinations: 1 * 2 * 2 * 4 * 4 * 5 * 6 * 6 * 9 * 10 * 12 * 25 * 36 = 11,197,440,000 (10^10)
 Applying common planes (codim=1, hs=2)...
 [1/12 (#4)]: 2 => 2 (-), dim=11.0, hs=3.0
 [2/12 (#12)]: 2 => 2 (-), dim=11.0, hs=4.0
 [3/12 (#2)]: 4 => 3 (1 incl), dim=11.0, hs=4.0
  Removed: 2.3
 [4/12 (#9)]: 4 => 4 (-), dim=11.0, hs=5.0
 [5/12 (#6)]: 5 => 5 (-), dim=11.0, hs=7.0
 [6/12 (#5)]: 6 => 6 (-), dim=11.0, hs=6.0
 [7/12 (#11)]: 6 => 6 (-), dim=11.0, hs=6.0
 [8/12 (#10)]: 9 => 9 (-), dim=11.0, hs=7.0
 [9/12 (#7)]: 10 => 5 (5 incl), dim=11.2, hs=7.0
  Removed: 7.0,7.3,7.4,7.5,7.6
 [10/12 (#1)]: 12 => 12 (-), dim=11.0, hs=8.0
 [11/12 (#0)]: 25 => 21 (4 incl), dim=11.0, hs=10.0
  Removed: 0.8,0.3,0.13,0.23
 [12/12 (#3)]: 36 => 26 (10 incl), dim=11.0, hs=12.0
  Removed: 3.0,3.6,3.7,3.9,3.10,3.21,3.22,3.23,3.24,3.25
 Savings: factor 4.395604395604396, 1 formulas
[1/11 (#4, #12)]: 2 * 2 = 4 => 4 (-), dim=10.0, hs=5.0
[2/11 (w1, #2)]: 4 * 3 = 12 => 8 (4 incl), dim=9.2, hs=7.0
[3/11 (w2, #9)]: 8 * 4 = 32 => 10 (22 incl), dim=8.2, hs=8.1
[4/11 (w3, #6)]: 10 * 5 = 50 => 44 (6 empty), dim=7.2, hs=12.3
[5/11 (w4, #7)]: 44 * 5 = 220 => 112 (22 empty, 86 incl), dim=6.6, hs=14.5
[6/11 (w5, #5)]: 112 * 6 = 672 => 98 (520 empty, 54 incl), dim=6.5, hs=15.1
 Applying common planes (codim=1, hs=1)...
 [1/6 (w6)]: 98 => 98 (-), dim=6.5, hs=15.1
 [2/6 (#11)]: 6 => 6 (-), dim=10.0, hs=6.0
 [3/6 (#10)]: 9 => 9 (-), dim=10.0, hs=7.0
 [4/6 (#1)]: 12 => 12 (-), dim=10.0, hs=8.0
 [5/6 (#0)]: 21 => 17 (4 incl), dim=10.1, hs=10.0
  Removed: 0.16,0.17,0.19,0.15
 [6/6 (#3)]: 26 => 26 (-), dim=10.0, hs=12.0
 Savings: factor 1.2352941176470589, 0 formulas
[7/11 (w6, #11)]: 98 * 6 = 588 => 135 (264 empty, 189 incl), dim=5.9, hs=15.7
[8/11 (w7, #10)]: 135 * 9 = 1215 => 154 (653 empty, 408 incl), dim=5.9, hs=15.9
[9/11 (w8, #1)]: 154 * 12 = 1848 => 123 (674 empty, 1051 incl), dim=5.0, hs=15.5
[10/11 (w9, #0)]: 123 * 17 = 2091 => 184 (1332 empty, 575 incl), dim=5.2, hs=16.3
[11/11 (w10, #3)]: 184 * 26 = 4784 => 322 (3569 empty, 893 incl), dim=5.1, hs=16.6
Total time: 7.199 sec, total intersections: 11,809, total inclusion tests: 119,348
Common plane time: 0.262 sec
Solutions: 322, dim=5.1, hs=16.6, max=322, maxin=4784
f-vector: [0, 0, 0, 0, 22, 240, 50, 10]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.101 GiB, pagefile 0.096 GiB
Memory: pmem(rss=81682432, vms=74952704, num_page_faults=31634, peak_wset=108314624, wset=81682432, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=46360, nonpaged_pool=45808, pagefile=74952704, peak_pagefile=102563840, private=74952704)
