
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:33+02:00
Command line: BIOMD0000000397 --yices
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=yices


Model: BIOMD0000000397
Logfile: db/BIOMD0000000397/ep11-yices-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^38.5 in 76 bags
Bag sizes: 4 4 1 1 1 1 1 17 17 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 4 9 4 4 8 4 4 4 4 7 7 4 2 2 2 4 9 4 2 4 4 2 4 1 1 4 20 7 6 4 3 5 6 12 5 11 5 4 7 3 10 9 5 22 22 2 4 6 11 2 3 4

Minimizing (20,0)... (20,0)


Running SMT solver (1)... 0.010 sec
No solutions found



Solution:
\ no solution

0 polyhedra, 1 rounds, smt 0.010 sec, isect 0.000 sec, insert 0.000 sec, total 0.078 sec
Preprocessing 0.014 sec, super total 0.092 sec
Comparing... 0.049 sec.  Solution matches input.
