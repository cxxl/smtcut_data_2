
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.4 (default, Aug  9 2019, 18:34:13) [MSC v.1915 64 bit (AMD64)]
numpy 1.16.5, sympy 1.4, gmpy2 2.1.0a5
It is now 2020-04-08T08:27:56+02:00
Command line: ..\trop\ptcut.py BIOMD0000000073 -e11 --rat --no-cache=n --maxruntime 86400 --maxmem 18669M --comment "concurrent=6, timeout=86400"

----------------------------------------------------------------------
Solving BIOMD0000000073 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=86400
Random seed: 1T4LWP9WG3MPQ
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 16
Formulas: 16
Order: 15 0 2 5 6 7 9 11 13 14 1 10 3 4 8 12
Possible combinations: 3 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 14 * 14 * 24 * 24 * 24 * 24 = 51,140,175,593,472 (10^14)
 Applying common planes (codim=0, hs=1)...
 [1/16 (#15)]: 3 => 3 (-), dim=15.0, hs=3.3
 [2/16 (#0)]: 4 => 4 (-), dim=15.0, hs=2.2
 [3/16 (#2)]: 4 => 4 (-), dim=15.0, hs=3.2
 [4/16 (#5)]: 4 => 4 (-), dim=15.0, hs=3.2
 [5/16 (#6)]: 4 => 4 (-), dim=15.0, hs=3.2
 [6/16 (#7)]: 4 => 4 (-), dim=15.0, hs=3.2
 [7/16 (#9)]: 4 => 4 (-), dim=15.0, hs=3.2
 [8/16 (#11)]: 4 => 4 (-), dim=15.0, hs=3.2
 [9/16 (#13)]: 4 => 4 (-), dim=15.0, hs=3.2
 [10/16 (#14)]: 4 => 4 (-), dim=15.0, hs=3.2
 [11/16 (#1)]: 14 => 14 (-), dim=15.0, hs=4.9
 [12/16 (#10)]: 14 => 14 (-), dim=15.0, hs=5.6
 [13/16 (#3)]: 24 => 24 (-), dim=15.0, hs=6.7
 [14/16 (#4)]: 24 => 24 (-), dim=15.0, hs=6.7
 [15/16 (#8)]: 24 => 24 (-), dim=15.0, hs=6.7
 [16/16 (#12)]: 24 => 24 (-), dim=15.0, hs=6.7
 Savings: factor 1.0, 0 formulas
[1/15 (#0, #15)]: 4 * 3 = 12 => 12 (-), dim=14.0, hs=4.6
[2/15 (w1, #2)]: 12 * 4 = 48 => 48 (-), dim=13.0, hs=6.8
[3/15 (w2, #5)]: 48 * 4 = 192 => 60 (132 incl), dim=12.2, hs=8.0
[4/15 (w3, #6)]: 60 * 4 = 240 => 240 (-), dim=11.2, hs=10.2
[5/15 (w4, #7)]: 240 * 4 = 960 => 336 (624 incl), dim=10.3, hs=11.3
[6/15 (w5, #9)]: 336 * 4 = 1344 => 1344 (-), dim=9.3, hs=13.5
[7/15 (w6, #11)]: 1344 * 4 = 5376 => 5376 (-), dim=8.3, hs=15.8
[8/15 (w7, #13)]: 5376 * 4 = 21504 => 13824 (7680 empty), dim=7.2, hs=17.0
