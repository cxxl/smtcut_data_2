
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-03T21:23:53+02:00
Command line: ..\..\trop\ptcut.py BIOMD0000000103 -e11 --no-cache=n --maxruntime 3600 --maxmem 3838M --comment "concurrent=11, timeout=3600"

----------------------------------------------------------------------
Solving BIOMD0000000103 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=False, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=11, timeout=3600
Random seed: 36C9Y48M2YRQ3
Effective epsilon: 1/11.0

Reading db\BIOMD0000000103\vector_field_q.txt
Reading db\BIOMD0000000103\conservation_laws_q.txt
Computing tropical system... 
Tropicalization time: 4.381 sec
Variables: x1-x17
Saving tropical system...
Creating polyhedra... 
Creation time: 1.051 sec
Saving polyhedra...

Sorting ascending...
Dimension: 17
Formulas: 17
Order: 14 16 6 4 12 13 15 2 9 5 11 10 1 8 3 0 7
Possible combinations: 3 * 3 * 5 * 6 * 6 * 6 * 6 * 9 * 9 * 12 * 12 * 16 * 20 * 25 * 36 * 49 * 54 = 518,378,945,495,040,000 (10^18)
[1/16 (#14, #16)]: 3 * 3 = 9 => 9 (-), dim=15.0, hs=6.0
[2/16 (w1, #6)]: 9 * 5 = 45 => 45 (-), dim=14.0, hs=11.0
[3/16 (w2, #4)]: 45 * 6 = 270 => 240 (30 empty), dim=13.0, hs=14.5
[4/16 (w3, #12)]: 240 * 6 = 1440 => 540 (200 empty, 700 incl), dim=12.0, hs=16.3
[5/16 (w4, #13)]: 540 * 6 = 3240 => 1121 (384 empty, 1735 incl), dim=11.2, hs=18.1
[6/16 (w5, #15)]: 1121 * 6 = 6726 => 1476 (1474 empty, 3776 incl), dim=10.1, hs=18.7
[7/16 (w6, #2)]: 1476 * 9 = 13284 => 1485 (6500 empty, 5299 incl), dim=9.7, hs=19.2
[8/16 (w7, #9)]: 1485 * 9 = 13365 => 867 (5623 empty, 6875 incl), dim=9.3, hs=19.1
[9/16 (w8, #5)]: 867 * 12 = 10404 => 1402 (6399 empty, 2603 incl), dim=8.9, hs=20.5
[10/16 (w9, #11)]: 1402 * 12 = 16824 => 1847 (7340 empty, 7637 incl), dim=8.0, hs=20.4
[11/16 (w10, #10)]: 1847 * 16 = 29552 => 2149 (15984 empty, 11419 incl), dim=7.3, hs=20.2
[12/16 (w11, #1)]: 2149 * 20 = 42980 => 1112 (26324 empty, 15544 incl), dim=6.5, hs=20.0
[13/16 (w12, #8)]: 1112 * 25 = 27800 => 1370 (21672 empty, 4758 incl), dim=6.5, hs=21.1
[14/16 (w13, #3)]: 1370 * 36 = 49320 => 1564 (33021 empty, 14735 incl), dim=6.1, hs=20.9
[15/16 (w14, #0)]: 1564 * 49 = 76636 => 2063 (57194 empty, 17379 incl), dim=6.2, hs=21.5
[16/16 (w15, #7)]: 2063 * 54 = 111402 => 1938 (73054 empty, 36410 incl), dim=6.0, hs=21.7
Total time: 763.480 sec, total intersections: 403,297, total inclusion tests: 9,615,886
Common plane time: 1.370 sec
Solutions: 1938, dim=6.0, hs=21.7, max=2149, maxin=111402
f-vector: [0, 0, 0, 0, 2, 530, 940, 424, 42]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.525 GiB, pagefile 0.529 GiB
Memory: pmem(rss=110313472, vms=105644032, num_page_faults=735592, peak_wset=563752960, wset=110313472, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=152032, nonpaged_pool=60088, pagefile=105644032, peak_pagefile=568512512, private=105644032)
