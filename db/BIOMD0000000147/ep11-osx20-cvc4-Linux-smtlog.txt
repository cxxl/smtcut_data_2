
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-25T11:51:47+02:00
Command line: --no-dump --no-comp BIOMD0000000147 --cvc4 --pp
Flags: ppone ppsmt pp2x2 no-dump justone maxadd=20 solver=cvc4


Model: BIOMD0000000147
Logfile: db/BIOMD0000000147/ep11-osx20-cvc4-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^15.9 in 25 bags
Bag sizes: 2 12 4 4 6 1 2 49 36 16 4 2 1 6 4 12 1 1 12 4 6 1 2 4 11

Filtering bags...
Collecting...
Bag 1/20, 2 polyhedra
Bag 2/20, 12 polyhedra
Bag 3/20, 4 polyhedra => 2 dropped
Bag 4/20, 4 polyhedra
Bag 5/20, 6 polyhedra
Bag 6/20, 2 polyhedra
Bag 7/20, 49 polyhedra
Bag 8/20, 36 polyhedra
Bag 9/20, 16 polyhedra => 12 dropped
Bag 10/20, 4 polyhedra
Bag 11/20, 2 polyhedra
Bag 12/20, 6 polyhedra
Bag 13/20, 4 polyhedra => 2 dropped
Bag 14/20, 12 polyhedra
Bag 15/20, 12 polyhedra
Bag 16/20, 4 polyhedra => 2 dropped
Bag 17/20, 6 polyhedra
Bag 18/20, 2 polyhedra
Bag 19/20, 4 polyhedra
Bag 20/20, 11 polyhedra
Possible combinations after preprocessing: 10^14.4 in 21 bags
Bag sizes: 1 2 12 2 4 6 2 49 36 4 4 2 6 2 12 12 2 6 2 4 11
Time: 0.195 sec

Checking for superfluous polyhedra...
Bag 1/21, 49 polyhedra:
  Polyhedron 1... required, 0.157 sec
  Polyhedron 2... superfluous, 0.133 sec
  Polyhedron 3... superfluous, 0.118 sec
  Polyhedron 4... superfluous, 0.107 sec
  Polyhedron 5... superfluous, 0.049 sec
  Polyhedron 6... superfluous, 0.064 sec
  Polyhedron 7... required, 0.087 sec
  Polyhedron 8... superfluous, 0.055 sec
  Polyhedron 9... required, 0.118 sec
  Polyhedron 10... superfluous, 0.109 sec
  Polyhedron 11... superfluous, 0.034 sec
  Polyhedron 12... superfluous, 0.040 sec
  Polyhedron 13... superfluous, 0.027 sec
  Polyhedron 14... superfluous, 0.040 sec
  Polyhedron 15... superfluous, 0.063 sec
  Polyhedron 16... superfluous, 0.106 sec
  Polyhedron 17... superfluous, 0.044 sec
  Polyhedron 18... superfluous, 0.028 sec
  Polyhedron 19... superfluous, 0.026 sec
  Polyhedron 20... superfluous, 0.037 sec
  Polyhedron 21... superfluous, 0.028 sec
  Polyhedron 22... superfluous, 0.062 sec
  Polyhedron 23... superfluous, 0.084 sec
  Polyhedron 24... superfluous, 0.060 sec
  Polyhedron 25... superfluous, 0.019 sec
  Polyhedron 26... superfluous, 0.027 sec
  Polyhedron 27... superfluous, 0.035 sec
  Polyhedron 28... superfluous, 0.029 sec
  Polyhedron 29... superfluous, 0.061 sec
  Polyhedron 30... superfluous, 0.014 sec
  Polyhedron 31... required, 0.073 sec
  Polyhedron 32... superfluous, 0.050 sec
  Polyhedron 33... superfluous, 0.015 sec
  Polyhedron 34... superfluous, 0.014 sec
  Polyhedron 35... superfluous, 0.025 sec
  Polyhedron 36... required, 0.155 sec
  Polyhedron 37... superfluous, 0.032 sec
  Polyhedron 38... superfluous, 0.022 sec
  Polyhedron 39... superfluous, 0.017 sec
  Polyhedron 40... superfluous, 0.015 sec
  Polyhedron 41... superfluous, 0.015 sec
  Polyhedron 42... superfluous, 0.024 sec
  Polyhedron 43... superfluous, 0.169 sec
  Polyhedron 44... superfluous, 0.134 sec
  Polyhedron 45... superfluous, 0.071 sec
  Polyhedron 46... superfluous, 0.021 sec
  Polyhedron 47... superfluous, 0.035 sec
  Polyhedron 48... superfluous, 0.053 sec
  Polyhedron 49... superfluous, 0.068 sec
  => 5 polyhedra left
Bag 2/21, 36 polyhedra:
  Polyhedron 1... superfluous, 0.052 sec
  Polyhedron 2... superfluous, 0.012 sec
  Polyhedron 3... superfluous, 0.046 sec
  Polyhedron 4... superfluous, 0.018 sec
  Polyhedron 5... superfluous, 0.017 sec
  Polyhedron 6... superfluous, 0.021 sec
  Polyhedron 7... required, 0.178 sec
  Polyhedron 8... superfluous, 0.023 sec
  Polyhedron 9... superfluous, 0.025 sec
  Polyhedron 10... superfluous, 0.018 sec
  Polyhedron 11... superfluous, 0.032 sec
  Polyhedron 12... superfluous, 0.018 sec
  Polyhedron 13... superfluous, 0.017 sec
  Polyhedron 14... required, 0.180 sec
  Polyhedron 15... superfluous, 0.022 sec
  Polyhedron 16... superfluous, 0.015 sec
  Polyhedron 17... superfluous, 0.016 sec
  Polyhedron 18... superfluous, 0.023 sec
  Polyhedron 19... superfluous, 0.044 sec
  Polyhedron 20... superfluous, 0.046 sec
  Polyhedron 21... superfluous, 0.097 sec
  Polyhedron 22... superfluous, 0.014 sec
  Polyhedron 23... required, 0.082 sec
  Polyhedron 24... superfluous, 0.040 sec
  Polyhedron 25... superfluous, 0.050 sec
  Polyhedron 26... superfluous, 0.026 sec
  Polyhedron 27... superfluous, 0.018 sec
  Polyhedron 28... superfluous, 0.039 sec
  Polyhedron 29... superfluous, 0.018 sec
  Polyhedron 30... superfluous, 0.019 sec
  Polyhedron 31... superfluous, 0.041 sec
  Polyhedron 32... superfluous, 0.016 sec
  Polyhedron 33... superfluous, 0.044 sec
  Polyhedron 34... superfluous, 0.038 sec
  Polyhedron 35... superfluous, 0.018 sec
  Polyhedron 36... required, 0.194 sec
  => 4 polyhedra left
Bag 3/21, 12 polyhedra:
  Polyhedron 1... superfluous, 0.012 sec
  Polyhedron 2... superfluous, 0.036 sec
  Polyhedron 3... superfluous, 0.009 sec
  Polyhedron 4... superfluous, 0.017 sec
  Polyhedron 5... required, 0.076 sec
  Polyhedron 6... superfluous, 0.036 sec
  Polyhedron 7... superfluous, 0.009 sec
  Polyhedron 8... superfluous, 0.015 sec
  Polyhedron 9... superfluous, 0.030 sec
  Polyhedron 10... required, 0.197 sec
  Polyhedron 11... superfluous, 0.004 sec
  Polyhedron 12... superfluous, 0.020 sec
  => 2 polyhedra left
Bag 4/21, 12 polyhedra:
  Polyhedron 1... superfluous, 0.026 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.013 sec
  Polyhedron 4... superfluous, 0.009 sec
  Polyhedron 5... superfluous, 0.076 sec
  Polyhedron 6... required, 0.161 sec
  Polyhedron 7... superfluous, 0.014 sec
  Polyhedron 8... superfluous, 0.008 sec
  Polyhedron 9... required, 0.128 sec
  Polyhedron 10... superfluous, 0.025 sec
  Polyhedron 11... superfluous, 0.011 sec
  Polyhedron 12... superfluous, 0.010 sec
  => 2 polyhedra left
Bag 5/21, 12 polyhedra:
  Polyhedron 1... superfluous, 0.041 sec
  Polyhedron 2... superfluous, 0.014 sec
  Polyhedron 3... superfluous, 0.008 sec
  Polyhedron 4... superfluous, 0.007 sec
  Polyhedron 5... required, 0.105 sec
  Polyhedron 6... superfluous, 0.017 sec
  Polyhedron 7... superfluous, 0.007 sec
  Polyhedron 8... superfluous, 0.008 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... required, 0.047 sec
  Polyhedron 11... superfluous, 0.011 sec
  Polyhedron 12... superfluous, 0.009 sec
  => 2 polyhedra left
Bag 6/21, 11 polyhedra:
  Polyhedron 1... required, 0.087 sec
  Polyhedron 2... superfluous, 0.014 sec
  Polyhedron 3... superfluous, 0.061 sec
  Polyhedron 4... required, 0.061 sec
  Polyhedron 5... superfluous, 0.046 sec
  Polyhedron 6... required, 0.116 sec
  Polyhedron 7... required, 0.099 sec
  Polyhedron 8... superfluous, 0.012 sec
  Polyhedron 9... required, 0.149 sec
  Polyhedron 10... superfluous, 0.057 sec
  Polyhedron 11... required, 0.105 sec
  => 6 polyhedra left
Bag 7/21, 6 polyhedra:
  Polyhedron 1... required, 0.133 sec
  Polyhedron 2... required, 0.148 sec
  Polyhedron 3... required, 0.231 sec
  Polyhedron 4... superfluous, 0.051 sec
  Polyhedron 5... required, 0.070 sec
  Polyhedron 6... superfluous, 0.012 sec
  => 4 polyhedra left
Bag 8/21, 6 polyhedra:
  Polyhedron 1... superfluous, 0.062 sec
  Polyhedron 2... superfluous, 0.065 sec
  Polyhedron 3... superfluous, 0.014 sec
  Polyhedron 4... required, 0.109 sec
  Polyhedron 5... required, 0.091 sec
  Polyhedron 6... superfluous, 0.055 sec
  => 2 polyhedra left
Bag 9/21, 6 polyhedra:
  Polyhedron 1... superfluous, 0.008 sec
  Polyhedron 2... superfluous, 0.007 sec
  Polyhedron 3... required, 0.148 sec
  Polyhedron 4... superfluous, 0.032 sec
  Polyhedron 5... required, 0.050 sec
  Polyhedron 6... superfluous, 0.020 sec
  => 2 polyhedra left
Bag 10/21, 4 polyhedra:
  Polyhedron 1... superfluous, 0.008 sec
  Polyhedron 2... superfluous, 0.023 sec
  Polyhedron 3... required, 0.122 sec
  Polyhedron 4... superfluous, 0.011 sec
  => 1 polyhedra left
Bag 11/21, 4 polyhedra:
  Polyhedron 1... required, 0.125 sec
  Polyhedron 2... required, 0.234 sec
  Polyhedron 3... required, 0.087 sec
  Polyhedron 4... required, 0.094 sec
  => 4 polyhedra left
Bag 12/21, 4 polyhedra:
  Polyhedron 1... superfluous, 0.008 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.241 sec
  Polyhedron 4... superfluous, 0.015 sec
  => 1 polyhedra left
Bag 13/21, 4 polyhedra:
  Polyhedron 1... superfluous, 0.047 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.130 sec
  Polyhedron 4... superfluous, 0.022 sec
  => 1 polyhedra left
Bag 14/21, 2 polyhedra:
  Polyhedron 1... required, 0.232 sec
  Polyhedron 2... required, 0.126 sec
  => 2 polyhedra left
Bag 15/21, 2 polyhedra:
  Polyhedron 1... required, 0.145 sec
  Polyhedron 2... required, 0.117 sec
  => 2 polyhedra left
Bag 16/21, 2 polyhedra:
  Polyhedron 1... required, 0.159 sec
  Polyhedron 2... required, 0.207 sec
  => 2 polyhedra left
Bag 17/21, 2 polyhedra:
  Polyhedron 1... superfluous, 0.010 sec
  Polyhedron 2... required, 0.122 sec
  => 1 polyhedra left
Bag 18/21, 2 polyhedra:
  Polyhedron 1... required, 0.091 sec
  Polyhedron 2... required, 0.197 sec
  => 2 polyhedra left
Bag 19/21, 2 polyhedra:
  Polyhedron 1... required, 0.201 sec
  Polyhedron 2... required, 0.150 sec
  => 2 polyhedra left
Bag 20/21, 2 polyhedra:
  Polyhedron 1... required, 0.101 sec
  Polyhedron 2... superfluous, 0.007 sec
  => 1 polyhedra left
Possible combinations after preprocessing: 10^6.3 in 16 bags
Bag sizes: 1 5 4 2 2 2 6 4 2 2 4 2 2 2 2 2
Time: 10.904 sec

Combining small bags...
Intersecting 2x6... 12 polyhedra
Intersecting 2x12... 24 polyhedra
Intersecting 2x5... 10 polyhedra
Intersecting 2x10... 15 polyhedra
Intersecting 2x15... 20 polyhedra
Intersecting 2x20... 40 polyhedra
Intersecting 2x4... 8 polyhedra
Intersecting 2x8... 16 polyhedra
Intersecting 2x16... 32 polyhedra
Intersecting 2x4... 8 polyhedra
Intersecting 4x8... 30 polyhedra
Possible combinations after preprocessing: 10^6.0 in 5 bags
Bag sizes: 1 24 30 32 40
Time: 1.904 sec

Checking for superfluous polyhedra...
Bag 1/5, 40 polyhedra:
  Polyhedron 1... superfluous, 0.044 sec
  Polyhedron 2... superfluous, 0.018 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.010 sec
  Polyhedron 5... superfluous, 0.010 sec
  Polyhedron 6... superfluous, 0.037 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... required, 0.063 sec
  Polyhedron 9... required, 0.041 sec
  Polyhedron 10... required, 0.036 sec
  Polyhedron 11... required, 0.067 sec
  Polyhedron 12... superfluous, 0.018 sec
  Polyhedron 13... superfluous, 0.015 sec
  Polyhedron 14... superfluous, 0.017 sec
  Polyhedron 15... superfluous, 0.013 sec
  Polyhedron 16... superfluous, 0.022 sec
  Polyhedron 17... superfluous, 0.017 sec
  Polyhedron 18... superfluous, 0.014 sec
  Polyhedron 19... superfluous, 0.011 sec
  Polyhedron 20... superfluous, 0.010 sec
  Polyhedron 21... required, 0.031 sec
  Polyhedron 22... superfluous, 0.024 sec
  Polyhedron 23... superfluous, 0.026 sec
  Polyhedron 24... superfluous, 0.011 sec
  Polyhedron 25... superfluous, 0.010 sec
  Polyhedron 26... superfluous, 0.049 sec
  Polyhedron 27... superfluous, 0.014 sec
  Polyhedron 28... required, 0.062 sec
  Polyhedron 29... required, 0.043 sec
  Polyhedron 30... required, 0.038 sec
  Polyhedron 31... required, 0.041 sec
  Polyhedron 32... superfluous, 0.019 sec
  Polyhedron 33... superfluous, 0.025 sec
  Polyhedron 34... superfluous, 0.027 sec
  Polyhedron 35... superfluous, 0.015 sec
  Polyhedron 36... superfluous, 0.016 sec
  Polyhedron 37... required, 0.020 sec
  Polyhedron 38... superfluous, 0.014 sec
  Polyhedron 39... superfluous, 0.010 sec
  Polyhedron 40... superfluous, 0.010 sec
  => 10 polyhedra left
Bag 2/5, 32 polyhedra:
  Polyhedron 1... superfluous, 0.068 sec
  Polyhedron 2... superfluous, 0.060 sec
  Polyhedron 3... required, 0.056 sec
  Polyhedron 4... superfluous, 0.066 sec
  Polyhedron 5... superfluous, 0.017 sec
  Polyhedron 6... superfluous, 0.013 sec
  Polyhedron 7... superfluous, 0.014 sec
  Polyhedron 8... superfluous, 0.014 sec
  Polyhedron 9... required, 0.042 sec
  Polyhedron 10... required, 0.068 sec
  Polyhedron 11... required, 0.101 sec
  Polyhedron 12... required, 0.064 sec
  Polyhedron 13... superfluous, 0.010 sec
  Polyhedron 14... superfluous, 0.009 sec
  Polyhedron 15... superfluous, 0.009 sec
  Polyhedron 16... superfluous, 0.009 sec
  Polyhedron 17... superfluous, 0.015 sec
  Polyhedron 18... superfluous, 0.017 sec
  Polyhedron 19... superfluous, 0.012 sec
  Polyhedron 20... superfluous, 0.013 sec
  Polyhedron 21... superfluous, 0.072 sec
  Polyhedron 22... superfluous, 0.053 sec
  Polyhedron 23... superfluous, 0.074 sec
  Polyhedron 24... required, 0.048 sec
  Polyhedron 25... superfluous, 0.013 sec
  Polyhedron 26... superfluous, 0.010 sec
  Polyhedron 27... superfluous, 0.008 sec
  Polyhedron 28... superfluous, 0.009 sec
  Polyhedron 29... required, 0.069 sec
  Polyhedron 30... required, 0.085 sec
  Polyhedron 31... required, 0.046 sec
  Polyhedron 32... required, 0.030 sec
  => 10 polyhedra left
Bag 3/5, 30 polyhedra:
  Polyhedron 1... superfluous, 0.057 sec
  Polyhedron 2... required, 0.063 sec
  Polyhedron 3... superfluous, 0.018 sec
  Polyhedron 4... superfluous, 0.061 sec
  Polyhedron 5... required, 0.034 sec
  Polyhedron 6... required, 0.027 sec
  Polyhedron 7... superfluous, 0.077 sec
  Polyhedron 8... required, 0.070 sec
  Polyhedron 9... required, 0.125 sec
  Polyhedron 10... superfluous, 0.016 sec
  Polyhedron 11... required, 0.091 sec
  Polyhedron 12... required, 0.086 sec
  Polyhedron 13... required, 0.048 sec
  Polyhedron 14... superfluous, 0.022 sec
  Polyhedron 15... superfluous, 0.116 sec
  Polyhedron 16... required, 0.089 sec
  Polyhedron 17... required, 0.097 sec
  Polyhedron 18... superfluous, 0.032 sec
  Polyhedron 19... required, 0.130 sec
  Polyhedron 20... required, 0.123 sec
  Polyhedron 21... required, 0.048 sec
  Polyhedron 22... superfluous, 0.025 sec
  Polyhedron 23... required, 0.101 sec
  Polyhedron 24... required, 0.071 sec
  Polyhedron 25... required, 0.087 sec
  Polyhedron 26... superfluous, 0.011 sec
  Polyhedron 27... required, 0.135 sec
  Polyhedron 28... required, 0.114 sec
  Polyhedron 29... required, 0.054 sec
  Polyhedron 30... superfluous, 0.024 sec
  => 19 polyhedra left
Bag 4/5, 24 polyhedra:
  Polyhedron 1... required, 0.049 sec
  Polyhedron 2... superfluous, 0.013 sec
  Polyhedron 3... required, 0.069 sec
  Polyhedron 4... required, 0.058 sec
  Polyhedron 5... required, 0.069 sec
  Polyhedron 6... superfluous, 0.035 sec
  Polyhedron 7... superfluous, 0.018 sec
  Polyhedron 8... superfluous, 0.010 sec
  Polyhedron 9... superfluous, 0.011 sec
  Polyhedron 10... superfluous, 0.009 sec
  Polyhedron 11... superfluous, 0.012 sec
  Polyhedron 12... superfluous, 0.024 sec
  Polyhedron 13... superfluous, 0.030 sec
  Polyhedron 14... superfluous, 0.010 sec
  Polyhedron 15... superfluous, 0.011 sec
  Polyhedron 16... superfluous, 0.009 sec
  Polyhedron 17... superfluous, 0.010 sec
  Polyhedron 18... superfluous, 0.010 sec
  Polyhedron 19... superfluous, 0.064 sec
  Polyhedron 20... required, 0.074 sec
  Polyhedron 21... superfluous, 0.009 sec
  Polyhedron 22... required, 0.042 sec
  Polyhedron 23... superfluous, 0.033 sec
  Polyhedron 24... required, 0.068 sec
  => 7 polyhedra left
Possible combinations after preprocessing: 10^4.1 in 5 bags
Bag sizes: 10 10 19 7 1
Time: 5.036 sec

Minimizing (10,6)... (10,6)


Running SMT solver (1)... 0.056 sec
Checking... Minimizing (22,61)... (21,12), 0.022 sec
Inserting... 0.000 sec
[1 ph, smt 0.056, isect 0.022, ins 0.000, tot 0.092]

Running SMT solver (2)... 0.006 sec
Checking... Minimizing (22,64)... (21,11), 0.023 sec
Inserting... 0.000 sec
[2 ph, smt 0.062, isect 0.045, ins 0.000, tot 0.125]

Running SMT solver (3)... 0.013 sec
Checking... Minimizing (23,63)... (22,7), 0.020 sec
Inserting... 0.000 sec
[3 ph, smt 0.075, isect 0.064, ins 0.000, tot 0.162]

Running SMT solver (4)... 0.052 sec
Checking... Minimizing (19,67)... (19,14), 0.025 sec
Inserting... 0.000 sec
[4 ph, smt 0.127, isect 0.089, ins 0.000, tot 0.243]

Running SMT solver (5)... 0.019 sec
Checking... Minimizing (19,67)... (19,12), 0.025 sec
Inserting... 0.000 sec
[5 ph, smt 0.146, isect 0.114, ins 0.000, tot 0.291]

Running SMT solver (6)... 0.024 sec
Checking... Minimizing (19,67)... (19,18), 0.027 sec
Inserting... 0.000 sec
[6 ph, smt 0.170, isect 0.141, ins 0.001, tot 0.347]

Running SMT solver (7)... 0.005 sec
Checking... Minimizing (19,67)... (19,15), 0.026 sec
Inserting... 0.003 sec
[6 ph, smt 0.176, isect 0.167, ins 0.003, tot 0.384]

Running SMT solver (8)... 0.021 sec
Checking... Minimizing (18,64)... (18,19), 0.027 sec
Inserting... 0.000 sec
[7 ph, smt 0.197, isect 0.194, ins 0.004, tot 0.436]

Running SMT solver (9)... 0.009 sec
Checking... Minimizing (18,64)... (18,20), 0.030 sec
Inserting... 0.000 sec
[8 ph, smt 0.205, isect 0.223, ins 0.004, tot 0.479]

Running SMT solver (10)... 0.005 sec
Checking... Minimizing (18,64)... (18,20), 0.032 sec
Inserting... 0.000 sec
[9 ph, smt 0.210, isect 0.256, ins 0.004, tot 0.520]

Running SMT solver (11)... 0.007 sec
Checking... Minimizing (18,64)... (18,19), 0.029 sec
Inserting... 0.000 sec
[10 ph, smt 0.216, isect 0.284, ins 0.004, tot 0.560]

Running SMT solver (12)... 0.020 sec
Checking... Minimizing (18,64)... (18,16), 0.027 sec
Inserting... 0.000 sec
[11 ph, smt 0.236, isect 0.311, ins 0.005, tot 0.611]

Running SMT solver (13)... 0.012 sec
Checking... Minimizing (19,66)... (19,17), 0.029 sec
Inserting... 0.003 sec
[12 ph, smt 0.247, isect 0.340, ins 0.008, tot 0.658]

Running SMT solver (14)... 0.005 sec
Checking... Minimizing (18,62)... (18,22), 0.030 sec
Inserting... 0.006 sec
[13 ph, smt 0.253, isect 0.370, ins 0.014, tot 0.703]

Running SMT solver (15)... 0.005 sec
Checking... Minimizing (19,66)... (19,20), 0.028 sec
Inserting... 0.003 sec
[14 ph, smt 0.258, isect 0.397, ins 0.017, tot 0.742]

Running SMT solver (16)... 0.006 sec
Checking... Minimizing (18,62)... (18,21), 0.028 sec
Inserting... 0.006 sec
[15 ph, smt 0.264, isect 0.425, ins 0.022, tot 0.786]

Running SMT solver (17)... 0.015 sec
Checking... Minimizing (18,63)... (18,22), 0.029 sec
Inserting... 0.001 sec
[16 ph, smt 0.279, isect 0.455, ins 0.023, tot 0.833]

Running SMT solver (18)... 0.005 sec
Checking... Minimizing (18,63)... (18,21), 0.028 sec
Inserting... 0.003 sec
[17 ph, smt 0.284, isect 0.483, ins 0.026, tot 0.874]

Running SMT solver (19)... 0.006 sec
Checking... Minimizing (18,63)... (18,18), 0.027 sec
Inserting... 0.000 sec
[18 ph, smt 0.289, isect 0.509, ins 0.026, tot 0.910]

Running SMT solver (20)... 0.011 sec
Checking... Minimizing (18,64)... (18,21), 0.030 sec
Inserting... 0.000 sec
[19 ph, smt 0.300, isect 0.540, ins 0.027, tot 0.955]

Running SMT solver (21)... 0.007 sec
Checking... Minimizing (19,67)... (19,21), 0.031 sec
Inserting... 0.000 sec
[20 ph, smt 0.307, isect 0.570, ins 0.027, tot 0.997]

Running SMT solver (22)... 0.005 sec
Checking... Minimizing (18,64)... (18,19), 0.027 sec
Inserting... 0.000 sec
[21 ph, smt 0.312, isect 0.597, ins 0.027, tot 1.033]

Running SMT solver (23)... 0.004 sec
Checking... Minimizing (18,64)... (18,21), 0.029 sec
Inserting... 0.003 sec
[22 ph, smt 0.316, isect 0.626, ins 0.030, tot 1.074]

Running SMT solver (24)... 0.005 sec
Checking... Minimizing (19,68)... (19,16), 0.028 sec
Inserting... 0.003 sec
[23 ph, smt 0.321, isect 0.654, ins 0.033, tot 1.113]

Running SMT solver (25)... 0.005 sec
Checking... Minimizing (19,66)... (19,16), 0.026 sec
Inserting... 0.005 sec
[24 ph, smt 0.326, isect 0.680, ins 0.038, tot 1.153]

Running SMT solver (26)... 0.004 sec
Checking... Minimizing (19,66)... (19,17), 0.027 sec
Inserting... 0.005 sec
[25 ph, smt 0.330, isect 0.708, ins 0.043, tot 1.194]

Running SMT solver (27)... 0.007 sec
Checking... Minimizing (19,66)... (19,15), 0.029 sec
Inserting... 0.000 sec
[26 ph, smt 0.338, isect 0.737, ins 0.044, tot 1.234]

Running SMT solver (28)... 0.012 sec
Checking... Minimizing (19,66)... (19,20), 0.029 sec
Inserting... 0.001 sec
[27 ph, smt 0.350, isect 0.766, ins 0.044, tot 1.281]

Running SMT solver (29)... 0.008 sec
Checking... Minimizing (18,62)... (18,21), 0.030 sec
Inserting... 0.007 sec
[28 ph, smt 0.358, isect 0.796, ins 0.051, tot 1.329]

Running SMT solver (30)... 0.008 sec
Checking... Minimizing (18,61)... (18,23), 0.029 sec
Inserting... 0.008 sec
[29 ph, smt 0.366, isect 0.825, ins 0.059, tot 1.378]

Running SMT solver (31)... 0.008 sec
Checking... Minimizing (18,61)... (18,20), 0.027 sec
Inserting... 0.003 sec
[30 ph, smt 0.374, isect 0.853, ins 0.063, tot 1.421]

Running SMT solver (32)... 0.005 sec
Checking... Minimizing (18,61)... (18,22), 0.029 sec
Inserting... 0.010 sec
[31 ph, smt 0.379, isect 0.881, ins 0.073, tot 1.468]

Running SMT solver (33)... 0.009 sec
Checking... Minimizing (18,62)... (18,20), 0.029 sec
Inserting... 0.008 sec
[32 ph, smt 0.389, isect 0.911, ins 0.081, tot 1.519]

Running SMT solver (34)... 0.007 sec
Checking... Minimizing (18,62)... (18,18), 0.029 sec
Inserting... 0.004 sec
[33 ph, smt 0.395, isect 0.940, ins 0.084, tot 1.562]

Running SMT solver (35)... 0.007 sec
Checking... Minimizing (19,65)... (19,23), 0.028 sec
Inserting... 0.004 sec
[34 ph, smt 0.403, isect 0.967, ins 0.088, tot 1.605]

Running SMT solver (36)... 0.004 sec
Checking... Minimizing (18,62)... (18,24), 0.030 sec
Inserting... 0.008 sec
[35 ph, smt 0.407, isect 0.997, ins 0.096, tot 1.651]

Running SMT solver (37)... 0.004 sec
Checking... Minimizing (18,62)... (18,22), 0.028 sec
Inserting... 0.003 sec
[36 ph, smt 0.412, isect 1.025, ins 0.100, tot 1.691]

Running SMT solver (38)... 0.005 sec
Checking... Minimizing (18,62)... (18,25), 0.029 sec
Inserting... 0.006 sec
[37 ph, smt 0.416, isect 1.055, ins 0.106, tot 1.735]

Running SMT solver (39)... 0.012 sec
Checking... Minimizing (19,65)... (19,22), 0.031 sec
Inserting... 0.001 sec
[38 ph, smt 0.428, isect 1.085, ins 0.107, tot 1.783]

Running SMT solver (40)... 0.016 sec
Checking... Minimizing (19,66)... (19,17), 0.029 sec
Inserting... 0.001 sec
[39 ph, smt 0.445, isect 1.114, ins 0.108, tot 1.833]

Running SMT solver (41)... 0.005 sec
Checking... Minimizing (19,65)... (19,21), 0.030 sec
Inserting... 0.008 sec
[39 ph, smt 0.449, isect 1.144, ins 0.116, tot 1.880]

Running SMT solver (42)... 0.009 sec
Checking... Minimizing (19,66)... (19,18), 0.030 sec
Inserting... 0.004 sec
[39 ph, smt 0.458, isect 1.174, ins 0.120, tot 1.926]

Running SMT solver (43)... 0.019 sec
Checking... Minimizing (22,60)... (21,14), 0.023 sec
Inserting... 0.001 sec
[40 ph, smt 0.477, isect 1.197, ins 0.121, tot 1.972]

Running SMT solver (44)... 0.017 sec
Checking... Minimizing (24,62)... (23,3), 0.017 sec
Inserting... 0.005 sec
[41 ph, smt 0.494, isect 1.214, ins 0.125, tot 2.015]

Running SMT solver (45)... 0.026 sec
Checking... Minimizing (19,67)... (19,14), 0.028 sec
Inserting... 0.001 sec
[42 ph, smt 0.520, isect 1.242, ins 0.126, tot 2.074]

Running SMT solver (46)... 0.006 sec
Checking... Minimizing (19,67)... (19,13), 0.027 sec
Inserting... 0.003 sec
[42 ph, smt 0.526, isect 1.269, ins 0.129, tot 2.113]

Running SMT solver (47)... 0.028 sec
Checking... Minimizing (19,68)... (19,21), 0.029 sec
Inserting... 0.001 sec
[43 ph, smt 0.553, isect 1.298, ins 0.130, tot 2.175]

Running SMT solver (48)... 0.007 sec
Checking... Minimizing (19,67)... (19,18), 0.029 sec
Inserting... 0.001 sec
[44 ph, smt 0.561, isect 1.327, ins 0.131, tot 2.216]

Running SMT solver (49)... 0.025 sec
Checking... Minimizing (19,64)... (19,14), 0.024 sec
Inserting... 0.001 sec
[45 ph, smt 0.586, isect 1.350, ins 0.132, tot 2.270]

Running SMT solver (50)... 0.006 sec
Checking... Minimizing (18,64)... (18,20), 0.032 sec
Inserting... 0.004 sec
[45 ph, smt 0.592, isect 1.382, ins 0.136, tot 2.316]

Running SMT solver (51)... 0.005 sec
Checking... Minimizing (18,64)... (18,21), 0.030 sec
Inserting... 0.006 sec
[46 ph, smt 0.597, isect 1.412, ins 0.142, tot 2.361]

Running SMT solver (52)... 0.011 sec
Checking... Minimizing (19,68)... (19,15), 0.027 sec
Inserting... 0.001 sec
[47 ph, smt 0.608, isect 1.439, ins 0.143, tot 2.403]

Running SMT solver (53)... 0.006 sec
Checking... Minimizing (18,65)... (18,19), 0.028 sec
Inserting... 0.001 sec
[48 ph, smt 0.613, isect 1.467, ins 0.144, tot 2.441]

Running SMT solver (54)... 0.008 sec
Checking... Minimizing (19,68)... (19,16), 0.028 sec
Inserting... 0.001 sec
[49 ph, smt 0.621, isect 1.494, ins 0.144, tot 2.482]

Running SMT solver (55)... 0.005 sec
Checking... Minimizing (19,67)... (19,22), 0.031 sec
Inserting... 0.004 sec
[50 ph, smt 0.627, isect 1.525, ins 0.148, tot 2.525]

Running SMT solver (56)... 0.008 sec
Checking... Minimizing (18,63)... (18,22), 0.029 sec
Inserting... 0.002 sec
[51 ph, smt 0.634, isect 1.554, ins 0.150, tot 2.568]

Running SMT solver (57)... 0.005 sec
Checking... Minimizing (18,64)... (18,21), 0.029 sec
Inserting... 0.001 sec
[52 ph, smt 0.640, isect 1.584, ins 0.151, tot 2.608]

Running SMT solver (58)... 0.021 sec
Checking... Minimizing (22,64)... (21,9), 0.022 sec
Inserting... 0.001 sec
[53 ph, smt 0.661, isect 1.605, ins 0.153, tot 2.656]

Running SMT solver (59)... 0.006 sec
Checking... Minimizing (22,61)... (21,10), 0.022 sec
Inserting... 0.001 sec
[54 ph, smt 0.667, isect 1.627, ins 0.154, tot 2.690]

Running SMT solver (60)... 0.001 sec
No more solutions found

54 polyhedra, 60 rounds, smt 0.669 sec, isect 1.627 sec, insert 0.154 sec, total 2.708 sec
Preprocessing 18.074 sec, super total 20.784 sec
