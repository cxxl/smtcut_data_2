
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:43+02:00
Command line: BIOMD0000000030 --cvc4
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=cvc4


Model: BIOMD0000000030
Logfile: db/BIOMD0000000030/ep11-cvc4-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^10.0 in 21 bags
Bag sizes: 8 8 8 4 12 32 1 1 1 1 1 1 1 2 2 1 2 2 9 5 16

Minimizing (8,0)... (8,0)


Running SMT solver (1)... 0.038 sec
Found point (-2,0,0,-3,-2,0,0,0,-2,-2,-2,-2,2,-1,-1,2,0,0)
Checking... Minimizing (17,57)... (17,4), 0.016 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x10 = 2
  eq10: -1 x4 -1 x6 +1 x12 = 1
  eq11: -1 x4 -1 x6 +1 x11 = 1
  eq12: -1 x1 -1 x6 +1 x17 = 2
  eq13: -1 x2 -1 x6 +1 x13 = 2
  eq14: -1 x12 = 2
  eq15: -1 x4 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: -1 x1 -1 x5 +1 x18 >= 1
  ie1: +1 x1 >= -3
  ie2: +1 x5 >= -2
  ie3: +1 x18 >= -2
END

0.000 sec
[1 ph, smt 0.038, isect 0.016, ins 0.000, tot 0.093]

Running SMT solver (2)... 0.003 sec
Found point (-7/4,0,0,-3,-2,0,0,0,-7/4,-7/4,-2,-2,2,-1,-1,2,1/4,1/4)
Checking... Minimizing (17,57)... (17,3), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x4 -1 x6 +1 x12 = 1
  eq10: -1 x4 -1 x6 +1 x11 = 1
  eq11: -1 x1 -1 x6 +1 x17 = 2
  eq12: -1 x2 -1 x6 +1 x13 = 2
  eq13: -1 x12 = 2
  eq14: -1 x5 = 2
  eq15: -1 x4 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: -1 x1 -1 x5 +1 x7 >= 3
  ie1: +1 x10 >= -2
  ie2: +1 x18 >= -2
END

0.001 sec
[2 ph, smt 0.041, isect 0.027, ins 0.001, tot 0.112]

Running SMT solver (3)... 0.005 sec
Found point (-3/4,0,0,-3,-2,0,0,0,-3/4,-3/4,-2,-2,2,-1,-1,2,1,1)
Checking... Minimizing (17,58)... (17,3), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x12 +1 x14 = 1
  eq3: -1 x1 -1 x5 +1 x10 = 2
  eq4: -1 x2 -1 x5 +1 x7 = 2
  eq5: -1 x3 -1 x6 +1 x16 = 2
  eq6: +1 x16 -1 x17 = 1
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x4 -1 x6 +1 x12 = 1
  eq10: -1 x4 -1 x6 +1 x11 = 1
  eq11: -1 x2 -1 x6 +1 x13 = 2
  eq12: -1 x12 = 2
  eq13: -1 x5 = 2
  eq14: -1 x4 = 3
  eq15: +1 x13 -1 x18 = 1
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: -1 x1 -1 x5 +1 x18 >= 1
  ie1: -1 x3 -1 x5 +1 x9 >= 1
  ie2: +1 x10 >= -2
END

0.000 sec
[3 ph, smt 0.046, isect 0.038, ins 0.001, tot 0.132]

Running SMT solver (4)... 0.008 sec
Found point (-3,-3/4,-3/4,-7/4,-1,-1,1/4,1/4,-2,-2,-7/4,-7/4,1/4,-3/4,-3/4,1/4,-2,-2)
Checking... Minimizing (17,57)... (17,4), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x10 = 2
  eq10: -1 x4 -1 x6 +1 x12 = 1
  eq11: -1 x4 -1 x6 +1 x11 = 1
  eq12: -1 x1 -1 x6 +1 x17 = 2
  eq13: -1 x2 -1 x6 +1 x13 = 2
  eq14: -1 x18 = 2
  eq15: -1 x1 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: +1 x12 >= -3
  ie1: -1 x4 -1 x6 +1 x17 >= 0
  ie2: +1 x13 >= -2
  ie3: +1 x11 >= -2
END

0.000 sec
[4 ph, smt 0.054, isect 0.049, ins 0.002, tot 0.154]

Running SMT solver (5)... 0.006 sec
Found point (-3,0,0,-3/4,-1,-1,1,1,-2,-2,-3/4,-3/4,1,1/4,1/4,1,-2,-2)
Checking... Minimizing (17,54)... (17,3), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x8 = 2
  eq8: -1 x10 = 2
  eq9: +1 x3 +1 x5 -1 x10 = 1
  eq10: -1 x4 -1 x6 +1 x11 = 1
  eq11: -1 x1 -1 x6 +1 x17 = 2
  eq12: -1 x2 -1 x6 +1 x13 = 2
  eq13: -1 x18 = 2
  eq14: -1 x1 = 3
  eq15: -1 x4 -1 x6 +1 x12 = 1
  eq16: +1 x2 +1 x5 -1 x9 = 1
  ie0: +1 x12 >= -3
  ie1: -1 x4 -1 x6 +1 x7 >= 2
  ie2: -1 x3 -1 x5 +1 x15 >= 1
END

0.000 sec
[5 ph, smt 0.059, isect 0.061, ins 0.002, tot 0.174]

Running SMT solver (6)... 0.007 sec
Found point (-3,-1,-1,-9/4,-1,-3/4,0,0,-2,-2,-2,-2,1/4,-1,-1,1/4,-7/4,-7/4)
Checking... Minimizing (17,57)... (17,3), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x10 = 2
  eq10: -1 x4 -1 x6 +1 x12 = 1
  eq11: -1 x4 -1 x6 +1 x11 = 1
  eq12: -1 x1 -1 x6 +1 x17 = 2
  eq13: -1 x2 -1 x6 +1 x13 = 2
  eq14: -1 x12 = 2
  eq15: -1 x1 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: +1 x4 >= -3
  ie1: -1 x1 -1 x5 +1 x18 >= 1
  ie2: +1 x18 >= -2
END

0.000 sec
[6 ph, smt 0.066, isect 0.071, ins 0.002, tot 0.195]

Running SMT solver (7)... 0.001 sec
No more solutions found



Solution:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x10 = 2
  eq10: -1 x4 -1 x6 +1 x12 = 1
  eq11: -1 x4 -1 x6 +1 x11 = 1
  eq12: -1 x1 -1 x6 +1 x17 = 2
  eq13: -1 x2 -1 x6 +1 x13 = 2
  eq14: -1 x12 = 2
  eq15: -1 x1 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: +1 x4 >= -3
  ie1: -1 x1 -1 x5 +1 x18 >= 1
  ie2: +1 x18 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x8 = 2
  eq8: -1 x10 = 2
  eq9: +1 x3 +1 x5 -1 x10 = 1
  eq10: -1 x4 -1 x6 +1 x11 = 1
  eq11: -1 x1 -1 x6 +1 x17 = 2
  eq12: -1 x2 -1 x6 +1 x13 = 2
  eq13: -1 x18 = 2
  eq14: -1 x1 = 3
  eq15: -1 x4 -1 x6 +1 x12 = 1
  eq16: +1 x2 +1 x5 -1 x9 = 1
  ie0: +1 x12 >= -3
  ie1: -1 x4 -1 x6 +1 x7 >= 2
  ie2: -1 x3 -1 x5 +1 x15 >= 1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x10 = 2
  eq10: -1 x4 -1 x6 +1 x12 = 1
  eq11: -1 x4 -1 x6 +1 x11 = 1
  eq12: -1 x1 -1 x6 +1 x17 = 2
  eq13: -1 x2 -1 x6 +1 x13 = 2
  eq14: -1 x18 = 2
  eq15: -1 x1 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: +1 x12 >= -3
  ie1: -1 x4 -1 x6 +1 x17 >= 0
  ie2: +1 x13 >= -2
  ie3: +1 x11 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x12 +1 x14 = 1
  eq3: -1 x1 -1 x5 +1 x10 = 2
  eq4: -1 x2 -1 x5 +1 x7 = 2
  eq5: -1 x3 -1 x6 +1 x16 = 2
  eq6: +1 x16 -1 x17 = 1
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x4 -1 x6 +1 x12 = 1
  eq10: -1 x4 -1 x6 +1 x11 = 1
  eq11: -1 x2 -1 x6 +1 x13 = 2
  eq12: -1 x12 = 2
  eq13: -1 x5 = 2
  eq14: -1 x4 = 3
  eq15: +1 x13 -1 x18 = 1
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: -1 x1 -1 x5 +1 x18 >= 1
  ie1: -1 x3 -1 x5 +1 x9 >= 1
  ie2: +1 x10 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x4 -1 x6 +1 x12 = 1
  eq10: -1 x4 -1 x6 +1 x11 = 1
  eq11: -1 x1 -1 x6 +1 x17 = 2
  eq12: -1 x2 -1 x6 +1 x13 = 2
  eq13: -1 x12 = 2
  eq14: -1 x5 = 2
  eq15: -1 x4 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: -1 x1 -1 x5 +1 x7 >= 3
  ie1: +1 x10 >= -2
  ie2: +1 x18 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x9 = 2
  eq1: -1 x11 +1 x15 = 1
  eq2: -1 x1 -1 x6 +1 x18 = 2
  eq3: -1 x12 +1 x14 = 1
  eq4: -1 x1 -1 x5 +1 x10 = 2
  eq5: -1 x2 -1 x5 +1 x7 = 2
  eq6: -1 x3 -1 x6 +1 x16 = 2
  eq7: -1 x3 -1 x5 +1 x15 = 1
  eq8: -1 x3 -1 x5 +1 x8 = 2
  eq9: -1 x10 = 2
  eq10: -1 x4 -1 x6 +1 x12 = 1
  eq11: -1 x4 -1 x6 +1 x11 = 1
  eq12: -1 x1 -1 x6 +1 x17 = 2
  eq13: -1 x2 -1 x6 +1 x13 = 2
  eq14: -1 x12 = 2
  eq15: -1 x4 = 3
  eq16: -1 x2 -1 x5 +1 x14 = 1
  ie0: -1 x1 -1 x5 +1 x18 >= 1
  ie1: +1 x1 >= -3
  ie2: +1 x5 >= -2
  ie3: +1 x18 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end



6 polyhedra, 7 rounds, smt 0.067 sec, isect 0.071 sec, insert 0.002 sec, total 0.198 sec
Preprocessing 0.006 sec, super total 0.205 sec
Comparing... 0.042 sec.  Solution matches input.
