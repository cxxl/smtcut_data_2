
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-03T22:36:35+02:00
Command line: ..\trop\ptcut.py BIOMD0000000430 -e11 --no-cache=n --maxruntime 3600 --maxmem 41393M --comment "concurrent=1, timeout=3600"

----------------------------------------------------------------------
Solving BIOMD0000000430 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=False, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=1, timeout=3600
Random seed: 241USTTKXFTP4
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 27
Formulas: 29
Order: 2 4 8 10 11 13 15 16 17 19 20 21 22 0 5 14 24 6 23 25 26 3 28 18 7 27 12 9 1
Possible combinations: 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 3 * 6 * 7 * 7 * 8 * 8 * 9 * 12 * 13 * 16 * 18 * 20 = 7,303,955,742,720 (10^13)
 Applying common planes (codim=12, hs=40)...
 [1/16 (#0)]: 2 => 1 (1 incl), dim=15.0, hs=40.0
  Removed: 0.1
 [2/16 (#5)]: 2 => 1 (1 incl), dim=15.0, hs=40.0
  Removed: 5.0
 [3/16 (#14)]: 2 => 1 (1 incl), dim=15.0, hs=41.0
  Removed: 14.1
 [4/16 (#24)]: 2 => 1 (1 incl), dim=14.0, hs=40.0
  Removed: 24.1
 [5/16 (#6)]: 3 => 1 (2 incl), dim=15.0, hs=40.0
  Removed: 6.1,6.2
 [6/16 (#23)]: 6 => 5 (1 incl), dim=14.0, hs=36.4
  Removed: 23.5
 [7/16 (#25)]: 7 => 7 (-), dim=14.0, hs=35.6
 [8/16 (#26)]: 7 => 5 (1 empty, 1 incl), dim=14.0, hs=38.0
  Removed: 26.1,26.2
 [9/16 (#3)]: 8 => 3 (2 empty, 3 incl), dim=14.3, hs=37.7
  Removed: 3.0,3.2,3.3,3.5,3.7
 [10/16 (#28)]: 8 => 4 (4 empty), dim=14.0, hs=35.2
  Removed: 28.0,28.1,28.2,28.4
 [11/16 (#18)]: 9 => 3 (6 incl), dim=15.0, hs=37.0
  Removed: 18.0,18.2,18.4,18.5,18.6,18.7
 [12/16 (#7)]: 12 => 3 (9 incl), dim=15.0, hs=39.7
  Removed: 7.1,7.2,7.3,7.5,7.6,7.7,7.8,7.9,7.10
 [13/16 (#27)]: 13 => 4 (9 empty), dim=14.0, hs=34.5
  Removed: 27.0,27.1,27.2,27.3,27.4,27.5,27.6,27.8,27.10
 [14/16 (#12)]: 16 => 4 (12 incl), dim=15.0, hs=36.2
  Removed: 12.0,12.1,12.2,12.5,12.6,12.7,12.8,12.9,12.11,12.12,12.14,12.15
 [15/16 (#9)]: 18 => 5 (3 empty, 10 incl), dim=14.4, hs=36.2
  Removed: 9.0,9.2,9.3,9.4,9.6,9.7,9.8,9.10,9.11,9.13,9.14,9.16,9.17
 [16/16 (#1)]: 20 => 4 (16 incl), dim=15.0, hs=36.8
  Removed: 1.0,1.1,1.2,1.3,1.4,1.5,1.7,1.9,1.10,1.11,1.12,1.14,1.15,1.16,1.17,1.18
 Applying common planes (codim=13, hs=41)...
 [1/11 (#23)]: 5 => 5 (-), dim=13.0, hs=37.2
 [2/11 (#25)]: 7 => 7 (-), dim=13.0, hs=36.6
 [3/11 (#26)]: 5 => 5 (-), dim=13.0, hs=38.6
 [4/11 (#3)]: 3 => 3 (-), dim=13.3, hs=38.7
 [5/11 (#28)]: 4 => 4 (-), dim=13.0, hs=36.2
 [6/11 (#18)]: 3 => 3 (-), dim=14.0, hs=38.0
 [7/11 (#7)]: 3 => 3 (-), dim=14.0, hs=40.7
 [8/11 (#27)]: 4 => 4 (-), dim=13.0, hs=35.5
 [9/11 (#12)]: 4 => 4 (-), dim=14.0, hs=37.2
 [10/11 (#9)]: 5 => 5 (-), dim=13.4, hs=37.2
 [11/11 (#1)]: 4 => 4 (-), dim=14.0, hs=37.8
 Savings: factor 1,207,664 (10^6), 18 formulas
[1/10 (#23, #3)]: 5 * 3 = 15 => 14 (1 incl), dim=12.3, hs=36.0
[2/10 (w1, #18)]: 14 * 3 = 42 => 32 (10 incl), dim=12.3, hs=35.2
[3/10 (w2, #7)]: 32 * 3 = 96 => 90 (3 empty, 3 incl), dim=12.3, hs=36.1
[4/10 (w3, #28)]: 90 * 4 = 360 => 161 (102 empty, 97 incl), dim=11.2, hs=35.1
[5/10 (w4, #27)]: 161 * 4 = 644 => 124 (436 empty, 84 incl), dim=10.2, hs=33.5
[6/10 (w5, #12)]: 124 * 4 = 496 => 201 (197 empty, 98 incl), dim=10.3, hs=33.4
[7/10 (w6, #1)]: 201 * 4 = 804 => 305 (273 empty, 226 incl), dim=10.3, hs=33.4
[8/10 (w7, #26)]: 305 * 5 = 1525 => 432 (526 empty, 567 incl), dim=9.5, hs=32.0
[9/10 (w8, #9)]: 432 * 5 = 2160 => 669 (893 empty, 598 incl), dim=9.2, hs=32.1
[10/10 (w9, #25)]: 669 * 7 = 4683 => 1676 (1834 empty, 1173 incl), dim=8.2, hs=31.9
Total time: 58.050 sec, total intersections: 11,007, total inclusion tests: 1,339,927
Common plane time: 0.900 sec
Solutions: 1676, dim=8.2, hs=31.9, max=1676, maxin=4683
f-vector: [0, 0, 0, 0, 0, 0, 4, 372, 646, 530, 124]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.634 GiB, pagefile 0.649 GiB
Memory: pmem(rss=159932416, vms=154738688, num_page_faults=594146, peak_wset=681283584, wset=159932416, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=188064, nonpaged_pool=87560, pagefile=154738688, peak_pagefile=696647680, private=154738688)
