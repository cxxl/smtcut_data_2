
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-25T11:48:18+02:00
Command line: --no-dump --no-comp BIOMD0000000666 --cvc4 --pp
Flags: ppone ppsmt pp2x2 no-dump justone maxadd=20 solver=cvc4


Model: BIOMD0000000666
Logfile: db/BIOMD0000000666/ep11-osx20-cvc4-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^23.5 in 44 bags
Bag sizes: 1 2 8 8 4 4 12 12 8 8 8 8 2 2 6 6 20 20 2 2 4 4 4 4 4 4 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2

Filtering bags...
Collecting...
Bag 1/43, 2 polyhedra
Bag 2/43, 8 polyhedra
Bag 3/43, 8 polyhedra
Bag 4/43, 4 polyhedra
Bag 5/43, 4 polyhedra
Bag 6/43, 12 polyhedra
Bag 7/43, 12 polyhedra
Bag 8/43, 8 polyhedra
Bag 9/43, 8 polyhedra
Bag 10/43, 8 polyhedra
Bag 11/43, 8 polyhedra
Bag 12/43, 2 polyhedra
Bag 13/43, 2 polyhedra
Bag 14/43, 6 polyhedra
Bag 15/43, 6 polyhedra
Bag 16/43, 20 polyhedra
Bag 17/43, 20 polyhedra
Bag 18/43, 2 polyhedra
Bag 19/43, 2 polyhedra
Bag 20/43, 4 polyhedra
Bag 21/43, 4 polyhedra
Bag 22/43, 4 polyhedra
Bag 23/43, 4 polyhedra
Bag 24/43, 4 polyhedra
Bag 25/43, 4 polyhedra
Bag 26/43, 2 polyhedra
Bag 27/43, 2 polyhedra
Bag 28/43, 2 polyhedra
Bag 29/43, 2 polyhedra
Bag 30/43, 2 polyhedra
Bag 31/43, 2 polyhedra
Bag 32/43, 2 polyhedra
Bag 33/43, 2 polyhedra
Bag 34/43, 2 polyhedra
Bag 35/43, 2 polyhedra
Bag 36/43, 2 polyhedra
Bag 37/43, 2 polyhedra
Bag 38/43, 2 polyhedra
Bag 39/43, 2 polyhedra
Bag 40/43, 2 polyhedra
Bag 41/43, 2 polyhedra
Bag 42/43, 2 polyhedra
Bag 43/43, 2 polyhedra
Possible combinations after preprocessing: 10^23.5 in 44 bags
Bag sizes: 1 2 8 8 4 4 12 12 8 8 8 8 2 2 6 6 20 20 2 2 4 4 4 4 4 4 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
Time: 0.101 sec

Checking for superfluous polyhedra...
Bag 1/44, 20 polyhedra:
  Polyhedron 1... superfluous, 0.037 sec
  Polyhedron 2... superfluous, 0.010 sec
  Polyhedron 3... superfluous, 0.006 sec
  Polyhedron 4... required, 0.015 sec
  Polyhedron 5... superfluous, 0.002 sec
  Polyhedron 6... superfluous, 0.007 sec
  Polyhedron 7... superfluous, 0.007 sec
  Polyhedron 8... superfluous, 0.007 sec
  Polyhedron 9... superfluous, 0.001 sec
  Polyhedron 10... superfluous, 0.005 sec
  Polyhedron 11... superfluous, 0.005 sec
  Polyhedron 12... superfluous, 0.005 sec
  Polyhedron 13... superfluous, 0.006 sec
  Polyhedron 14... superfluous, 0.006 sec
  Polyhedron 15... superfluous, 0.006 sec
  Polyhedron 16... superfluous, 0.007 sec
  Polyhedron 17... superfluous, 0.002 sec
  Polyhedron 18... superfluous, 0.005 sec
  Polyhedron 19... superfluous, 0.005 sec
  Polyhedron 20... superfluous, 0.005 sec
  => 1 polyhedra left
Bag 2/44, 20 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... superfluous, 0.001 sec
  Polyhedron 4... required, 0.017 sec
  Polyhedron 5... superfluous, 0.001 sec
  Polyhedron 6... superfluous, 0.001 sec
  Polyhedron 7... superfluous, 0.001 sec
  Polyhedron 8... superfluous, 0.001 sec
  Polyhedron 9... superfluous, 0.001 sec
  Polyhedron 10... superfluous, 0.001 sec
  Polyhedron 11... superfluous, 0.001 sec
  Polyhedron 12... superfluous, 0.001 sec
  Polyhedron 13... superfluous, 0.001 sec
  Polyhedron 14... superfluous, 0.001 sec
  Polyhedron 15... superfluous, 0.001 sec
  Polyhedron 16... superfluous, 0.001 sec
  Polyhedron 17... superfluous, 0.001 sec
  Polyhedron 18... superfluous, 0.001 sec
  Polyhedron 19... superfluous, 0.001 sec
  Polyhedron 20... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 3/44, 12 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.007 sec
  Polyhedron 3... superfluous, 0.005 sec
  Polyhedron 4... required, 0.019 sec
  Polyhedron 5... required, 0.014 sec
  Polyhedron 6... superfluous, 0.006 sec
  Polyhedron 7... superfluous, 0.005 sec
  Polyhedron 8... superfluous, 0.006 sec
  Polyhedron 9... superfluous, 0.006 sec
  Polyhedron 10... superfluous, 0.006 sec
  Polyhedron 11... superfluous, 0.006 sec
  Polyhedron 12... superfluous, 0.006 sec
  => 2 polyhedra left
Bag 4/44, 12 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... superfluous, 0.001 sec
  Polyhedron 4... required, 0.015 sec
  Polyhedron 5... required, 0.012 sec
  Polyhedron 6... superfluous, 0.001 sec
  Polyhedron 7... superfluous, 0.001 sec
  Polyhedron 8... superfluous, 0.001 sec
  Polyhedron 9... superfluous, 0.001 sec
  Polyhedron 10... superfluous, 0.001 sec
  Polyhedron 11... superfluous, 0.001 sec
  Polyhedron 12... superfluous, 0.001 sec
  => 2 polyhedra left
Bag 5/44, 8 polyhedra:
  Polyhedron 1... required, 0.017 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... superfluous, 0.005 sec
  Polyhedron 4... superfluous, 0.005 sec
  Polyhedron 5... superfluous, 0.005 sec
  Polyhedron 6... superfluous, 0.005 sec
  Polyhedron 7... superfluous, 0.007 sec
  Polyhedron 8... superfluous, 0.007 sec
  => 1 polyhedra left
Bag 6/44, 8 polyhedra:
  Polyhedron 1... required, 0.026 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... superfluous, 0.001 sec
  Polyhedron 4... superfluous, 0.001 sec
  Polyhedron 5... superfluous, 0.001 sec
  Polyhedron 6... superfluous, 0.001 sec
  Polyhedron 7... superfluous, 0.001 sec
  Polyhedron 8... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 7/44, 8 polyhedra:
  Polyhedron 1... superfluous, 0.013 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.016 sec
  Polyhedron 4... required, 0.013 sec
  Polyhedron 5... required, 0.017 sec
  Polyhedron 6... required, 0.016 sec
  Polyhedron 7... superfluous, 0.006 sec
  Polyhedron 8... superfluous, 0.005 sec
  => 4 polyhedra left
Bag 8/44, 8 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... required, 0.017 sec
  Polyhedron 4... required, 0.013 sec
  Polyhedron 5... required, 0.015 sec
  Polyhedron 6... required, 0.015 sec
  Polyhedron 7... superfluous, 0.001 sec
  Polyhedron 8... superfluous, 0.001 sec
  => 4 polyhedra left
Bag 9/44, 8 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... superfluous, 0.008 sec
  Polyhedron 3... required, 0.016 sec
  Polyhedron 4... superfluous, 0.006 sec
  Polyhedron 5... required, 0.012 sec
  Polyhedron 6... superfluous, 0.006 sec
  Polyhedron 7... superfluous, 0.006 sec
  Polyhedron 8... superfluous, 0.005 sec
  => 2 polyhedra left
Bag 10/44, 8 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... required, 0.014 sec
  Polyhedron 4... superfluous, 0.001 sec
  Polyhedron 5... required, 0.014 sec
  Polyhedron 6... superfluous, 0.001 sec
  Polyhedron 7... superfluous, 0.001 sec
  Polyhedron 8... superfluous, 0.001 sec
  => 2 polyhedra left
Bag 11/44, 6 polyhedra:
  Polyhedron 1... required, 0.018 sec
  Polyhedron 2... superfluous, 0.012 sec
  Polyhedron 3... superfluous, 0.006 sec
  Polyhedron 4... superfluous, 0.017 sec
  Polyhedron 5... superfluous, 0.011 sec
  Polyhedron 6... superfluous, 0.013 sec
  => 1 polyhedra left
Bag 12/44, 6 polyhedra:
  Polyhedron 1... required, 0.017 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... superfluous, 0.001 sec
  Polyhedron 4... superfluous, 0.001 sec
  Polyhedron 5... superfluous, 0.001 sec
  Polyhedron 6... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 13/44, 4 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... superfluous, 0.004 sec
  Polyhedron 3... required, 0.013 sec
  Polyhedron 4... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 14/44, 4 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... required, 0.014 sec
  Polyhedron 4... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 15/44, 4 polyhedra:
  Polyhedron 1... superfluous, 0.004 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... required, 0.011 sec
  Polyhedron 4... superfluous, 0.002 sec
  => 1 polyhedra left
Bag 16/44, 4 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... required, 0.011 sec
  Polyhedron 4... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 17/44, 4 polyhedra:
  Polyhedron 1... required, 0.012 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... superfluous, 0.005 sec
  Polyhedron 4... superfluous, 0.002 sec
  => 1 polyhedra left
Bag 18/44, 4 polyhedra:
  Polyhedron 1... required, 0.016 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... superfluous, 0.001 sec
  Polyhedron 4... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 19/44, 4 polyhedra:
  Polyhedron 1... required, 0.011 sec
  Polyhedron 2... superfluous, 0.004 sec
  Polyhedron 3... superfluous, 0.005 sec
  Polyhedron 4... superfluous, 0.004 sec
  => 1 polyhedra left
Bag 20/44, 4 polyhedra:
  Polyhedron 1... required, 0.010 sec
  Polyhedron 2... superfluous, 0.001 sec
  Polyhedron 3... superfluous, 0.001 sec
  Polyhedron 4... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 21/44, 2 polyhedra:
  Polyhedron 1... superfluous, 0.001 sec
  Polyhedron 2... required, 0.011 sec
  => 1 polyhedra left
Bag 22/44, 2 polyhedra:
  Polyhedron 1... required, 0.009 sec
  Polyhedron 2... superfluous, 0.004 sec
  => 1 polyhedra left
Bag 23/44, 2 polyhedra:
  Polyhedron 1... required, 0.009 sec
  Polyhedron 2... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 24/44, 2 polyhedra:
  Polyhedron 1... required, 0.008 sec
  Polyhedron 2... superfluous, 0.004 sec
  => 1 polyhedra left
Bag 25/44, 2 polyhedra:
  Polyhedron 1... required, 0.009 sec
  Polyhedron 2... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 26/44, 2 polyhedra:
  Polyhedron 1... required, 0.012 sec
  Polyhedron 2... superfluous, 0.004 sec
  => 1 polyhedra left
Bag 27/44, 2 polyhedra:
  Polyhedron 1... required, 0.012 sec
  Polyhedron 2... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 28/44, 2 polyhedra:
  Polyhedron 1... required, 0.009 sec
  Polyhedron 2... required, 0.008 sec
  => 2 polyhedra left
Bag 29/44, 2 polyhedra:
  Polyhedron 1... required, 0.007 sec
  Polyhedron 2... required, 0.008 sec
  => 2 polyhedra left
Bag 30/44, 2 polyhedra:
  Polyhedron 1... required, 0.009 sec
  Polyhedron 2... required, 0.009 sec
  => 2 polyhedra left
Bag 31/44, 2 polyhedra:
  Polyhedron 1... superfluous, 0.004 sec
  Polyhedron 2... required, 0.011 sec
  => 1 polyhedra left
Bag 32/44, 2 polyhedra:
  Polyhedron 1... required, 0.009 sec
  Polyhedron 2... required, 0.006 sec
  => 2 polyhedra left
Bag 33/44, 2 polyhedra:
  Polyhedron 1... required, 0.010 sec
  Polyhedron 2... superfluous, 0.004 sec
  => 1 polyhedra left
Bag 34/44, 2 polyhedra:
  Polyhedron 1... required, 0.007 sec
  Polyhedron 2... superfluous, 0.011 sec
  => 1 polyhedra left
Bag 35/44, 2 polyhedra:
  Polyhedron 1... required, 0.010 sec
  Polyhedron 2... required, 0.008 sec
  => 2 polyhedra left
Bag 36/44, 2 polyhedra:
  Polyhedron 1... required, 0.012 sec
  Polyhedron 2... superfluous, 0.001 sec
  => 1 polyhedra left
Bag 37/44, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 38/44, 2 polyhedra:
  Polyhedron 1... required, 0.006 sec
  Polyhedron 2... superfluous, 0.003 sec
  => 1 polyhedra left
Bag 39/44, 2 polyhedra:
  Polyhedron 1... required, 0.004 sec
  Polyhedron 2... required, 0.008 sec
  => 2 polyhedra left
Bag 40/44, 2 polyhedra:
  Polyhedron 1... required, 0.005 sec
  Polyhedron 2... required, 0.005 sec
  => 2 polyhedra left
Bag 41/44, 2 polyhedra:
  Polyhedron 1... required, 0.005 sec
  Polyhedron 2... superfluous, 0.003 sec
  => 1 polyhedra left
Bag 42/44, 2 polyhedra:
  Polyhedron 1... required, 0.008 sec
  Polyhedron 2... superfluous, 0.002 sec
  => 1 polyhedra left
Bag 43/44, 2 polyhedra:
  Polyhedron 1... required, 0.006 sec
  Polyhedron 2... required, 0.005 sec
  => 2 polyhedra left
Possible combinations after preprocessing: 10^4.8 in 15 bags
Bag sizes: 1 2 2 4 4 2 2 2 2 2 2 2 2 2 2
Time: 1.273 sec

Combining small bags...
Intersecting 2x4... 8 polyhedra
Intersecting 2x8... 16 polyhedra
Intersecting 2x16... 32 polyhedra
Intersecting 2x4... 8 polyhedra
Intersecting 2x8... 16 polyhedra
Intersecting 2x16... 32 polyhedra
Intersecting 2x2... 4 polyhedra
Intersecting 2x4... 8 polyhedra
Intersecting 2x8... 16 polyhedra
Intersecting 2x16... 32 polyhedra
Possible combinations after preprocessing: 10^4.8 in 5 bags
Bag sizes: 1 2 32 32 32
Time: 0.525 sec

Checking for superfluous polyhedra...
Bag 1/5, 32 polyhedra:
  Polyhedron 1... superfluous, 0.018 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.007 sec
  Polyhedron 4... required, 0.008 sec
  Polyhedron 5... superfluous, 0.001 sec
  Polyhedron 6... superfluous, 0.001 sec
  Polyhedron 7... superfluous, 0.001 sec
  Polyhedron 8... superfluous, 0.001 sec
  Polyhedron 9... superfluous, 0.006 sec
  Polyhedron 10... superfluous, 0.005 sec
  Polyhedron 11... superfluous, 0.001 sec
  Polyhedron 12... superfluous, 0.001 sec
  Polyhedron 13... superfluous, 0.006 sec
  Polyhedron 14... superfluous, 0.005 sec
  Polyhedron 15... superfluous, 0.006 sec
  Polyhedron 16... superfluous, 0.005 sec
  Polyhedron 17... required, 0.008 sec
  Polyhedron 18... superfluous, 0.005 sec
  Polyhedron 19... superfluous, 0.007 sec
  Polyhedron 20... superfluous, 0.006 sec
  Polyhedron 21... superfluous, 0.001 sec
  Polyhedron 22... superfluous, 0.001 sec
  Polyhedron 23... superfluous, 0.001 sec
  Polyhedron 24... superfluous, 0.001 sec
  Polyhedron 25... superfluous, 0.001 sec
  Polyhedron 26... superfluous, 0.005 sec
  Polyhedron 27... superfluous, 0.005 sec
  Polyhedron 28... superfluous, 0.005 sec
  Polyhedron 29... required, 0.006 sec
  Polyhedron 30... required, 0.006 sec
  Polyhedron 31... superfluous, 0.005 sec
  Polyhedron 32... superfluous, 0.017 sec
  => 5 polyhedra left
Bag 2/5, 32 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... required, 0.006 sec
  Polyhedron 4... required, 0.006 sec
  Polyhedron 5... superfluous, 0.005 sec
  Polyhedron 6... required, 0.006 sec
  Polyhedron 7... superfluous, 0.005 sec
  Polyhedron 8... superfluous, 0.007 sec
  Polyhedron 9... superfluous, 0.005 sec
  Polyhedron 10... superfluous, 0.005 sec
  Polyhedron 11... superfluous, 0.004 sec
  Polyhedron 12... superfluous, 0.005 sec
  Polyhedron 13... required, 0.006 sec
  Polyhedron 14... required, 0.005 sec
  Polyhedron 15... superfluous, 0.005 sec
  Polyhedron 16... superfluous, 0.005 sec
  Polyhedron 17... superfluous, 0.004 sec
  Polyhedron 18... superfluous, 0.005 sec
  Polyhedron 19... required, 0.005 sec
  Polyhedron 20... required, 0.006 sec
  Polyhedron 21... superfluous, 0.004 sec
  Polyhedron 22... required, 0.006 sec
  Polyhedron 23... superfluous, 0.004 sec
  Polyhedron 24... superfluous, 0.004 sec
  Polyhedron 25... superfluous, 0.005 sec
  Polyhedron 26... superfluous, 0.004 sec
  Polyhedron 27... superfluous, 0.004 sec
  Polyhedron 28... superfluous, 0.005 sec
  Polyhedron 29... required, 0.005 sec
  Polyhedron 30... required, 0.006 sec
  Polyhedron 31... superfluous, 0.005 sec
  Polyhedron 32... superfluous, 0.005 sec
  => 10 polyhedra left
Bag 3/5, 32 polyhedra:
  Polyhedron 1... required, 0.006 sec
  Polyhedron 2... superfluous, 0.015 sec
  Polyhedron 3... required, 0.005 sec
  Polyhedron 4... superfluous, 0.004 sec
  Polyhedron 5... required, 0.005 sec
  Polyhedron 6... superfluous, 0.004 sec
  Polyhedron 7... required, 0.005 sec
  Polyhedron 8... superfluous, 0.004 sec
  Polyhedron 9... required, 0.005 sec
  Polyhedron 10... superfluous, 0.004 sec
  Polyhedron 11... required, 0.004 sec
  Polyhedron 12... superfluous, 0.004 sec
  Polyhedron 13... required, 0.005 sec
  Polyhedron 14... superfluous, 0.004 sec
  Polyhedron 15... required, 0.004 sec
  Polyhedron 16... superfluous, 0.004 sec
  Polyhedron 17... required, 0.005 sec
  Polyhedron 18... superfluous, 0.004 sec
  Polyhedron 19... required, 0.004 sec
  Polyhedron 20... superfluous, 0.004 sec
  Polyhedron 21... required, 0.004 sec
  Polyhedron 22... superfluous, 0.003 sec
  Polyhedron 23... required, 0.004 sec
  Polyhedron 24... superfluous, 0.003 sec
  Polyhedron 25... required, 0.004 sec
  Polyhedron 26... superfluous, 0.004 sec
  Polyhedron 27... required, 0.004 sec
  Polyhedron 28... superfluous, 0.004 sec
  Polyhedron 29... required, 0.004 sec
  Polyhedron 30... required, 0.004 sec
  Polyhedron 31... required, 0.016 sec
  Polyhedron 32... required, 0.006 sec
  => 18 polyhedra left
Possible combinations after preprocessing: 1800 in 5 bags
Bag sizes: 5 10 18 2 1
Time: 0.502 sec

Combining small bags...
Intersecting 2x18... 36 polyhedra
Possible combinations after preprocessing: 1800 in 4 bags
Bag sizes: 1 5 10 36
Time: 0.135 sec

Checking for superfluous polyhedra...
Bag 1/4, 36 polyhedra:
  Polyhedron 1... required, 0.015 sec
  Polyhedron 2... required, 0.005 sec
  Polyhedron 3... required, 0.005 sec
  Polyhedron 4... required, 0.004 sec
  Polyhedron 5... required, 0.005 sec
  Polyhedron 6... required, 0.004 sec
  Polyhedron 7... required, 0.005 sec
  Polyhedron 8... required, 0.004 sec
  Polyhedron 9... required, 0.006 sec
  Polyhedron 10... required, 0.005 sec
  Polyhedron 11... required, 0.004 sec
  Polyhedron 12... required, 0.004 sec
  Polyhedron 13... required, 0.005 sec
  Polyhedron 14... required, 0.004 sec
  Polyhedron 15... required, 0.004 sec
  Polyhedron 16... required, 0.006 sec
  Polyhedron 17... required, 0.005 sec
  Polyhedron 18... required, 0.004 sec
  Polyhedron 19... superfluous, 0.003 sec
  Polyhedron 20... superfluous, 0.002 sec
  Polyhedron 21... superfluous, 0.002 sec
  Polyhedron 22... superfluous, 0.002 sec
  Polyhedron 23... superfluous, 0.002 sec
  Polyhedron 24... superfluous, 0.002 sec
  Polyhedron 25... superfluous, 0.002 sec
  Polyhedron 26... superfluous, 0.002 sec
  Polyhedron 27... required, 0.006 sec
  Polyhedron 28... required, 0.005 sec
  Polyhedron 29... required, 0.004 sec
  Polyhedron 30... required, 0.004 sec
  Polyhedron 31... required, 0.004 sec
  Polyhedron 32... required, 0.004 sec
  Polyhedron 33... required, 0.004 sec
  Polyhedron 34... required, 0.004 sec
  Polyhedron 35... required, 0.004 sec
  Polyhedron 36... required, 0.004 sec
  => 28 polyhedra left
Possible combinations after preprocessing: 1400 in 4 bags
Bag sizes: 28 10 5 1
Time: 0.166 sec

Minimizing (19,29)... (19,14)


Running SMT solver (1)... 0.016 sec
Checking... Minimizing (30,27)... (30,14), 0.016 sec
Inserting... 0.000 sec
[1 ph, smt 0.016, isect 0.016, ins 0.000, tot 0.048]

Running SMT solver (2)... 0.004 sec
Checking... Minimizing (30,27)... (30,12), 0.017 sec
Inserting... 0.000 sec
[2 ph, smt 0.020, isect 0.033, ins 0.000, tot 0.073]

Running SMT solver (3)... 0.004 sec
Checking... Minimizing (30,27)... (30,7), 0.016 sec
Inserting... 0.000 sec
[3 ph, smt 0.024, isect 0.049, ins 0.000, tot 0.096]

Running SMT solver (4)... 0.003 sec
Checking... Minimizing (30,27)... (30,8), 0.016 sec
Inserting... 0.000 sec
[4 ph, smt 0.027, isect 0.065, ins 0.000, tot 0.120]

Running SMT solver (5)... 0.003 sec
Checking... Minimizing (30,27)... (30,11), 0.017 sec
Inserting... 0.000 sec
[5 ph, smt 0.030, isect 0.082, ins 0.001, tot 0.144]

Running SMT solver (6)... 0.003 sec
Checking... Minimizing (30,27)... (30,14), 0.017 sec
Inserting... 0.000 sec
[6 ph, smt 0.033, isect 0.099, ins 0.001, tot 0.168]

Running SMT solver (7)... 0.003 sec
Checking... Minimizing (30,27)... (30,12), 0.017 sec
Inserting... 0.000 sec
[7 ph, smt 0.037, isect 0.116, ins 0.001, tot 0.193]

Running SMT solver (8)... 0.004 sec
Checking... Minimizing (30,27)... (30,8), 0.016 sec
Inserting... 0.000 sec
[8 ph, smt 0.040, isect 0.132, ins 0.002, tot 0.218]

Running SMT solver (9)... 0.003 sec
Checking... Minimizing (30,27)... (30,11), 0.017 sec
Inserting... 0.001 sec
[9 ph, smt 0.044, isect 0.149, ins 0.003, tot 0.242]

Running SMT solver (10)... 0.003 sec
Checking... Minimizing (30,27)... (30,12), 0.017 sec
Inserting... 0.000 sec
[10 ph, smt 0.047, isect 0.166, ins 0.003, tot 0.267]

Running SMT solver (11)... 0.005 sec
Checking... Minimizing (30,27)... (30,11), 0.017 sec
Inserting... 0.001 sec
[11 ph, smt 0.052, isect 0.183, ins 0.004, tot 0.293]

Running SMT solver (12)... 0.003 sec
Checking... Minimizing (30,27)... (30,11), 0.017 sec
Inserting... 0.001 sec
[12 ph, smt 0.055, isect 0.200, ins 0.004, tot 0.317]

Running SMT solver (13)... 0.004 sec
Checking... Minimizing (30,26)... (30,11), 0.016 sec
Inserting... 0.001 sec
[13 ph, smt 0.059, isect 0.216, ins 0.005, tot 0.342]

Running SMT solver (14)... 0.004 sec
Checking... Minimizing (30,26)... (30,11), 0.016 sec
Inserting... 0.001 sec
[14 ph, smt 0.062, isect 0.232, ins 0.005, tot 0.366]

Running SMT solver (15)... 0.003 sec
Checking... Minimizing (30,27)... (30,12), 0.017 sec
Inserting... 0.001 sec
[15 ph, smt 0.065, isect 0.249, ins 0.006, tot 0.390]

Running SMT solver (16)... 0.004 sec
Checking... Minimizing (30,26)... (30,8), 0.015 sec
Inserting... 0.001 sec
[16 ph, smt 0.069, isect 0.264, ins 0.007, tot 0.415]

Running SMT solver (17)... 0.003 sec
Checking... Minimizing (30,27)... (30,7), 0.015 sec
Inserting... 0.001 sec
[17 ph, smt 0.073, isect 0.279, ins 0.008, tot 0.438]

Running SMT solver (18)... 0.003 sec
Checking... Minimizing (30,26)... (30,8), 0.015 sec
Inserting... 0.003 sec
[18 ph, smt 0.075, isect 0.295, ins 0.012, tot 0.464]

Running SMT solver (19)... 0.003 sec
Checking... Minimizing (30,26)... (30,12), 0.017 sec
Inserting... 0.001 sec
[19 ph, smt 0.079, isect 0.312, ins 0.013, tot 0.489]

Running SMT solver (20)... 0.003 sec
Checking... Minimizing (30,26)... (30,12), 0.017 sec
Inserting... 0.001 sec
[20 ph, smt 0.082, isect 0.328, ins 0.014, tot 0.513]

Running SMT solver (21)... 0.004 sec
Checking... Minimizing (30,26)... (29,8), 0.015 sec
Inserting... 0.001 sec
[21 ph, smt 0.085, isect 0.343, ins 0.015, tot 0.537]

Running SMT solver (22)... 0.003 sec
Checking... Minimizing (30,26)... (30,7), 0.014 sec
Inserting... 0.001 sec
[22 ph, smt 0.088, isect 0.357, ins 0.016, tot 0.559]

Running SMT solver (23)... 0.003 sec
Checking... Minimizing (30,26)... (30,7), 0.017 sec
Inserting... 0.003 sec
[23 ph, smt 0.091, isect 0.374, ins 0.019, tot 0.586]

Running SMT solver (24)... 0.003 sec
Checking... Minimizing (30,26)... (29,8), 0.014 sec
Inserting... 0.005 sec
[23 ph, smt 0.094, isect 0.388, ins 0.024, tot 0.611]

Running SMT solver (25)... 0.003 sec
Checking... Minimizing (30,26)... (29,8), 0.014 sec
Inserting... 0.006 sec
[23 ph, smt 0.097, isect 0.402, ins 0.030, tot 0.638]

Running SMT solver (26)... 0.003 sec
Checking... Minimizing (30,26)... (29,8), 0.015 sec
Inserting... 0.004 sec
[24 ph, smt 0.100, isect 0.417, ins 0.034, tot 0.662]

Running SMT solver (27)... 0.004 sec
Checking... Minimizing (30,28)... (30,10), 0.016 sec
Inserting... 0.003 sec
[25 ph, smt 0.104, isect 0.433, ins 0.037, tot 0.688]

Running SMT solver (28)... 0.003 sec
Checking... Minimizing (30,28)... (30,10), 0.016 sec
Inserting... 0.003 sec
[26 ph, smt 0.107, isect 0.449, ins 0.040, tot 0.714]

Running SMT solver (29)... 0.003 sec
Checking... Minimizing (30,29)... (30,10), 0.016 sec
Inserting... 0.003 sec
[27 ph, smt 0.110, isect 0.465, ins 0.043, tot 0.740]

Running SMT solver (30)... 0.003 sec
Checking... Minimizing (30,28)... (30,9), 0.015 sec
Inserting... 0.004 sec
[28 ph, smt 0.113, isect 0.480, ins 0.047, tot 0.766]

Running SMT solver (31)... 0.003 sec
Checking... Minimizing (30,28)... (30,9), 0.015 sec
Inserting... 0.003 sec
[29 ph, smt 0.116, isect 0.495, ins 0.051, tot 0.791]

Running SMT solver (32)... 0.003 sec
Checking... Minimizing (30,28)... (30,5), 0.014 sec
Inserting... 0.003 sec
[30 ph, smt 0.119, isect 0.509, ins 0.054, tot 0.815]

Running SMT solver (33)... 0.003 sec
Checking... Minimizing (30,27)... (30,5), 0.013 sec
Inserting... 0.004 sec
[31 ph, smt 0.122, isect 0.522, ins 0.058, tot 0.839]

Running SMT solver (34)... 0.003 sec
Checking... Minimizing (31,30)... (31,4), 0.015 sec
Inserting... 0.001 sec
[32 ph, smt 0.126, isect 0.537, ins 0.059, tot 0.862]

Running SMT solver (35)... 0.003 sec
Checking... Minimizing (30,28)... (30,5), 0.013 sec
Inserting... 0.003 sec
[32 ph, smt 0.129, isect 0.550, ins 0.062, tot 0.884]

Running SMT solver (36)... 0.003 sec
Checking... Minimizing (30,27)... (30,5), 0.013 sec
Inserting... 0.003 sec
[33 ph, smt 0.132, isect 0.563, ins 0.065, tot 0.907]

Running SMT solver (37)... 0.003 sec
Checking... Minimizing (30,29)... (30,10), 0.015 sec
Inserting... 0.001 sec
[34 ph, smt 0.135, isect 0.578, ins 0.066, tot 0.931]

Running SMT solver (38)... 0.003 sec
Checking... Minimizing (30,29)... (30,8), 0.015 sec
Inserting... 0.005 sec
[35 ph, smt 0.138, isect 0.594, ins 0.071, tot 0.958]

Running SMT solver (39)... 0.003 sec
Checking... Minimizing (30,29)... (30,8), 0.015 sec
Inserting... 0.003 sec
[36 ph, smt 0.141, isect 0.609, ins 0.075, tot 0.983]

Running SMT solver (40)... 0.003 sec
Checking... Minimizing (30,29)... (30,9), 0.015 sec
Inserting... 0.001 sec
[37 ph, smt 0.144, isect 0.624, ins 0.076, tot 1.006]

Running SMT solver (41)... 0.003 sec
Checking... Minimizing (30,29)... (30,9), 0.015 sec
Inserting... 0.001 sec
[38 ph, smt 0.147, isect 0.640, ins 0.077, tot 1.030]

Running SMT solver (42)... 0.003 sec
Checking... Minimizing (30,29)... (30,12), 0.016 sec
Inserting... 0.002 sec
[39 ph, smt 0.150, isect 0.656, ins 0.079, tot 1.054]

Running SMT solver (43)... 0.003 sec
Checking... Minimizing (30,29)... (30,12), 0.016 sec
Inserting... 0.002 sec
[40 ph, smt 0.153, isect 0.672, ins 0.081, tot 1.079]

Running SMT solver (44)... 0.003 sec
Checking... Minimizing (31,29)... (30,7), 0.014 sec
Inserting... 0.002 sec
[41 ph, smt 0.156, isect 0.686, ins 0.082, tot 1.102]

Running SMT solver (45)... 0.003 sec
Checking... Minimizing (31,29)... (30,7), 0.014 sec
Inserting... 0.002 sec
[42 ph, smt 0.159, isect 0.700, ins 0.084, tot 1.125]

Running SMT solver (46)... 0.003 sec
Checking... Minimizing (31,29)... (30,7), 0.014 sec
Inserting... 0.004 sec
[43 ph, smt 0.162, isect 0.715, ins 0.088, tot 1.151]

Running SMT solver (47)... 0.003 sec
Checking... Minimizing (31,29)... (30,7), 0.014 sec
Inserting... 0.002 sec
[44 ph, smt 0.165, isect 0.729, ins 0.090, tot 1.173]

Running SMT solver (48)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.007 sec
[42 ph, smt 0.169, isect 0.743, ins 0.097, tot 1.202]

Running SMT solver (49)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.006 sec
[40 ph, smt 0.172, isect 0.756, ins 0.104, tot 1.228]

Running SMT solver (50)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.006 sec
[38 ph, smt 0.175, isect 0.771, ins 0.110, tot 1.256]

Running SMT solver (51)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.015 sec
Inserting... 0.005 sec
[36 ph, smt 0.178, isect 0.785, ins 0.115, tot 1.281]

Running SMT solver (52)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.015 sec
Inserting... 0.001 sec
[37 ph, smt 0.181, isect 0.800, ins 0.115, tot 1.304]

Running SMT solver (53)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.014 sec
Inserting... 0.001 sec
[38 ph, smt 0.184, isect 0.814, ins 0.116, tot 1.326]

Running SMT solver (54)... 0.003 sec
Checking... Minimizing (30,28)... (30,7), 0.015 sec
Inserting... 0.001 sec
[39 ph, smt 0.187, isect 0.829, ins 0.117, tot 1.348]

Running SMT solver (55)... 0.003 sec
Checking... Minimizing (30,28)... (30,8), 0.015 sec
Inserting... 0.001 sec
[40 ph, smt 0.190, isect 0.844, ins 0.118, tot 1.371]

Running SMT solver (56)... 0.003 sec
Checking... Minimizing (30,28)... (30,8), 0.015 sec
Inserting... 0.001 sec
[41 ph, smt 0.193, isect 0.859, ins 0.119, tot 1.394]

Running SMT solver (57)... 0.003 sec
Checking... Minimizing (30,28)... (30,7), 0.015 sec
Inserting... 0.001 sec
[42 ph, smt 0.195, isect 0.874, ins 0.120, tot 1.417]

Running SMT solver (58)... 0.003 sec
Checking... Minimizing (30,28)... (30,6), 0.014 sec
Inserting... 0.001 sec
[43 ph, smt 0.199, isect 0.888, ins 0.121, tot 1.438]

Running SMT solver (59)... 0.003 sec
Checking... Minimizing (30,28)... (30,6), 0.014 sec
Inserting... 0.001 sec
[44 ph, smt 0.201, isect 0.901, ins 0.122, tot 1.460]

Running SMT solver (60)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.014 sec
Inserting... 0.003 sec
[44 ph, smt 0.204, isect 0.916, ins 0.125, tot 1.484]

Running SMT solver (61)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.014 sec
Inserting... 0.003 sec
[44 ph, smt 0.207, isect 0.930, ins 0.128, tot 1.507]

Running SMT solver (62)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.001 sec
[45 ph, smt 0.210, isect 0.944, ins 0.129, tot 1.529]

Running SMT solver (63)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.001 sec
[46 ph, smt 0.213, isect 0.959, ins 0.130, tot 1.551]

Running SMT solver (64)... 0.003 sec
Checking... Minimizing (30,27)... (30,8), 0.015 sec
Inserting... 0.001 sec
[47 ph, smt 0.216, isect 0.973, ins 0.131, tot 1.574]

Running SMT solver (65)... 0.003 sec
Checking... Minimizing (30,27)... (30,8), 0.015 sec
Inserting... 0.003 sec
[48 ph, smt 0.219, isect 0.988, ins 0.134, tot 1.598]

Running SMT solver (66)... 0.003 sec
Checking... Minimizing (30,27)... (30,6), 0.013 sec
Inserting... 0.001 sec
[49 ph, smt 0.222, isect 1.001, ins 0.135, tot 1.619]

Running SMT solver (67)... 0.003 sec
Checking... Minimizing (30,27)... (30,5), 0.013 sec
Inserting... 0.001 sec
[50 ph, smt 0.225, isect 1.014, ins 0.136, tot 1.640]

Running SMT solver (68)... 0.003 sec
Checking... Minimizing (30,27)... (30,6), 0.013 sec
Inserting... 0.003 sec
[51 ph, smt 0.228, isect 1.028, ins 0.139, tot 1.664]

Running SMT solver (69)... 0.003 sec
Checking... Minimizing (30,27)... (30,6), 0.013 sec
Inserting... 0.002 sec
[52 ph, smt 0.231, isect 1.041, ins 0.141, tot 1.685]

Running SMT solver (70)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.015 sec
Inserting... 0.004 sec
[52 ph, smt 0.234, isect 1.055, ins 0.144, tot 1.710]

Running SMT solver (71)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.014 sec
Inserting... 0.003 sec
[52 ph, smt 0.236, isect 1.069, ins 0.147, tot 1.733]

Running SMT solver (72)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.014 sec
Inserting... 0.003 sec
[52 ph, smt 0.239, isect 1.083, ins 0.150, tot 1.757]

Running SMT solver (73)... 0.003 sec
Checking... Minimizing (30,27)... (29,9), 0.014 sec
Inserting... 0.002 sec
[53 ph, smt 0.242, isect 1.098, ins 0.152, tot 1.779]

Running SMT solver (74)... 0.003 sec
Checking... Minimizing (30,27)... (30,5), 0.013 sec
Inserting... 0.001 sec
[54 ph, smt 0.245, isect 1.111, ins 0.153, tot 1.800]

Running SMT solver (75)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.003 sec
[54 ph, smt 0.248, isect 1.125, ins 0.156, tot 1.824]

Running SMT solver (76)... 0.003 sec
Checking... Minimizing (30,27)... (29,8), 0.014 sec
Inserting... 0.003 sec
[54 ph, smt 0.251, isect 1.139, ins 0.159, tot 1.846]

Running SMT solver (77)... 0.003 sec
Checking... Minimizing (30,28)... (30,11), 0.016 sec
Inserting... 0.001 sec
[55 ph, smt 0.254, isect 1.154, ins 0.160, tot 1.870]

Running SMT solver (78)... 0.003 sec
Checking... Minimizing (30,28)... (30,9), 0.015 sec
Inserting... 0.004 sec
[56 ph, smt 0.257, isect 1.169, ins 0.164, tot 1.895]

Running SMT solver (79)... 0.003 sec
Checking... Minimizing (30,28)... (30,9), 0.015 sec
Inserting... 0.002 sec
[57 ph, smt 0.260, isect 1.184, ins 0.165, tot 1.919]

Running SMT solver (80)... 0.003 sec
Checking... Minimizing (30,28)... (30,11), 0.017 sec
Inserting... 0.002 sec
[58 ph, smt 0.263, isect 1.202, ins 0.167, tot 1.945]

Running SMT solver (81)... 0.003 sec
Checking... Minimizing (30,27)... (30,9), 0.015 sec
Inserting... 0.002 sec
[59 ph, smt 0.266, isect 1.217, ins 0.169, tot 1.969]

Running SMT solver (82)... 0.003 sec
Checking... Minimizing (30,27)... (30,9), 0.015 sec
Inserting... 0.002 sec
[60 ph, smt 0.269, isect 1.232, ins 0.171, tot 1.993]

Running SMT solver (83)... 0.004 sec
Checking... Minimizing (30,26)... (30,9), 0.015 sec
Inserting... 0.003 sec
[61 ph, smt 0.272, isect 1.248, ins 0.174, tot 2.019]

Running SMT solver (84)... 0.003 sec
Checking... Minimizing (30,26)... (30,9), 0.016 sec
Inserting... 0.003 sec
[62 ph, smt 0.275, isect 1.264, ins 0.177, tot 2.045]

Running SMT solver (85)... 0.004 sec
Checking... Minimizing (30,27)... (30,9), 0.016 sec
Inserting... 0.004 sec
[63 ph, smt 0.279, isect 1.279, ins 0.181, tot 2.072]

Running SMT solver (86)... 0.003 sec
Checking... Minimizing (30,27)... (30,9), 0.015 sec
Inserting... 0.004 sec
[64 ph, smt 0.282, isect 1.295, ins 0.185, tot 2.098]

Running SMT solver (87)... 0.001 sec
No more solutions found

64 polyhedra, 87 rounds, smt 0.283 sec, isect 1.295 sec, insert 0.185 sec, total 2.101 sec
Preprocessing 2.721 sec, super total 4.823 sec
