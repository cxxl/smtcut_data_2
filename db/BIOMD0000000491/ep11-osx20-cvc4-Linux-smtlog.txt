
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-25T11:46:20+02:00
Command line: --no-dump --no-comp BIOMD0000000491 --cvc4 --pp
Flags: ppone ppsmt pp2x2 no-dump justone maxadd=20 solver=cvc4


Model: BIOMD0000000491
Logfile: db/BIOMD0000000491/ep11-osx20-cvc4-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^24.0 in 58 bags
Bag sizes: 1 3 2 1 1 3 7 2 3 2 2 2 5 4 4 4 2 10 2 11 2 5 1 1 1 1 1 3 3 7 5 5 3 2 3 2 2 2 2 2 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 2 14 57

Filtering bags...
Collecting...
Bag 1/50, 3 polyhedra
Bag 2/50, 2 polyhedra
Bag 3/50, 3 polyhedra
Bag 4/50, 7 polyhedra
Bag 5/50, 2 polyhedra
Bag 6/50, 3 polyhedra
Bag 7/50, 2 polyhedra
Bag 8/50, 2 polyhedra
Bag 9/50, 2 polyhedra
Bag 10/50, 5 polyhedra
Bag 11/50, 4 polyhedra
Bag 12/50, 4 polyhedra
Bag 13/50, 4 polyhedra
Bag 14/50, 2 polyhedra
Bag 15/50, 10 polyhedra
Bag 16/50, 2 polyhedra
Bag 17/50, 11 polyhedra
Bag 18/50, 2 polyhedra
Bag 19/50, 5 polyhedra
Bag 20/50, 3 polyhedra
Bag 21/50, 3 polyhedra
Bag 22/50, 7 polyhedra
Bag 23/50, 5 polyhedra
Bag 24/50, 5 polyhedra
Bag 25/50, 3 polyhedra
Bag 26/50, 2 polyhedra
Bag 27/50, 3 polyhedra
Bag 28/50, 2 polyhedra
Bag 29/50, 2 polyhedra
Bag 30/50, 2 polyhedra
Bag 31/50, 2 polyhedra
Bag 32/50, 2 polyhedra
Bag 33/50, 2 polyhedra
Bag 34/50, 2 polyhedra
Bag 35/50, 2 polyhedra
Bag 36/50, 2 polyhedra
Bag 37/50, 3 polyhedra
Bag 38/50, 2 polyhedra
Bag 39/50, 3 polyhedra
Bag 40/50, 2 polyhedra
Bag 41/50, 2 polyhedra
Bag 42/50, 2 polyhedra
Bag 43/50, 2 polyhedra
Bag 44/50, 2 polyhedra
Bag 45/50, 2 polyhedra
Bag 46/50, 2 polyhedra
Bag 47/50, 2 polyhedra
Bag 48/50, 2 polyhedra
Bag 49/50, 14 polyhedra
Bag 50/50, 57 polyhedra
Possible combinations after preprocessing: 10^24.0 in 51 bags
Bag sizes: 1 3 2 3 7 2 3 2 2 2 5 4 4 4 2 10 2 11 2 5 3 3 7 5 5 3 2 3 2 2 2 2 2 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 2 14 57
Time: 0.241 sec

Checking for superfluous polyhedra...
Bag 1/51, 57 polyhedra:
  Polyhedron 1... superfluous, 0.063 sec
  Polyhedron 2... superfluous, 0.023 sec
  Polyhedron 3... superfluous, 0.021 sec
  Polyhedron 4... superfluous, 0.022 sec
  Polyhedron 5... superfluous, 0.021 sec
  Polyhedron 6... superfluous, 0.022 sec
  Polyhedron 7... superfluous, 0.022 sec
  Polyhedron 8... superfluous, 0.021 sec
  Polyhedron 9... superfluous, 0.022 sec
  Polyhedron 10... superfluous, 0.021 sec
  Polyhedron 11... superfluous, 0.021 sec
  Polyhedron 12... superfluous, 0.021 sec
  Polyhedron 13... superfluous, 0.022 sec
  Polyhedron 14... superfluous, 0.021 sec
  Polyhedron 15... superfluous, 0.024 sec
  Polyhedron 16... superfluous, 0.022 sec
  Polyhedron 17... superfluous, 0.031 sec
  Polyhedron 18... superfluous, 0.022 sec
  Polyhedron 19... superfluous, 0.023 sec
  Polyhedron 20... superfluous, 0.022 sec
  Polyhedron 21... superfluous, 0.023 sec
  Polyhedron 22... superfluous, 0.022 sec
  Polyhedron 23... superfluous, 0.023 sec
  Polyhedron 24... superfluous, 0.022 sec
  Polyhedron 25... superfluous, 0.022 sec
  Polyhedron 26... superfluous, 0.022 sec
  Polyhedron 27... superfluous, 0.023 sec
  Polyhedron 28... superfluous, 0.022 sec
  Polyhedron 29... superfluous, 0.023 sec
  Polyhedron 30... superfluous, 0.023 sec
  Polyhedron 31... superfluous, 0.021 sec
  Polyhedron 32... superfluous, 0.018 sec
  Polyhedron 33... superfluous, 0.017 sec
  Polyhedron 34... superfluous, 0.017 sec
  Polyhedron 35... superfluous, 0.017 sec
  Polyhedron 36... superfluous, 0.022 sec
  Polyhedron 37... superfluous, 0.022 sec
  Polyhedron 38... superfluous, 0.021 sec
  Polyhedron 39... superfluous, 0.021 sec
  Polyhedron 40... superfluous, 0.017 sec
  Polyhedron 41... superfluous, 0.033 sec
  Polyhedron 42... superfluous, 0.023 sec
  Polyhedron 43... superfluous, 0.022 sec
  Polyhedron 44... superfluous, 0.023 sec
  Polyhedron 45... superfluous, 0.023 sec
  Polyhedron 46... superfluous, 0.022 sec
  Polyhedron 47... superfluous, 0.022 sec
  Polyhedron 48... superfluous, 0.022 sec
  Polyhedron 49... superfluous, 0.022 sec
  Polyhedron 50... superfluous, 0.022 sec
  Polyhedron 51... superfluous, 0.022 sec
  Polyhedron 52... superfluous, 0.026 sec
  Polyhedron 53... superfluous, 0.018 sec
  Polyhedron 54... superfluous, 0.017 sec
  Polyhedron 55... superfluous, 0.023 sec
  Polyhedron 56... superfluous, 0.023 sec
  Polyhedron 57... required, 0.075 sec
  => 1 polyhedra left
Bag 2/51, 14 polyhedra:
  Polyhedron 1... superfluous, 0.036 sec
  Polyhedron 2... superfluous, 0.047 sec
  Polyhedron 3... superfluous, 0.026 sec
  Polyhedron 4... superfluous, 0.038 sec
  Polyhedron 5... superfluous, 0.022 sec
  Polyhedron 6... superfluous, 0.018 sec
  Polyhedron 7... superfluous, 0.041 sec
  Polyhedron 8... superfluous, 0.032 sec
  Polyhedron 9... superfluous, 0.038 sec
  Polyhedron 10... superfluous, 0.039 sec
  Polyhedron 11... superfluous, 0.034 sec
  Polyhedron 12... superfluous, 0.046 sec
  Polyhedron 13... superfluous, 0.037 sec
  Polyhedron 14... required, 0.051 sec
  => 1 polyhedra left
Bag 3/51, 11 polyhedra:
  Polyhedron 1... superfluous, 0.012 sec
  Polyhedron 2... superfluous, 0.010 sec
  Polyhedron 3... superfluous, 0.010 sec
  Polyhedron 4... superfluous, 0.010 sec
  Polyhedron 5... superfluous, 0.010 sec
  Polyhedron 6... superfluous, 0.010 sec
  Polyhedron 7... superfluous, 0.011 sec
  Polyhedron 8... superfluous, 0.010 sec
  Polyhedron 9... superfluous, 0.011 sec
  Polyhedron 10... superfluous, 0.022 sec
  Polyhedron 11... required, 0.045 sec
  => 1 polyhedra left
Bag 4/51, 10 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... superfluous, 0.002 sec
  Polyhedron 3... superfluous, 0.002 sec
  Polyhedron 4... superfluous, 0.002 sec
  Polyhedron 5... superfluous, 0.002 sec
  Polyhedron 6... superfluous, 0.009 sec
  Polyhedron 7... superfluous, 0.010 sec
  Polyhedron 8... superfluous, 0.009 sec
  Polyhedron 9... superfluous, 0.011 sec
  Polyhedron 10... required, 0.034 sec
  => 1 polyhedra left
Bag 5/51, 7 polyhedra:
  Polyhedron 1... superfluous, 0.010 sec
  Polyhedron 2... superfluous, 0.009 sec
  Polyhedron 3... superfluous, 0.008 sec
  Polyhedron 4... superfluous, 0.008 sec
  Polyhedron 5... superfluous, 0.009 sec
  Polyhedron 6... superfluous, 0.008 sec
  Polyhedron 7... required, 0.030 sec
  => 1 polyhedra left
Bag 6/51, 7 polyhedra:
  Polyhedron 1... superfluous, 0.011 sec
  Polyhedron 2... superfluous, 0.009 sec
  Polyhedron 3... superfluous, 0.010 sec
  Polyhedron 4... superfluous, 0.014 sec
  Polyhedron 5... superfluous, 0.012 sec
  Polyhedron 6... superfluous, 0.010 sec
  Polyhedron 7... required, 0.031 sec
  => 1 polyhedra left
Bag 7/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.021 sec
  Polyhedron 2... superfluous, 0.009 sec
  Polyhedron 3... superfluous, 0.009 sec
  Polyhedron 4... superfluous, 0.009 sec
  Polyhedron 5... required, 0.028 sec
  => 1 polyhedra left
Bag 8/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.009 sec
  Polyhedron 2... superfluous, 0.008 sec
  Polyhedron 3... superfluous, 0.013 sec
  Polyhedron 4... superfluous, 0.015 sec
  Polyhedron 5... required, 0.023 sec
  => 1 polyhedra left
Bag 9/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.009 sec
  Polyhedron 2... superfluous, 0.009 sec
  Polyhedron 3... superfluous, 0.011 sec
  Polyhedron 4... superfluous, 0.009 sec
  Polyhedron 5... required, 0.021 sec
  => 1 polyhedra left
Bag 10/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.008 sec
  Polyhedron 2... superfluous, 0.008 sec
  Polyhedron 3... superfluous, 0.007 sec
  Polyhedron 4... superfluous, 0.007 sec
  Polyhedron 5... required, 0.015 sec
  => 1 polyhedra left
Bag 11/51, 4 polyhedra:
  Polyhedron 1... superfluous, 0.008 sec
  Polyhedron 2... superfluous, 0.007 sec
  Polyhedron 3... superfluous, 0.007 sec
  Polyhedron 4... required, 0.015 sec
  => 1 polyhedra left
Bag 12/51, 4 polyhedra:
  Polyhedron 1... superfluous, 0.007 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... superfluous, 0.006 sec
  Polyhedron 4... required, 0.012 sec
  => 1 polyhedra left
Bag 13/51, 4 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.007 sec
  Polyhedron 3... superfluous, 0.007 sec
  Polyhedron 4... required, 0.022 sec
  => 1 polyhedra left
Bag 14/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.007 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.011 sec
  => 1 polyhedra left
Bag 15/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.007 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.013 sec
  => 1 polyhedra left
Bag 16/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.014 sec
  => 1 polyhedra left
Bag 17/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.007 sec
  Polyhedron 2... superfluous, 0.006 sec
  Polyhedron 3... required, 0.012 sec
  => 1 polyhedra left
Bag 18/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.008 sec
  Polyhedron 3... required, 0.010 sec
  => 1 polyhedra left
Bag 19/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... required, 0.012 sec
  => 1 polyhedra left
Bag 20/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... required, 0.011 sec
  => 1 polyhedra left
Bag 21/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.006 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... required, 0.010 sec
  => 1 polyhedra left
Bag 22/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... superfluous, 0.005 sec
  Polyhedron 3... required, 0.008 sec
  => 1 polyhedra left
Bag 23/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.004 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 24/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 25/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 26/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 27/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.007 sec
  => 1 polyhedra left
Bag 28/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 29/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.005 sec
  Polyhedron 2... required, 0.008 sec
  => 1 polyhedra left
Bag 30/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.007 sec
  => 1 polyhedra left
Bag 31/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.004 sec
  Polyhedron 2... required, 0.005 sec
  => 1 polyhedra left
Bag 32/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.004 sec
  => 1 polyhedra left
Bag 33/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.004 sec
  => 1 polyhedra left
Bag 34/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.004 sec
  => 1 polyhedra left
Bag 35/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.004 sec
  => 1 polyhedra left
Bag 36/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 37/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 38/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 39/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 40/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 41/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 42/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 43/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Bag 44/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.003 sec
  => 1 polyhedra left
Bag 45/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.003 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Bag 46/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Bag 47/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Bag 48/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Bag 49/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Bag 50/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.002 sec
  Polyhedron 2... required, 0.002 sec
  => 1 polyhedra left
Possible combinations after preprocessing: 1 in 1 bags
Bag sizes: 1
Time: 3.175 sec
Trivial: one solution
Preprocessing 3.433 sec, super total 3.433 sec
