
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:40+02:00
Command line: BIOMD0000000491 --cvc4
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=cvc4


Model: BIOMD0000000491
Logfile: db/BIOMD0000000491/ep11-cvc4-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^24.0 in 58 bags
Bag sizes: 1 3 2 1 1 3 7 2 3 2 2 2 5 4 4 4 2 10 2 11 2 5 1 1 1 1 1 3 3 7 5 5 3 2 3 2 2 2 2 2 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 2 14 57

Minimizing (8,0)... (8,0)


Running SMT solver (1)... 0.113 sec
Found point (-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2)
Checking... Minimizing (57,171)... (57,0), 0.042 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 +1 x7 = 0
  eq1: -1 x44 +1 x57 = 0
  eq2: -1 x48 +1 x57 = 0
  eq3: -1 x9 +1 x13 = 0
  eq4: -1 x51 +1 x52 = 0
  eq5: -1 x34 +1 x57 = 0
  eq6: -1 x43 +1 x44 = 0
  eq7: -1 x7 +1 x15 = 0
  eq8: -1 x30 +1 x47 = 0
  eq9: -1 x32 +1 x47 = 0
  eq10: -1 x38 +1 x57 = 0
  eq11: -1 x57 = 2
  eq12: -1 x41 +1 x42 = 0
  eq13: -1 x35 +1 x36 = 0
  eq14: -1 x40 +1 x57 = 0
  eq15: -1 x10 +1 x13 = 0
  eq16: -1 x42 +1 x57 = 0
  eq17: -1 x15 +1 x22 = 0
  eq18: -1 x12 +1 x16 = 0
  eq19: -1 x28 +1 x30 = 0
  eq20: -1 x37 +1 x38 = 0
  eq21: -1 x53 +1 x57 = 0
  eq22: -1 x55 +1 x57 = 0
  eq23: -1 x2 +1 x9 = 0
  eq24: -1 x8 +1 x13 = 0
  eq25: -1 x45 +1 x46 = 0
  eq26: -1 x56 +1 x57 = 0
  eq27: -1 x18 +1 x28 = 0
  eq28: -1 x46 +1 x57 = 0
  eq29: -1 x18 +1 x23 = 0
  eq30: -1 x13 +1 x14 = 0
  eq31: -1 x18 +1 x21 = 0
  eq32: -1 x36 +1 x57 = 0
  eq33: -1 x4 +1 x7 = 0
  eq34: -1 x3 +1 x9 = 0
  eq35: -1 x19 +1 x20 = 0
  eq36: -1 x18 +1 x26 = 0
  eq37: -1 x50 +1 x57 = 0
  eq38: -1 x6 +1 x12 = 0
  eq39: -1 x22 +1 x32 = 0
  eq40: -1 x20 +1 x56 = 0
  eq41: -1 x11 +1 x16 = 0
  eq42: -1 x18 +1 x27 = 0
  eq43: -1 x54 +1 x57 = 0
  eq44: -1 x18 +1 x24 = 0
  eq45: -1 x47 +1 x48 = 0
  eq46: -1 x5 +1 x7 = 0
  eq47: -1 x17 +1 x18 = 0
  eq48: -1 x33 +1 x34 = 0
  eq49: -1 x49 +1 x50 = 0
  eq50: -1 x14 +1 x17 = 0
  eq51: -1 x31 +1 x45 = 0
  eq52: -1 x18 +1 x25 = 0
  eq53: -1 x39 +1 x40 = 0
  eq54: -1 x29 +1 x37 = 0
  eq55: -1 x16 +1 x28 = 0
  eq56: -1 x52 +1 x57 = 0
END

0.000 sec
[1 ph, smt 0.113, isect 0.042, ins 0.000, tot 0.247]

Running SMT solver (2)... 0.002 sec
No more solutions found



Solution:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

MAXIMIZE
Subject To
  eq0: -1 x1 +1 x7 = 0
  eq1: -1 x44 +1 x57 = 0
  eq2: -1 x48 +1 x57 = 0
  eq3: -1 x9 +1 x13 = 0
  eq4: -1 x51 +1 x52 = 0
  eq5: -1 x34 +1 x57 = 0
  eq6: -1 x43 +1 x44 = 0
  eq7: -1 x7 +1 x15 = 0
  eq8: -1 x30 +1 x47 = 0
  eq9: -1 x32 +1 x47 = 0
  eq10: -1 x38 +1 x57 = 0
  eq11: -1 x57 = 2
  eq12: -1 x41 +1 x42 = 0
  eq13: -1 x35 +1 x36 = 0
  eq14: -1 x40 +1 x57 = 0
  eq15: -1 x10 +1 x13 = 0
  eq16: -1 x42 +1 x57 = 0
  eq17: -1 x15 +1 x22 = 0
  eq18: -1 x12 +1 x16 = 0
  eq19: -1 x28 +1 x30 = 0
  eq20: -1 x37 +1 x38 = 0
  eq21: -1 x53 +1 x57 = 0
  eq22: -1 x55 +1 x57 = 0
  eq23: -1 x2 +1 x9 = 0
  eq24: -1 x8 +1 x13 = 0
  eq25: -1 x45 +1 x46 = 0
  eq26: -1 x56 +1 x57 = 0
  eq27: -1 x18 +1 x28 = 0
  eq28: -1 x46 +1 x57 = 0
  eq29: -1 x18 +1 x23 = 0
  eq30: -1 x13 +1 x14 = 0
  eq31: -1 x18 +1 x21 = 0
  eq32: -1 x36 +1 x57 = 0
  eq33: -1 x4 +1 x7 = 0
  eq34: -1 x3 +1 x9 = 0
  eq35: -1 x19 +1 x20 = 0
  eq36: -1 x18 +1 x26 = 0
  eq37: -1 x50 +1 x57 = 0
  eq38: -1 x6 +1 x12 = 0
  eq39: -1 x22 +1 x32 = 0
  eq40: -1 x20 +1 x56 = 0
  eq41: -1 x11 +1 x16 = 0
  eq42: -1 x18 +1 x27 = 0
  eq43: -1 x54 +1 x57 = 0
  eq44: -1 x18 +1 x24 = 0
  eq45: -1 x47 +1 x48 = 0
  eq46: -1 x5 +1 x7 = 0
  eq47: -1 x17 +1 x18 = 0
  eq48: -1 x33 +1 x34 = 0
  eq49: -1 x49 +1 x50 = 0
  eq50: -1 x14 +1 x17 = 0
  eq51: -1 x31 +1 x45 = 0
  eq52: -1 x18 +1 x25 = 0
  eq53: -1 x39 +1 x40 = 0
  eq54: -1 x29 +1 x37 = 0
  eq55: -1 x16 +1 x28 = 0
  eq56: -1 x52 +1 x57 = 0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end



1 polyhedra, 2 rounds, smt 0.115 sec, isect 0.042 sec, insert 0.000 sec, total 0.252 sec
Preprocessing 0.005 sec, super total 0.259 sec
Comparing... 0.100 sec.  Solution matches input.
