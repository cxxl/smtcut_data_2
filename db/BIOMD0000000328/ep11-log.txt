
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:21:09+02:00
Command line: ..\trop\ptcut.py BIOMD0000000328 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000328 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 3T33OHG67A6WW
Effective epsilon: 1/11.0

Reading db\BIOMD0000000328\vector_field_q.txt
Reading db\BIOMD0000000328\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 7 with: k1*k2*k22*k23*k3*k4 + k1*k2*k22*k23*k3*x7 + k1*k2*k22*k23*k4*x7 + k1*k2*k23*k3*k4*x7 + k1*k2*k23*k3*x7**2 + k1*k2*k23*k4*x7**2 + k1*k2*k3*k4*x7**2 + k1*k2*k3*x7**3 + k1*k2*k4*x7**3 + k1*k22*k23*k3*k4*x8 + k1*k23*k3*k4*x7*x8 + k1*k3*k4*x7**2*x8 + k2*k22*k23*k3*k4*x8 + k2*k23*k3*k4*x7*x8 + k2*k3*k4*x7**2*x8

Multiplying equation 8 with: k1*k2*k22*k23*k3*k4 + k1*k2*k22*k23*k3*x7 + k1*k2*k22*k23*k4*x7 + k1*k2*k23*k3*k4*x7 + k1*k2*k23*k3*x7**2 + k1*k2*k23*k4*x7**2 + k1*k2*k3*k4*x7**2 + k1*k2*k3*x7**3 + k1*k2*k4*x7**3 + k1*k22*k23*k3*k4*x8 + k1*k23*k3*k4*x7*x8 + k1*k3*k4*x7**2*x8 + k2*k22*k23*k3*k4*x8 + k2*k23*k3*k4*x7*x8 + k2*k3*k4*x7**2*x8

Multiplying equation 9 with: k1*k2*k3*k4 + k1*k2*k3*x7 + k1*k2*k4*x7 + k1*k3*k4*x8 + k2*k3*k4*x8

Multiplying equation 10 with: k1*k2*k3*k4 + k1*k2*k3*x7 + k1*k2*k4*x7 + k1*k3*k4*x8 + k2*k3*k4*x8

Multiplying equation 11 with: k1*k2*k3*k4 + k1*k2*k3*x7 + k1*k2*k4*x7 + k1*k3*k4*x8 + k2*k3*k4*x8

Multiplying equation 12 with: k1*k2*k3*k4 + k1*k2*k3*x7 + k1*k2*k4*x7 + k1*k3*k4*x8 + k2*k3*k4*x8

System is rational in variable x7,x8.
System is rational in parameter k1,k2,k3,k4,k8,k9,k22,k23,k32,k33.
Tropicalization time: 61.294 sec
Variables: x1-x18
Saving tropical system...
Creating polyhedra... 
Creation time: 13.879 sec
Saving polyhedra...

Sorting ascending...
Dimension: 18
Formulas: 19
Order: 1 3 5 12 13 14 15 16 17 0 2 4 10 11 8 9 7 6 18
Possible combinations: 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 9 * 9 * 12 * 12 * 15 * 16 * 18 = 403,107,840 (10^9)
 Applying common planes (codim=9, hs=20)...
 [1/10 (#0)]: 2 => 2 (-), dim=8.0, hs=19.5
 [2/10 (#2)]: 2 => 2 (-), dim=8.0, hs=20.0
 [3/10 (#4)]: 2 => 2 (-), dim=8.0, hs=19.5
 [4/10 (#10)]: 9 => 3 (3 empty, 3 incl), dim=9.0, hs=22.3
  Removed: 10.1,10.2,10.10,10.13,10.16,10.20
 [5/10 (#11)]: 9 => 3 (3 empty, 3 incl), dim=9.0, hs=22.3
  Removed: 11.1,11.3,11.9,11.10,11.14,11.20
 [6/10 (#8)]: 12 => 3 (9 incl), dim=9.0, hs=24.3
  Removed: 8.1,8.3,8.4,8.15,8.17,8.18,8.24,8.28,8.29
 [7/10 (#9)]: 12 => 3 (2 empty, 7 incl), dim=9.0, hs=23.0
  Removed: 9.1,9.3,9.5,9.14,9.17,9.19,9.23,9.27,9.29
 [8/10 (#7)]: 15 => 4 (6 empty, 5 incl), dim=9.0, hs=21.8
  Removed: 7.1,7.66,7.3,7.73,7.75,7.78,7.48,7.80,7.19,7.58,7.61
 [9/10 (#6)]: 16 => 4 (6 empty, 6 incl), dim=9.0, hs=21.8
  Removed: 6.1,6.3,6.74,6.76,6.77,6.48,6.49,6.81,6.83,6.20,6.21,6.63
 [10/10 (#18)]: 18 => 5 (10 empty, 3 incl), dim=8.0, hs=19.8
  Removed: 18.0,18.2,18.4,18.6,18.7,18.8,18.10,18.12,18.13,18.14,18.15,18.16,18.17
 Savings: factor 7,776 (10^4), 9 formulas
[1/9 (#0, #2)]: 2 * 2 = 4 => 4 (-), dim=7.0, hs=19.5
 Applying common planes (codim=9, hs=13)...
 [1/9 (w1)]: 4 => 4 (-), dim=7.0, hs=21.5
 [2/9 (#4)]: 2 => 2 (-), dim=8.0, hs=20.0
 [3/9 (#10)]: 3 => 3 (-), dim=9.0, hs=24.3
 [4/9 (#11)]: 3 => 3 (-), dim=9.0, hs=24.3
 [5/9 (#8)]: 3 => 3 (-), dim=9.0, hs=24.3
 [6/9 (#9)]: 3 => 3 (-), dim=9.0, hs=25.0
 [7/9 (#7)]: 4 => 4 (-), dim=9.0, hs=23.8
 [8/9 (#6)]: 4 => 4 (-), dim=9.0, hs=23.8
 [9/9 (#18)]: 5 => 5 (-), dim=8.0, hs=21.2
 Savings: factor 1.0, 0 formulas
[2/9 (w1, #4)]: 4 * 2 = 8 => 8 (-), dim=6.0, hs=19.5
[3/9 (w2, #10)]: 8 * 3 = 24 => 20 (4 empty), dim=6.0, hs=21.4
[4/9 (w3, #11)]: 20 * 3 = 60 => 20 (8 empty, 32 incl), dim=6.0, hs=22.4
[5/9 (w4, #8)]: 20 * 3 = 60 => 20 (8 empty, 32 incl), dim=6.0, hs=22.9
[6/9 (w5, #9)]: 20 * 3 = 60 => 20 (8 empty, 32 incl), dim=6.0, hs=23.9
[7/9 (w6, #7)]: 20 * 4 = 80 => 28 (12 empty, 40 incl), dim=6.0, hs=24.1
[8/9 (w7, #6)]: 28 * 4 = 112 => 28 (20 empty, 64 incl), dim=6.0, hs=24.1
[9/9 (w8, #18)]: 28 * 5 = 140 => 86 (40 empty, 14 incl), dim=5.0, hs=23.0
Total time: 1.351 sec, total intersections: 676, total inclusion tests: 5,331
Common plane time: 0.201 sec
Solutions: 86, dim=5.0, hs=23.0, max=86, maxin=140
f-vector: [0, 0, 0, 0, 0, 86]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.092 GiB, pagefile 0.086 GiB
Memory: pmem(rss=83992576, vms=76943360, num_page_faults=27136, peak_wset=98451456, wset=83992576, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=45544, nonpaged_pool=45128, pagefile=76943360, peak_pagefile=92086272, private=76943360)
