
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:34+02:00
Command line: BIOMD0000000026 --msat
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=msat


Model: BIOMD0000000026
Logfile: db/BIOMD0000000026/ep11-msat-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^6.2 in 14 bags
Bag sizes: 4 8 2 4 12 1 1 1 2 1 2 5 3 9

Minimizing (4,0)... (4,0)


Running SMT solver (1)... 0.009 sec
Found point (-1,0,-3,-2,0,-1,0,-2,-1,2,1)
Checking... Minimizing (10,29)... (10,3), 0.009 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x4 +1 x6 = 2
  eq4: -1 x8 = 2
  eq5: -1 x2 -1 x5 +1 x10 = 2
  eq6: -1 x4 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x2 -1 x4 +1 x9 = 1
  eq9: +1 x10 -1 x11 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 -1 x4 +1 x11 >= 1
  ie2: -1 x2 -1 x4 +1 x6 >= 1
END

0.000 sec
[1 ph, smt 0.009, isect 0.009, ins 0.000, tot 0.044]

Running SMT solver (2)... 0.002 sec
Found point (-2,0,-3,-2,0,-2,0,-2,-1,2,0)
Checking... Minimizing (10,29)... (10,2), 0.007 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x1 >= -3
  ie1: +1 x4 >= -2
END

0.000 sec
[2 ph, smt 0.011, isect 0.015, ins 0.000, tot 0.058]

Running SMT solver (3)... 0.001 sec
Found point (-7/4,0,-3,-2,0,-7/4,0,-2,-1,2,1/4)
Checking... Minimizing (10,29)... (10,2), 0.005 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x8 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x4 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 +1 x2 >= 1
END

0.002 sec
[3 ph, smt 0.013, isect 0.021, ins 0.002, tot 0.071]

Running SMT solver (4)... 0.002 sec
Found point (-3,-1,-2,-1,-1,-2,0,-2,-1,0,-2)
Checking... Minimizing (10,29)... (10,3), 0.006 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x11 = 2
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: -1 x1 +1 x2 >= 1
  ie1: +1 x8 >= -2
  ie2: +1 x1 -1 x3 >= -2
END

0.000 sec
[4 ph, smt 0.015, isect 0.027, ins 0.002, tot 0.083]

Running SMT solver (5)... 0.001 sec
Found point (-3,-1,-9/4,-1,-3/4,-2,0,-2,-1,1/4,-7/4)
Checking... Minimizing (10,29)... (10,3), 0.005 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x11 >= -2
  ie1: +1 x5 >= -2
  ie2: +1 x3 >= -3
END

0.001 sec
[5 ph, smt 0.016, isect 0.032, ins 0.003, tot 0.096]

Running SMT solver (6)... 0.001 sec
Found point (-3,0,-3/4,-1,-1,-2,1,-3/4,1/4,1,-2)
Checking... Minimizing (10,27)... (10,2), 0.006 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: +1 x2 +1 x4 -1 x6 = 1
  eq3: -1 x1 = 3
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x1 -1 x5 +1 x11 = 2
  eq6: -1 x6 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x11 = 2
  ie0: +1 x2 +1 x5 -1 x8 >= -1
  ie1: -1 x1 +1 x3 >= 2
END

0.000 sec
[6 ph, smt 0.017, isect 0.038, ins 0.003, tot 0.108]

Running SMT solver (7)... 0.001 sec
No more solutions found



Solution:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: +1 x2 +1 x4 -1 x6 = 1
  eq3: -1 x1 = 3
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x1 -1 x5 +1 x11 = 2
  eq6: -1 x6 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x11 = 2
  ie0: +1 x2 +1 x5 -1 x8 >= -1
  ie1: -1 x1 +1 x3 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x11 >= -2
  ie1: +1 x5 >= -2
  ie2: +1 x3 >= -3
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x11 = 2
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: -1 x1 +1 x2 >= 1
  ie1: +1 x8 >= -2
  ie2: +1 x1 -1 x3 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x8 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x4 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 +1 x2 >= 1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x1 >= -3
  ie1: +1 x4 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x4 +1 x6 = 2
  eq4: -1 x8 = 2
  eq5: -1 x2 -1 x5 +1 x10 = 2
  eq6: -1 x4 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x2 -1 x4 +1 x9 = 1
  eq9: +1 x10 -1 x11 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 -1 x4 +1 x11 >= 1
  ie2: -1 x2 -1 x4 +1 x6 >= 1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end



6 polyhedra, 7 rounds, smt 0.018 sec, isect 0.038 sec, insert 0.003 sec, total 0.109 sec
Preprocessing 0.004 sec, super total 0.113 sec
Comparing... 0.014 sec.  Solution matches input.
