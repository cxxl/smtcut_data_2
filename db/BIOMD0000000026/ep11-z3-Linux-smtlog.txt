
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:34+02:00
Command line: BIOMD0000000026 --z3
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=z3


Model: BIOMD0000000026
Logfile: db/BIOMD0000000026/ep11-z3-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^6.2 in 14 bags
Bag sizes: 4 8 2 4 12 1 1 1 2 1 2 5 3 9

Minimizing (4,0)... (4,0)


Running SMT solver (1)... 0.006 sec
Found point (-3,0,0,-1,-1,-2,1,0,1,1,-2)
Checking... Minimizing (11,27)... (11,0), 0.008 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: +1 x2 +1 x4 -1 x6 = 1
  eq3: -1 x1 = 3
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x1 -1 x5 +1 x11 = 2
  eq6: -1 x6 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x11 = 2
  eq10: -1 x3 -1 x5 +1 x7 = 2
END

0.000 sec
[1 ph, smt 0.006, isect 0.008, ins 0.000, tot 0.044]

Running SMT solver (2)... 0.001 sec
Found point (-3,0,-1,-1,-1,-2,1,-1,0,1,-2)
Checking... Minimizing (10,29)... (10,3), 0.007 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x11 = 2
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: -1 x1 +1 x2 >= 1
  ie1: +1 x8 >= -2
  ie2: +1 x1 -1 x3 >= -2
END

0.000 sec
[2 ph, smt 0.007, isect 0.015, ins 0.000, tot 0.059]

Running SMT solver (3)... 0.001 sec
Found point (-3,0,-3/4,-1,-1,-2,1,-3/4,1/4,1,-2)
Checking... Minimizing (10,27)... (10,2), 0.005 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: +1 x2 +1 x4 -1 x6 = 1
  eq3: -1 x1 = 3
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x1 -1 x5 +1 x11 = 2
  eq6: -1 x6 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x11 = 2
  ie0: +1 x2 +1 x5 -1 x8 >= -1
  ie1: -1 x1 +1 x3 >= 2
END

0.001 sec
[2 ph, smt 0.008, isect 0.020, ins 0.001, tot 0.074]

Running SMT solver (4)... 0.002 sec
Found point (-3,-1,-3,-1,0,-2,0,-2,-1,1,-1)
Checking... Minimizing (10,29)... (10,2), 0.006 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x1 >= -3
  ie1: +1 x4 >= -2
END

0.000 sec
[3 ph, smt 0.010, isect 0.026, ins 0.001, tot 0.090]

Running SMT solver (5)... 0.001 sec
Found point (-3,-1,-11/4,-1,-1/4,-2,0,-2,-1,3/4,-5/4)
Checking... Minimizing (10,29)... (10,3), 0.005 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x11 >= -2
  ie1: +1 x5 >= -2
  ie2: +1 x3 >= -3
END

0.001 sec
[4 ph, smt 0.011, isect 0.032, ins 0.003, tot 0.104]

Running SMT solver (6)... 0.001 sec
Found point (-7/4,0,-3,-2,0,-7/4,0,-2,-1,2,1/4)
Checking... Minimizing (10,29)... (10,2), 0.006 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x8 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x4 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 +1 x2 >= 1
END

0.000 sec
[5 ph, smt 0.012, isect 0.037, ins 0.003, tot 0.119]

Running SMT solver (7)... 0.001 sec
Found point (2,0,-3,-2,0,2,0,-2,-1,2,1)
Checking... Minimizing (10,29)... (10,3), 0.006 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x4 +1 x6 = 2
  eq4: -1 x8 = 2
  eq5: -1 x2 -1 x5 +1 x10 = 2
  eq6: -1 x4 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x2 -1 x4 +1 x9 = 1
  eq9: +1 x10 -1 x11 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 -1 x4 +1 x11 >= 1
  ie2: -1 x2 -1 x4 +1 x6 >= 1
END

0.000 sec
[6 ph, smt 0.013, isect 0.043, ins 0.003, tot 0.132]

Running SMT solver (8)... 0.001 sec
No more solutions found



Solution:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x4 +1 x6 = 2
  eq4: -1 x8 = 2
  eq5: -1 x2 -1 x5 +1 x10 = 2
  eq6: -1 x4 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x2 -1 x4 +1 x9 = 1
  eq9: +1 x10 -1 x11 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 -1 x4 +1 x11 >= 1
  ie2: -1 x2 -1 x4 +1 x6 >= 1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x8 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x4 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x6 >= -2
  ie1: -1 x1 +1 x2 >= 1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x11 >= -2
  ie1: +1 x5 >= -2
  ie2: +1 x3 >= -3
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x3 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x8 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: +1 x1 >= -3
  ie1: +1 x4 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: +1 x2 +1 x4 -1 x6 = 1
  eq3: -1 x1 = 3
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x1 -1 x5 +1 x11 = 2
  eq6: -1 x6 = 2
  eq7: -1 x2 -1 x5 +1 x10 = 2
  eq8: -1 x8 +1 x9 = 1
  eq9: -1 x11 = 2
  ie0: +1 x2 +1 x5 -1 x8 >= -1
  ie1: -1 x1 +1 x3 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x7 = 2
  eq1: -1 x3 -1 x5 +1 x8 = 1
  eq2: -1 x1 = 3
  eq3: -1 x1 -1 x5 +1 x11 = 2
  eq4: -1 x1 -1 x4 +1 x6 = 2
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x5 +1 x10 = 2
  eq7: -1 x8 +1 x9 = 1
  eq8: -1 x11 = 2
  eq9: -1 x2 -1 x4 +1 x9 = 1
  ie0: -1 x1 +1 x2 >= 1
  ie1: +1 x8 >= -2
  ie2: +1 x1 -1 x3 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end



6 polyhedra, 8 rounds, smt 0.014 sec, isect 0.043 sec, insert 0.003 sec, total 0.134 sec
Preprocessing 0.004 sec, super total 0.138 sec
Comparing... 0.014 sec.  Solution matches input.
