
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:52+02:00
Command line: BIOMD0000000637 --msat
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=msat


Model: BIOMD0000000637
Logfile: db/BIOMD0000000637/ep11-msat-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^12.3 in 15 bags
Bag sizes: 12 12 9 12 16 3 9 2 3 8 4 6 4 9 8

Minimizing (0,0)... (0,0)


Running SMT solver (1)... 0.033 sec
Found point (-29/28,-1/4,-55/28,-33/28,-3/14,-2,1/4,-3/14,-17/14,-13/28,-17/14,-3/14)
Checking... Minimizing (9,52)... (9,12), 0.017 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: +1 x7 -1 x8 +1 x12 >= 0
  ie6: -1 x3 +1 x10 >= 1
  ie7: +1 x4 >= -2
  ie8: +1 x6 -1 x7 -1 x10 >= -2
  ie9: +1 x12 >= -2
  ie10: -1 x3 +1 x4 >= 0
  ie11: +1 x3 >= -2
END

0.000 sec
[1 ph, smt 0.033, isect 0.017, ins 0.000, tot 0.092]

Running SMT solver (2)... 0.003 sec
Found point (-9/8,-3/10,-15/8,-21/20,-31/40,-2,4/5,-7/40,-47/40,-39/40,-47/40,-7/40)
Checking... Minimizing (9,52)... (9,16), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x3 +1 x8 = 2
  eq2: -1 x6 = 2
  eq3: -1 x7 +1 x8 -1 x10 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: +1 x5 +1 x7 -1 x10 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x6 -1 x7 -1 x10 >= -2
  ie5: +1 x12 >= -2
  ie6: -1 x3 +1 x4 >= 0
  ie7: +1 x1 +1 x5 -1 x7 -1 x10 >= -2
  ie8: +1 x4 >= -2
  ie9: +1 x4 -1 x5 -1 x7 >= -3
  ie10: +1 x2 -1 x7 >= -2
  ie11: +1 x7 -1 x8 +1 x12 >= 0
  ie12: +1 x3 -1 x5 -1 x7 >= -2
  ie13: +1 x3 >= -2
  ie14: +1 x1 >= -2
  ie15: +1 x1 -1 x7 >= -2
END

0.000 sec
[2 ph, smt 0.037, isect 0.030, ins 0.000, tot 0.113]

Running SMT solver (3)... 0.027 sec
Found point (-15/8,-3/4,-9/8,-1/8,3/4,-2,1/8,1/8,-7/8,-1/8,-1,1/8)
Checking... Minimizing (9,52)... (9,15), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x3 +1 x8 = 2
  eq2: -1 x6 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 2
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: +1 x5 +1 x7 -1 x10 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x4 -1 x5 -1 x7 >= -3
  ie4: +1 x2 -1 x7 >= -2
  ie5: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie6: +1 x1 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x3 -1 x6 >= 0
  ie9: +1 x2 +1 x5 -1 x9 >= -1
  ie10: -1 x1 -1 x4 +1 x9 >= 1
  ie11: +1 x3 -1 x5 -1 x7 >= -2
  ie12: +1 x1 +1 x5 -1 x7 -1 x10 >= -2
  ie13: +1 x9 >= -2
  ie14: +1 x1 -1 x7 >= -2
END

0.000 sec
[3 ph, smt 0.064, isect 0.043, ins 0.000, tot 0.157]

Running SMT solver (4)... 0.006 sec
Found point (-6/5,-11/10,-9/5,-17/10,1/5,-2,-1/20,-9/10,-19/10,-17/20,-19/10,-17/20)
Checking... Minimizing (9,52)... (8,18), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x6 = 2
  eq1: +1 x8 -1 x9 = 1
  eq2: -1 x7 +1 x8 -1 x12 = 0
  eq3: +1 x7 +1 x10 -1 x11 = 1
  eq4: -1 x1 -1 x3 +1 x6 = 1
  eq5: -1 x1 -1 x4 +1 x11 = 1
  eq6: +1 x5 +1 x7 -1 x12 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x5 -1 x7 +1 x10 >= -1
  ie4: +1 x2 +1 x4 -1 x8 >= -2
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: +1 x4 -1 x5 -1 x7 >= -3
  ie13: +1 x2 -1 x7 >= -2
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 -1 x5 -1 x7 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

0.000 sec
[4 ph, smt 0.070, isect 0.058, ins 0.000, tot 0.182]

Running SMT solver (5)... 0.012 sec
Found point (-1,1/4,-2,-3/4,-1,-2,1,1/4,-3/4,-1,-3/4,1/4)
Checking... Minimizing (8,52)... (8,22), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x3 +1 x8 = 2
  eq2: -1 x6 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 2
  eq4: +1 x5 +1 x7 -1 x10 = 1
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x12 >= -1
  ie5: +1 x1 +1 x5 -1 x6 >= 0
  ie6: -1 x6 +1 x8 >= 2
  ie7: +1 x12 >= -2
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x9 >= -2
  ie10: +1 x4 -1 x5 -1 x7 >= -3
  ie11: +1 x2 -1 x7 >= -2
  ie12: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie13: -1 x6 +1 x11 >= 1
  ie14: +1 x3 -1 x6 >= 0
  ie15: +1 x2 +1 x5 -1 x9 >= -1
  ie16: -1 x1 -1 x4 +1 x9 >= 1
  ie17: -1 x1 +1 x3 -1 x4 >= -1
  ie18: +1 x3 -1 x5 -1 x7 >= -2
  ie19: -1 x1 -1 x4 +1 x6 >= -1
  ie20: +1 x1 >= -2
  ie21: +1 x1 -1 x7 >= -2
END

0.002 sec
[4 ph, smt 0.082, isect 0.071, ins 0.002, tot 0.214]

Running SMT solver (6)... 0.011 sec
Found point (0,-2,0,-1,4,0,-2,-1,-2,3,0,1)
Checking... Minimizing (11,51)... (10,7), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x7 = 2
  eq1: -1 x2 -1 x4 +1 x9 = 1
  eq2: -1 x9 = 2
  eq3: -1 x2 -1 x3 +1 x8 = 1
  eq4: -1 x6 +1 x7 +1 x10 = 1
  eq5: +1 x8 -1 x9 = 1
  eq6: -1 x7 +1 x8 -1 x12 = 0
  eq7: +1 x7 +1 x10 -1 x11 = 1
  eq8: -1 x1 -1 x4 +1 x11 = 1
  eq9: +1 x5 +1 x7 -1 x12 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x4 -1 x5 -1 x7 >= -3
  ie3: -1 x5 -1 x7 +1 x10 >= -1
  ie4: -1 x1 +1 x2 -1 x4 >= -1
  ie5: +1 x2 >= -2
  ie6: -1 x2 -1 x3 +1 x6 >= 1
END

0.000 sec
[5 ph, smt 0.093, isect 0.082, ins 0.002, tot 0.239]

Running SMT solver (7)... 0.009 sec
Found point (-5/4,3/4,-7/4,1/4,-7/4,-2,2,1,0,-2,0,1)
Checking... Minimizing (9,49)... (9,13), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x10 = 2
  eq2: -1 x9 +1 x12 = 1
  eq3: -1 x2 -1 x3 +1 x8 = 2
  eq4: -1 x6 = 2
  eq5: -1 x6 +1 x7 +1 x10 = 2
  eq6: -1 x2 -1 x5 +1 x9 = 1
  eq7: -1 x1 -1 x3 +1 x6 = 1
  eq8: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x5 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x1 -1 x4 +1 x10 >= -1
  ie3: +1 x1 >= -2
  ie4: -1 x6 +1 x8 >= 2
  ie5: +1 x12 >= -2
  ie6: +1 x3 -1 x6 >= 0
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: +1 x9 >= -2
  ie9: -1 x1 -1 x4 +1 x9 >= 1
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: +1 x4 -1 x5 >= 0
END

0.000 sec
[6 ph, smt 0.102, isect 0.093, ins 0.002, tot 0.264]

Running SMT solver (8)... 0.005 sec
Found point (-13/12,-1/6,-23/12,-13/12,-23/12,-2,23/12,-1/12,-13/12,-2,-7/6,-1/12)
Checking... Minimizing (9,49)... (9,14), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x10 = 2
  eq2: -1 x9 +1 x12 = 1
  eq3: -1 x2 -1 x3 +1 x8 = 2
  eq4: -1 x6 = 2
  eq5: -1 x7 +1 x8 -1 x10 = 0
  eq6: -1 x2 -1 x5 +1 x9 = 1
  eq7: -1 x1 -1 x3 +1 x6 = 1
  eq8: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x5 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: +1 x1 >= -2
  ie5: +1 x12 >= -2
  ie6: +1 x5 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: +1 x9 >= -2
  ie9: +1 x6 -1 x8 >= -2
  ie10: -1 x1 -1 x4 +1 x9 >= 1
  ie11: +1 x2 >= -2
  ie12: -1 x1 +1 x7 >= 2
  ie13: +1 x4 -1 x5 >= 0
END

0.000 sec
[7 ph, smt 0.107, isect 0.105, ins 0.002, tot 0.285]

Running SMT solver (9)... 0.006 sec
Found point (-15/8,-7/4,-9/8,-1,-7/8,-2,7/8,-7/8,-15/8,-7/4,-15/8,-7/8)
Checking... Minimizing (9,50)... (9,12), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 >= -2
  ie5: +1 x6 -1 x7 -1 x10 >= -2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
END

0.000 sec
[8 ph, smt 0.113, isect 0.118, ins 0.002, tot 0.308]

Running SMT solver (10)... 0.003 sec
Found point (-5/4,-1/8,-7/4,-5/8,-5/8,-2,7/8,1/8,-7/8,-7/8,-7/8,1/8)
Checking... Minimizing (8,50)... (8,18), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x12 >= -1
  ie5: -1 x6 +1 x8 >= 2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie13: -1 x6 +1 x11 >= 1
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x1 -1 x4 +1 x6 >= -1
  ie17: +1 x1 >= -2
END

0.002 sec
[8 ph, smt 0.116, isect 0.131, ins 0.004, tot 0.330]

Running SMT solver (11)... 0.002 sec
Found point (-5/4,3/4,-7/4,1/4,1/4,-2,0,1,0,0,0,1)
Checking... Minimizing (8,52)... (8,21), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x6 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 2
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x6 +1 x12 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x1 -1 x4 +1 x10 >= -1
  ie6: -1 x6 +1 x8 >= 2
  ie7: +1 x12 >= -2
  ie8: +1 x8 -1 x9 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 +1 x3 -1 x8 >= -2
  ie11: +1 x8 -1 x9 >= -1
  ie12: +1 x9 >= -2
  ie13: -1 x6 +1 x11 >= 1
  ie14: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie15: +1 x3 -1 x6 >= 0
  ie16: -1 x1 -1 x4 +1 x9 >= 1
  ie17: -1 x1 +1 x3 -1 x4 >= -1
  ie18: -1 x1 -1 x3 +1 x8 >= 1
  ie19: -1 x3 +1 x12 >= 1
  ie20: -1 x1 -1 x4 +1 x6 >= -1
END

0.000 sec
[9 ph, smt 0.119, isect 0.144, ins 0.004, tot 0.349]

Running SMT solver (12)... 0.002 sec
Found point (-7/6,2/3,-11/6,0,0,-2,1/6,5/6,-1/6,-1/6,-1/6,5/6)
Checking... Minimizing (8,52)... (8,18), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x6 +1 x12 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x1 -1 x4 +1 x10 >= -1
  ie6: -1 x6 +1 x8 >= 2
  ie7: +1 x12 >= -2
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x9 >= -2
  ie10: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie11: -1 x6 +1 x11 >= 1
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x3 +1 x12 >= 1
  ie17: -1 x1 -1 x4 +1 x6 >= -1
END

0.002 sec
[9 ph, smt 0.121, isect 0.156, ins 0.006, tot 0.370]

Running SMT solver (13)... 0.003 sec
Found point (-9/8,13/16,-15/8,1/16,3/16,-2,-1/16,15/16,-1/16,1/16,-1/16,1)
Checking... Minimizing (9,52)... (8,21), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: -1 x6 +1 x7 +1 x10 = 2
  eq3: +1 x8 -1 x9 = 1
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x6 +1 x12 >= -1
  ie6: -1 x1 -1 x4 +1 x10 >= -1
  ie7: -1 x6 +1 x8 >= 2
  ie8: +1 x12 >= -2
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x8 +1 x12 >= 0
  ie12: -1 x6 +1 x11 >= 1
  ie13: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie14: +1 x3 -1 x6 >= 0
  ie15: -1 x1 -1 x4 +1 x9 >= 1
  ie16: -1 x1 +1 x3 -1 x4 >= -1
  ie17: -1 x3 +1 x12 >= 1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
  ie20: +1 x7 >= -2
END

0.000 sec
[10 ph, smt 0.124, isect 0.170, ins 0.006, tot 0.390]

Running SMT solver (14)... 0.003 sec
Found point (-7/6,1/6,-11/6,-5/6,-1,-2,7/6,1/3,-2/3,-7/6,-1,1/3)
Checking... Minimizing (8,50)... (8,20), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x1 -1 x5 +1 x6 >= 0
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x8 >= 2
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x5 +1 x11 >= 1
  ie15: +1 x2 +1 x5 -1 x9 >= -1
  ie16: -1 x1 -1 x4 +1 x9 >= 1
  ie17: -1 x1 +1 x3 -1 x4 >= -1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
END

0.002 sec
[10 ph, smt 0.127, isect 0.183, ins 0.007, tot 0.413]

Running SMT solver (15)... 0.003 sec
Found point (-15/14,-3/14,-27/14,-8/7,-8/7,-2,15/14,-1/7,-8/7,-17/14,-17/14,-1/7)
Checking... Minimizing (9,50)... (9,16), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x10 +1 x11 = 0
  eq4: -1 x6 = 2
  eq5: -1 x7 +1 x8 -1 x10 = 0
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x10 +1 x12 >= -2
  ie4: +1 x12 >= -2
  ie5: -1 x1 -1 x5 +1 x12 >= 1
  ie6: -1 x3 +1 x4 >= 0
  ie7: +1 x2 >= -2
  ie8: +1 x6 -1 x10 >= -1
  ie9: +1 x9 >= -2
  ie10: -1 x1 +1 x7 >= 2
  ie11: +1 x5 >= -2
  ie12: +1 x6 -1 x8 >= -2
  ie13: +1 x5 +1 x7 -1 x8 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x1 >= -2
END

0.000 sec
[11 ph, smt 0.130, isect 0.198, ins 0.007, tot 0.435]

Running SMT solver (16)... 0.003 sec
Found point (-1,-1/10,-2,-6/5,-13/10,-2,6/5,-1/10,-11/10,-13/10,-6/5,-1/10)
Checking... Minimizing (8,50)... (8,22), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x1 -1 x5 +1 x6 >= 0
  ie4: -1 x1 -1 x4 +1 x10 >= -1
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: +1 x7 >= 1
  ie13: +1 x4 >= -2
  ie14: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie15: -1 x1 -1 x5 +1 x11 >= 1
  ie16: +1 x2 +1 x5 -1 x9 >= -1
  ie17: +1 x5 >= -2
  ie18: +1 x6 -1 x8 >= -2
  ie19: +1 x5 +1 x7 -1 x8 >= 0
  ie20: -1 x1 -1 x4 +1 x9 >= 1
  ie21: +1 x1 >= -2
END

0.003 sec
[10 ph, smt 0.133, isect 0.213, ins 0.010, tot 0.460]

Running SMT solver (17)... 0.006 sec
Found point (-5/12,-1,-17/12,-2,31/12,-17/12,-2,-1,-2,19/12,-17/12,1)
Checking... Minimizing (11,51)... (10,12), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 1
  eq5: -1 x2 -1 x3 +1 x6 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: +1 x4 >= -2
  ie5: +1 x1 +1 x3 -1 x6 >= -1
  ie6: -1 x3 +1 x10 >= 1
  ie7: -1 x3 +1 x4 >= -1
  ie8: -1 x3 +1 x12 >= 1
  ie9: -1 x1 +1 x2 -1 x4 >= -1
  ie10: +1 x2 >= -2
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

0.000 sec
[11 ph, smt 0.139, isect 0.224, ins 0.010, tot 0.481]

Running SMT solver (18)... 0.004 sec
Found point (-7/6,-7/6,-11/6,-11/6,1,-2,-5/6,-1,-2,-1/6,-2,-1/6)
Checking... Minimizing (12,52)... (10,10), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x2 -1 x4 +1 x9 = 1
  eq2: -1 x6 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 1
  eq4: -1 x2 -1 x3 +1 x6 = 1
  eq5: +1 x8 -1 x9 = 1
  eq6: -1 x7 +1 x8 -1 x12 = 0
  eq7: -1 x2 -1 x4 +1 x11 = 1
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: +1 x4 >= -2
  ie6: -1 x3 +1 x10 >= 1
  ie7: +1 x7 >= -2
  ie8: +1 x2 >= -2
  ie9: -1 x8 +1 x12 >= 0
END

0.000 sec
[12 ph, smt 0.143, isect 0.236, ins 0.011, tot 0.501]

Running SMT solver (19)... 0.004 sec
Found point (-1/8,-5/4,-3/4,-7/4,13/4,-7/8,-2,-1,-2,17/8,-7/8,1)
Checking... Minimizing (11,51)... (10,12), 0.010 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x2 -1 x3 +1 x8 = 1
  eq5: -1 x6 +1 x7 +1 x10 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x1 -1 x3 +1 x5 >= 0
  ie3: +1 x4 >= -2
  ie4: -1 x1 -1 x4 +1 x9 >= -1
  ie5: -1 x3 +1 x10 >= 1
  ie6: +1 x6 >= -2
  ie7: -1 x6 +1 x8 >= -2
  ie8: -1 x2 -1 x3 +1 x6 >= 1
  ie9: -1 x3 +1 x12 >= 1
  ie10: -1 x1 +1 x2 -1 x4 >= -1
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

0.002 sec
[12 ph, smt 0.147, isect 0.246, ins 0.012, tot 0.521]

Running SMT solver (20)... 0.005 sec
Found point (-1,-7/8,-2,-15/8,9/8,-2,-9/8,-7/8,-15/8,1/4,-15/8,1/4)
Checking... Minimizing (9,52)... (8,18), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: +1 x8 -1 x9 = 1
  eq3: -1 x7 +1 x8 -1 x12 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: -1 x3 +1 x10 >= 1
  ie13: +1 x7 -1 x8 +1 x10 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

0.002 sec
[12 ph, smt 0.152, isect 0.260, ins 0.014, tot 0.546]

Running SMT solver (21)... 0.005 sec
No more solutions found



Solution:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: +1 x8 -1 x9 = 1
  eq3: -1 x7 +1 x8 -1 x12 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: -1 x3 +1 x10 >= 1
  ie13: +1 x7 -1 x8 +1 x10 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x2 -1 x3 +1 x8 = 1
  eq5: -1 x6 +1 x7 +1 x10 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x1 -1 x3 +1 x5 >= 0
  ie3: +1 x4 >= -2
  ie4: -1 x1 -1 x4 +1 x9 >= -1
  ie5: -1 x3 +1 x10 >= 1
  ie6: +1 x6 >= -2
  ie7: -1 x6 +1 x8 >= -2
  ie8: -1 x2 -1 x3 +1 x6 >= 1
  ie9: -1 x3 +1 x12 >= 1
  ie10: -1 x1 +1 x2 -1 x4 >= -1
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 1
  eq5: -1 x2 -1 x3 +1 x6 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: +1 x4 >= -2
  ie5: +1 x1 +1 x3 -1 x6 >= -1
  ie6: -1 x3 +1 x10 >= 1
  ie7: -1 x3 +1 x4 >= -1
  ie8: -1 x3 +1 x12 >= 1
  ie9: -1 x1 +1 x2 -1 x4 >= -1
  ie10: +1 x2 >= -2
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x1 -1 x5 +1 x6 >= 0
  ie4: -1 x1 -1 x4 +1 x10 >= -1
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: +1 x7 >= 1
  ie13: +1 x4 >= -2
  ie14: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie15: -1 x1 -1 x5 +1 x11 >= 1
  ie16: +1 x2 +1 x5 -1 x9 >= -1
  ie17: +1 x5 >= -2
  ie18: +1 x6 -1 x8 >= -2
  ie19: +1 x5 +1 x7 -1 x8 >= 0
  ie20: -1 x1 -1 x4 +1 x9 >= 1
  ie21: +1 x1 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x1 -1 x5 +1 x6 >= 0
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x8 >= 2
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x5 +1 x11 >= 1
  ie15: +1 x2 +1 x5 -1 x9 >= -1
  ie16: -1 x1 -1 x4 +1 x9 >= 1
  ie17: -1 x1 +1 x3 -1 x4 >= -1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: -1 x6 +1 x7 +1 x10 = 2
  eq3: +1 x8 -1 x9 = 1
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x6 +1 x12 >= -1
  ie6: -1 x1 -1 x4 +1 x10 >= -1
  ie7: -1 x6 +1 x8 >= 2
  ie8: +1 x12 >= -2
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x8 +1 x12 >= 0
  ie12: -1 x6 +1 x11 >= 1
  ie13: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie14: +1 x3 -1 x6 >= 0
  ie15: -1 x1 -1 x4 +1 x9 >= 1
  ie16: -1 x1 +1 x3 -1 x4 >= -1
  ie17: -1 x3 +1 x12 >= 1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
  ie20: +1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 6

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x6 +1 x12 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x1 -1 x4 +1 x10 >= -1
  ie6: -1 x6 +1 x8 >= 2
  ie7: +1 x12 >= -2
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x9 >= -2
  ie10: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie11: -1 x6 +1 x11 >= 1
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x3 +1 x12 >= 1
  ie17: -1 x1 -1 x4 +1 x6 >= -1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 7

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x12 >= -1
  ie5: -1 x6 +1 x8 >= 2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie13: -1 x6 +1 x11 >= 1
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x1 -1 x4 +1 x6 >= -1
  ie17: +1 x1 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 8

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 >= -2
  ie5: +1 x6 -1 x7 -1 x10 >= -2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 9

MAXIMIZE
Subject To
  eq0: -1 x6 = 2
  eq1: +1 x8 -1 x9 = 1
  eq2: -1 x7 +1 x8 -1 x12 = 0
  eq3: +1 x7 +1 x10 -1 x11 = 1
  eq4: -1 x1 -1 x3 +1 x6 = 1
  eq5: -1 x1 -1 x4 +1 x11 = 1
  eq6: +1 x5 +1 x7 -1 x12 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x5 -1 x7 +1 x10 >= -1
  ie4: +1 x2 +1 x4 -1 x8 >= -2
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: +1 x4 -1 x5 -1 x7 >= -3
  ie13: +1 x2 -1 x7 >= -2
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 -1 x5 -1 x7 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 10

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x3 +1 x8 = 2
  eq2: -1 x6 = 2
  eq3: -1 x7 +1 x8 -1 x10 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: +1 x5 +1 x7 -1 x10 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x6 -1 x7 -1 x10 >= -2
  ie5: +1 x12 >= -2
  ie6: -1 x3 +1 x4 >= 0
  ie7: +1 x1 +1 x5 -1 x7 -1 x10 >= -2
  ie8: +1 x4 >= -2
  ie9: +1 x4 -1 x5 -1 x7 >= -3
  ie10: +1 x2 -1 x7 >= -2
  ie11: +1 x7 -1 x8 +1 x12 >= 0
  ie12: +1 x3 -1 x5 -1 x7 >= -2
  ie13: +1 x3 >= -2
  ie14: +1 x1 >= -2
  ie15: +1 x1 -1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 11

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: +1 x7 -1 x8 +1 x12 >= 0
  ie6: -1 x3 +1 x10 >= 1
  ie7: +1 x4 >= -2
  ie8: +1 x6 -1 x7 -1 x10 >= -2
  ie9: +1 x12 >= -2
  ie10: -1 x3 +1 x4 >= 0
  ie11: +1 x3 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end



12 polyhedra, 21 rounds, smt 0.157 sec, isect 0.260 sec, insert 0.014 sec, total 0.554 sec
Preprocessing 0.000 sec, super total 0.554 sec
Comparing... 0.110 sec.  Solution matches input.
