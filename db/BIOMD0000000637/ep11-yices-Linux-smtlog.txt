
----------------------------------------------------------------------
This is SMTcut v4.6.4 by Christoph Lueders -- http://wrogn.com
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] x86_64
pysmt 0.9.0, gmpy2 2.0.8
Datetime: 2020-06-23T20:10:56+02:00
Command line: BIOMD0000000637 --yices
Flags: no-ppone no-ppsmt no-pp2x2 dump justone solver=yices


Model: BIOMD0000000637
Logfile: db/BIOMD0000000637/ep11-yices-Linux-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^12.3 in 15 bags
Bag sizes: 12 12 9 12 16 3 9 2 3 8 4 6 4 9 8

Minimizing (0,0)... (0,0)


Running SMT solver (1)... 0.007 sec
Found point (-2,-2,-1,-1,-1,-2,1,-1,-2,-2,-2,-1)
Checking... Minimizing (13,49)... (11,5), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x1 -1 x5 +1 x11 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 1
  eq4: -1 x2 -1 x3 +1 x6 = 1
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x5 +1 x9 = 1
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x2 -1 x5 +1 x10 = 1
  eq10: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x5 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x2 >= -2
  ie4: +1 x5 >= -2
END

0.000 sec
[1 ph, smt 0.007, isect 0.015, ins 0.000, tot 0.062]

Running SMT solver (2)... 0.002 sec
Found point (-4/3,-2,-1/3,-1,11/3,-4/3,-2,-1,-2,5/3,-4/3,1)
Checking... Minimizing (11,51)... (10,12), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 1
  eq5: -1 x2 -1 x3 +1 x6 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: +1 x4 >= -2
  ie5: +1 x1 +1 x3 -1 x6 >= -1
  ie6: -1 x3 +1 x10 >= 1
  ie7: -1 x3 +1 x4 >= -1
  ie8: -1 x3 +1 x12 >= 1
  ie9: -1 x1 +1 x2 -1 x4 >= -1
  ie10: +1 x2 >= -2
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

0.000 sec
[2 ph, smt 0.009, isect 0.029, ins 0.000, tot 0.082]

Running SMT solver (3)... 0.003 sec
Found point (1,-1,-1,-2,3,0,-2,-1,-2,3,0,1)
Checking... Minimizing (11,51)... (10,12), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x2 -1 x3 +1 x8 = 1
  eq5: -1 x6 +1 x7 +1 x10 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x1 -1 x3 +1 x5 >= 0
  ie3: +1 x4 >= -2
  ie4: -1 x1 -1 x4 +1 x9 >= -1
  ie5: -1 x3 +1 x10 >= 1
  ie6: +1 x6 >= -2
  ie7: -1 x6 +1 x8 >= -2
  ie8: -1 x2 -1 x3 +1 x6 >= 1
  ie9: -1 x3 +1 x12 >= 1
  ie10: -1 x1 +1 x2 -1 x4 >= -1
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

0.000 sec
[3 ph, smt 0.012, isect 0.040, ins 0.000, tot 0.099]

Running SMT solver (4)... 0.001 sec
Found point (-1,-1,-2,-2,1/2,-2,-1/2,-1,-2,-1/2,-2,-1/2)
Checking... Minimizing (12,52)... (10,10), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x2 -1 x4 +1 x9 = 1
  eq2: -1 x6 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 1
  eq4: -1 x2 -1 x3 +1 x6 = 1
  eq5: +1 x8 -1 x9 = 1
  eq6: -1 x7 +1 x8 -1 x12 = 0
  eq7: -1 x2 -1 x4 +1 x11 = 1
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: +1 x4 >= -2
  ie6: -1 x3 +1 x10 >= 1
  ie7: +1 x7 >= -2
  ie8: +1 x2 >= -2
  ie9: -1 x8 +1 x12 >= 0
END

0.000 sec
[4 ph, smt 0.014, isect 0.052, ins 0.000, tot 0.115]

Running SMT solver (5)... 0.001 sec
Found point (-1,-1,-2,-2,-1,-2,1/2,-1,-2,-3/2,-2,-1)
Checking... Minimizing (12,52)... (10,9), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x4 +1 x9 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 1
  eq4: -1 x2 -1 x3 +1 x6 = 1
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x4 +1 x11 = 1
  eq7: +1 x7 +1 x10 -1 x11 = 1
  eq8: +1 x5 +1 x7 -1 x10 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x4 -1 x5 -1 x7 >= -3
  ie5: +1 x2 -1 x7 >= -2
  ie6: -1 x4 +1 x5 >= 0
  ie7: +1 x7 -1 x8 +1 x12 >= 0
  ie8: +1 x4 >= -2
END

0.000 sec
[5 ph, smt 0.014, isect 0.063, ins 0.000, tot 0.130]

Running SMT solver (6)... 0.001 sec
Found point (-1,-1/2,-2,-2,-2,-2,3/2,-1/2,-3/2,-2,-2,-1/2)
Checking... Minimizing (11,50)... (10,11), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x1 -1 x5 +1 x11 = 1
  eq4: -1 x6 = 2
  eq5: -1 x7 +1 x8 -1 x10 = 0
  eq6: -1 x2 -1 x5 +1 x9 = 1
  eq7: -1 x11 = 2
  eq8: -1 x1 -1 x3 +1 x6 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x5 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x1 >= -2
  ie4: -1 x10 +1 x12 >= -2
  ie5: +1 x12 >= -2
  ie6: +1 x5 >= -2
  ie7: +1 x9 >= -2
  ie8: +1 x6 -1 x8 >= -2
  ie9: +1 x2 >= -2
  ie10: -1 x1 +1 x7 >= 2
END

0.001 sec
[5 ph, smt 0.016, isect 0.075, ins 0.002, tot 0.148]

Running SMT solver (7)... 0.002 sec
Found point (-5/3,-5/3,-4/3,-4/3,2/3,-2,-1/3,-1,-2,-2/3,-2,-2/3)
Checking... Minimizing (12,52)... (10,12), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x2 -1 x4 +1 x9 = 1
  eq1: -1 x6 = 2
  eq2: -1 x6 +1 x7 +1 x10 = 1
  eq3: -1 x2 -1 x3 +1 x6 = 1
  eq4: +1 x8 -1 x9 = 1
  eq5: -1 x7 +1 x8 -1 x12 = 0
  eq6: -1 x2 -1 x4 +1 x11 = 1
  eq7: +1 x7 +1 x10 -1 x11 = 1
  eq8: -1 x1 -1 x4 +1 x11 = 1
  eq9: +1 x5 +1 x7 -1 x12 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x4 -1 x5 -1 x7 >= -3
  ie5: +1 x2 -1 x7 >= -2
  ie6: -1 x4 +1 x5 >= 0
  ie7: +1 x4 >= -2
  ie8: +1 x7 >= -2
  ie9: +1 x2 >= -2
  ie10: +1 x3 -1 x5 -1 x7 >= -2
  ie11: -1 x8 +1 x12 >= 0
END

0.000 sec
[6 ph, smt 0.018, isect 0.086, ins 0.002, tot 0.165]

Running SMT solver (8)... 0.001 sec
Found point (-3/2,0,-3/2,-3/2,-3/2,-2,2,1/2,-1/2,-2,-2,1/2)
Checking... Minimizing (11,50)... (10,10), 0.010 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x1 -1 x5 +1 x11 = 1
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x5 +1 x9 = 1
  eq7: -1 x11 = 2
  eq8: -1 x1 -1 x3 +1 x6 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x5 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x1 >= -2
  ie3: -1 x10 +1 x12 >= -2
  ie4: -1 x6 +1 x8 >= 2
  ie5: +1 x12 >= -2
  ie6: +1 x3 -1 x6 >= 0
  ie7: +1 x9 >= -2
  ie8: +1 x2 >= -2
  ie9: -1 x1 +1 x7 >= 2
END

0.000 sec
[7 ph, smt 0.019, isect 0.097, ins 0.002, tot 0.180]

Running SMT solver (9)... 0.002 sec
Found point (-2,-2,-1,-1,-1/3,-2,1/3,-1,-2,-4/3,-2,-1)
Checking... Minimizing (12,50)... (10,11), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x4 +1 x9 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 1
  eq4: -1 x2 -1 x3 +1 x6 = 1
  eq5: -1 x6 = 2
  eq6: -1 x2 -1 x4 +1 x11 = 1
  eq7: +1 x7 +1 x10 -1 x11 = 1
  eq8: -1 x2 -1 x5 +1 x10 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x7 -1 x8 +1 x12 >= 0
  ie5: -1 x2 +1 x7 >= 2
  ie6: +1 x5 >= -2
  ie7: -1 x2 +1 x4 -1 x5 >= -1
  ie8: -1 x2 +1 x3 -1 x5 >= 0
  ie9: +1 x2 >= -2
  ie10: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

0.000 sec
[8 ph, smt 0.020, isect 0.107, ins 0.002, tot 0.195]

Running SMT solver (10)... 0.005 sec
Found point (-7/6,-1/3,-11/6,-1,-1/3,-2,1/2,-1/6,-7/6,-2/3,-7/6,-1/6)
Checking... Minimizing (9,52)... (9,12), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: +1 x7 -1 x8 +1 x12 >= 0
  ie6: -1 x3 +1 x10 >= 1
  ie7: +1 x4 >= -2
  ie8: +1 x6 -1 x7 -1 x10 >= -2
  ie9: +1 x12 >= -2
  ie10: -1 x3 +1 x4 >= 0
  ie11: +1 x3 >= -2
END

0.000 sec
[9 ph, smt 0.025, isect 0.120, ins 0.002, tot 0.217]

Running SMT solver (11)... 0.001 sec
Found point (-4/3,-1/3,-5/3,-2/3,2/3,-2,-1/3,0,-1,1/3,-1,1/3)
Checking... Minimizing (9,52)... (8,18), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: +1 x8 -1 x9 = 1
  eq3: -1 x7 +1 x8 -1 x12 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: -1 x3 +1 x10 >= 1
  ie13: +1 x7 -1 x8 +1 x10 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

0.002 sec
[9 ph, smt 0.026, isect 0.135, ins 0.004, tot 0.237]

Running SMT solver (12)... 0.001 sec
Found point (-6/5,-4/5,-9/5,-7/5,-6/5,-2,4/5,-3/5,-8/5,-7/5,-8/5,-3/5)
Checking... Minimizing (9,52)... (9,16), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x3 +1 x8 = 2
  eq2: -1 x6 = 2
  eq3: -1 x7 +1 x8 -1 x10 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: +1 x5 +1 x7 -1 x10 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x6 -1 x7 -1 x10 >= -2
  ie5: +1 x12 >= -2
  ie6: -1 x3 +1 x4 >= 0
  ie7: +1 x1 +1 x5 -1 x7 -1 x10 >= -2
  ie8: +1 x4 >= -2
  ie9: +1 x4 -1 x5 -1 x7 >= -3
  ie10: +1 x2 -1 x7 >= -2
  ie11: +1 x7 -1 x8 +1 x12 >= 0
  ie12: +1 x3 -1 x5 -1 x7 >= -2
  ie13: +1 x3 >= -2
  ie14: +1 x1 >= -2
  ie15: +1 x1 -1 x7 >= -2
END

0.002 sec
[9 ph, smt 0.027, isect 0.149, ins 0.005, tot 0.257]

Running SMT solver (13)... 0.001 sec
Found point (-1,-1/4,-2,-5/4,-3/2,-2,5/4,-1/4,-5/4,-3/2,-5/4,-1/4)
Checking... Minimizing (8,50)... (8,22), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x1 -1 x5 +1 x6 >= 0
  ie4: -1 x1 -1 x4 +1 x10 >= -1
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: +1 x7 >= 1
  ie13: +1 x4 >= -2
  ie14: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie15: -1 x1 -1 x5 +1 x11 >= 1
  ie16: +1 x2 +1 x5 -1 x9 >= -1
  ie17: +1 x5 >= -2
  ie18: +1 x6 -1 x8 >= -2
  ie19: +1 x5 +1 x7 -1 x8 >= 0
  ie20: -1 x1 -1 x4 +1 x9 >= 1
  ie21: +1 x1 >= -2
END

0.002 sec
[9 ph, smt 0.028, isect 0.165, ins 0.007, tot 0.278]

Running SMT solver (14)... 0.001 sec
Found point (-2,-5/3,-1,-2/3,0,-2,1/3,-2/3,-5/3,-1,-5/3,-2/3)
Checking... Minimizing (9,50)... (9,12), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 >= -2
  ie5: +1 x6 -1 x7 -1 x10 >= -2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
END

0.003 sec
[9 ph, smt 0.028, isect 0.177, ins 0.010, tot 0.297]

Running SMT solver (15)... 0.001 sec
Found point (-3/2,0,-3/2,-1/2,-1/2,-2,1,1/2,-1/2,-1,-1,1/2)
Checking... Minimizing (9,50)... (9,11), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie4: +1 x1 >= -2
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x1 -1 x4 +1 x9 >= 1
  ie9: +1 x2 >= -2
  ie10: -1 x1 +1 x7 >= 2
END

0.000 sec
[10 ph, smt 0.030, isect 0.189, ins 0.010, tot 0.313]

Running SMT solver (16)... 0.001 sec
Found point (-4/3,1/3,-5/3,-2/3,-1,-2,4/3,2/3,-1/3,-4/3,-1,2/3)
Checking... Minimizing (8,50)... (8,20), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x1 -1 x5 +1 x6 >= 0
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x8 >= 2
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x5 +1 x11 >= 1
  ie15: +1 x2 +1 x5 -1 x9 >= -1
  ie16: -1 x1 -1 x4 +1 x9 >= 1
  ie17: -1 x1 +1 x3 -1 x4 >= -1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
END

0.003 sec
[10 ph, smt 0.031, isect 0.203, ins 0.013, tot 0.334]

Running SMT solver (17)... 0.001 sec
Found point (-1,1/2,-2,-1,0,-2,0,1/2,-1/2,0,-1,1/2)
Checking... Minimizing (9,52)... (9,14), 0.012 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x6 = 2
  eq3: -1 x6 +1 x7 +1 x10 = 2
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: -1 x6 +1 x8 >= 2
  ie5: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie6: +1 x12 >= -2
  ie7: +1 x3 -1 x6 >= 0
  ie8: +1 x8 -1 x9 >= 1
  ie9: -1 x1 -1 x4 +1 x9 >= 1
  ie10: -1 x3 +1 x12 >= 1
  ie11: +1 x2 +1 x3 -1 x8 >= -2
  ie12: +1 x8 -1 x9 >= -1
  ie13: +1 x9 >= -2
END

0.000 sec
[11 ph, smt 0.032, isect 0.216, ins 0.014, tot 0.351]

Running SMT solver (18)... 0.000 sec
Found point (-2,-2/3,-1,0,8/3,-2,-5/3,1/3,-2/3,5/3,-1,2)
Checking... Minimizing (10,52)... (9,13), 0.013 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: -1 x6 +1 x7 +1 x10 = 2
  eq3: +1 x8 -1 x9 = 1
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: +1 x1 >= -2
  ie5: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie6: +1 x12 >= -2
  ie7: +1 x3 -1 x6 >= 0
  ie8: +1 x7 >= -2
  ie9: -1 x1 -1 x4 +1 x9 >= 1
  ie10: +1 x2 >= -2
  ie11: +1 x9 >= -2
  ie12: -1 x8 +1 x12 >= 0
END

0.003 sec
[11 ph, smt 0.032, isect 0.228, ins 0.017, tot 0.371]

Running SMT solver (19)... 0.001 sec
Found point (-2,-2/3,-1,1/3,8/3,-2,-5/3,1/3,-2/3,5/3,-2/3,2)
Checking... Minimizing (9,52)... (8,21), 0.016 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: -1 x6 +1 x7 +1 x10 = 2
  eq3: +1 x8 -1 x9 = 1
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x6 +1 x12 >= -1
  ie6: -1 x1 -1 x4 +1 x10 >= -1
  ie7: -1 x6 +1 x8 >= 2
  ie8: +1 x12 >= -2
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x8 +1 x12 >= 0
  ie12: -1 x6 +1 x11 >= 1
  ie13: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie14: +1 x3 -1 x6 >= 0
  ie15: -1 x1 -1 x4 +1 x9 >= 1
  ie16: -1 x1 +1 x3 -1 x4 >= -1
  ie17: -1 x3 +1 x12 >= 1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
  ie20: +1 x7 >= -2
END

0.003 sec
[11 ph, smt 0.033, isect 0.244, ins 0.020, tot 0.393]

Running SMT solver (20)... 0.001 sec
Found point (-4/3,0,-5/3,-2/3,0,-2,1/3,1/3,-2/3,-1/3,-1,1/3)
Checking... Minimizing (9,52)... (9,10), 0.011 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie5: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie6: +1 x12 >= -2
  ie7: +1 x3 -1 x6 >= 0
  ie8: -1 x1 -1 x4 +1 x9 >= 1
  ie9: +1 x9 >= -2
END

0.000 sec
[12 ph, smt 0.033, isect 0.256, ins 0.020, tot 0.409]

Running SMT solver (21)... 0.000 sec
Found point (-2,-2/3,-1,1/3,2/3,-2,1/3,1/3,-2/3,-1/3,-2/3,1/3)
Checking... Minimizing (8,50)... (8,18), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x12 >= -1
  ie5: -1 x6 +1 x8 >= 2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie13: -1 x6 +1 x11 >= 1
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x1 -1 x4 +1 x6 >= -1
  ie17: +1 x1 >= -2
END

0.002 sec
[12 ph, smt 0.034, isect 0.270, ins 0.022, tot 0.428]

Running SMT solver (22)... 0.001 sec
Found point (-8/5,-2/5,-7/5,-1/5,2/5,-2,1/5,1/5,-4/5,-1/5,-4/5,1/5)
Checking... Minimizing (8,52)... (8,18), 0.014 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x6 +1 x12 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x1 -1 x4 +1 x10 >= -1
  ie6: -1 x6 +1 x8 >= 2
  ie7: +1 x12 >= -2
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x9 >= -2
  ie10: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie11: -1 x6 +1 x11 >= 1
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x3 +1 x12 >= 1
  ie17: -1 x1 -1 x4 +1 x6 >= -1
END

0.002 sec
[12 ph, smt 0.035, isect 0.284, ins 0.023, tot 0.447]

Running SMT solver (23)... 0.001 sec
Found point (-2,-3/2,-1,-1/2,1,-2,-1/4,-1/2,-3/2,-1/4,-3/2,-1/4)
Checking... Minimizing (9,52)... (8,18), 0.015 sec
Inserting... 

MAXIMIZE
Subject To
  eq0: -1 x6 = 2
  eq1: +1 x8 -1 x9 = 1
  eq2: -1 x7 +1 x8 -1 x12 = 0
  eq3: +1 x7 +1 x10 -1 x11 = 1
  eq4: -1 x1 -1 x3 +1 x6 = 1
  eq5: -1 x1 -1 x4 +1 x11 = 1
  eq6: +1 x5 +1 x7 -1 x12 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x5 -1 x7 +1 x10 >= -1
  ie4: +1 x2 +1 x4 -1 x8 >= -2
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: +1 x4 -1 x5 -1 x7 >= -3
  ie13: +1 x2 -1 x7 >= -2
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 -1 x5 -1 x7 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

0.002 sec
[12 ph, smt 0.035, isect 0.298, ins 0.025, tot 0.467]

Running SMT solver (24)... 0.000 sec
No more solutions found



Solution:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

MAXIMIZE
Subject To
  eq0: -1 x6 = 2
  eq1: +1 x8 -1 x9 = 1
  eq2: -1 x7 +1 x8 -1 x12 = 0
  eq3: +1 x7 +1 x10 -1 x11 = 1
  eq4: -1 x1 -1 x3 +1 x6 = 1
  eq5: -1 x1 -1 x4 +1 x11 = 1
  eq6: +1 x5 +1 x7 -1 x12 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x5 -1 x7 +1 x10 >= -1
  ie4: +1 x2 +1 x4 -1 x8 >= -2
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: +1 x4 -1 x5 -1 x7 >= -3
  ie13: +1 x2 -1 x7 >= -2
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 -1 x5 -1 x7 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x6 +1 x12 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x1 -1 x4 +1 x10 >= -1
  ie6: -1 x6 +1 x8 >= 2
  ie7: +1 x12 >= -2
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x9 >= -2
  ie10: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie11: -1 x6 +1 x11 >= 1
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x3 +1 x12 >= 1
  ie17: -1 x1 -1 x4 +1 x6 >= -1
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x12 >= -1
  ie5: -1 x6 +1 x8 >= 2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x2 -1 x3 +1 x7 +1 x12 >= 2
  ie13: -1 x6 +1 x11 >= 1
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: -1 x1 +1 x3 -1 x4 >= -1
  ie16: -1 x1 -1 x4 +1 x6 >= -1
  ie17: +1 x1 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: -1 x6 +1 x7 +1 x10 = 2
  eq3: +1 x8 -1 x9 = 1
  eq4: -1 x7 +1 x8 -1 x12 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x7 >= -1
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x6 +1 x12 >= -1
  ie6: -1 x1 -1 x4 +1 x10 >= -1
  ie7: -1 x6 +1 x8 >= 2
  ie8: +1 x12 >= -2
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x8 +1 x12 >= 0
  ie12: -1 x6 +1 x11 >= 1
  ie13: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie14: +1 x3 -1 x6 >= 0
  ie15: -1 x1 -1 x4 +1 x9 >= 1
  ie16: -1 x1 +1 x3 -1 x4 >= -1
  ie17: -1 x3 +1 x12 >= 1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
  ie20: +1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 2
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: -1 x1 -1 x5 +1 x6 >= 0
  ie3: -1 x1 -1 x4 +1 x10 >= -1
  ie4: -1 x6 +1 x8 >= 2
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie13: +1 x3 -1 x6 >= 0
  ie14: -1 x1 -1 x5 +1 x11 >= 1
  ie15: +1 x2 +1 x5 -1 x9 >= -1
  ie16: -1 x1 -1 x4 +1 x9 >= 1
  ie17: -1 x1 +1 x3 -1 x4 >= -1
  ie18: -1 x1 -1 x4 +1 x6 >= -1
  ie19: +1 x1 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 >= -2
  ie5: +1 x6 -1 x7 -1 x10 >= -2
  ie6: +1 x12 >= -2
  ie7: -1 x1 +1 x4 -1 x5 >= -1
  ie8: -1 x1 -1 x5 +1 x12 >= 1
  ie9: -1 x3 +1 x4 >= 0
  ie10: +1 x2 >= -2
  ie11: -1 x1 +1 x7 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 6

MAXIMIZE
Subject To
  eq0: -1 x1 -1 x5 +1 x10 = 1
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x1 -1 x5 +1 x6 >= 0
  ie4: -1 x1 -1 x4 +1 x10 >= -1
  ie5: +1 x12 >= -2
  ie6: -1 x1 +1 x4 -1 x5 >= -1
  ie7: -1 x1 -1 x5 +1 x12 >= 1
  ie8: -1 x3 +1 x4 >= 0
  ie9: +1 x2 >= -2
  ie10: +1 x9 >= -2
  ie11: -1 x1 +1 x7 >= 2
  ie12: +1 x7 >= 1
  ie13: +1 x4 >= -2
  ie14: -1 x1 -1 x3 +1 x5 +1 x7 >= 2
  ie15: -1 x1 -1 x5 +1 x11 >= 1
  ie16: +1 x2 +1 x5 -1 x9 >= -1
  ie17: +1 x5 >= -2
  ie18: +1 x6 -1 x8 >= -2
  ie19: +1 x5 +1 x7 -1 x8 >= 0
  ie20: -1 x1 -1 x4 +1 x9 >= 1
  ie21: +1 x1 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 7

MAXIMIZE
Subject To
  eq0: -1 x9 +1 x12 = 1
  eq1: -1 x2 -1 x3 +1 x8 = 2
  eq2: -1 x6 = 2
  eq3: -1 x7 +1 x8 -1 x10 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: +1 x5 +1 x7 -1 x10 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x6 -1 x7 -1 x10 >= -2
  ie5: +1 x12 >= -2
  ie6: -1 x3 +1 x4 >= 0
  ie7: +1 x1 +1 x5 -1 x7 -1 x10 >= -2
  ie8: +1 x4 >= -2
  ie9: +1 x4 -1 x5 -1 x7 >= -3
  ie10: +1 x2 -1 x7 >= -2
  ie11: +1 x7 -1 x8 +1 x12 >= 0
  ie12: +1 x3 -1 x5 -1 x7 >= -2
  ie13: +1 x3 >= -2
  ie14: +1 x1 >= -2
  ie15: +1 x1 -1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 8

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x6 = 2
  eq2: +1 x8 -1 x9 = 1
  eq3: -1 x7 +1 x8 -1 x12 = 0
  eq4: +1 x7 +1 x10 -1 x11 = 1
  eq5: -1 x1 -1 x3 +1 x6 = 1
  eq6: -1 x1 -1 x4 +1 x11 = 1
  eq7: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x2 +1 x4 -1 x8 >= -2
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: -1 x7 >= -1
  ie6: +1 x6 -1 x7 -1 x10 >= -2
  ie7: +1 x12 >= -2
  ie8: +1 x2 >= -2
  ie9: +1 x9 >= -2
  ie10: -1 x8 +1 x12 >= 0
  ie11: +1 x4 >= -2
  ie12: -1 x3 +1 x10 >= 1
  ie13: +1 x7 -1 x8 +1 x10 >= 0
  ie14: -1 x1 -1 x4 +1 x9 >= 1
  ie15: +1 x3 >= -2
  ie16: +1 x1 >= -2
  ie17: +1 x7 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 9

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x9 +1 x12 = 1
  eq2: -1 x2 -1 x3 +1 x8 = 2
  eq3: -1 x6 = 2
  eq4: -1 x7 +1 x8 -1 x10 = 0
  eq5: +1 x7 +1 x10 -1 x11 = 1
  eq6: -1 x1 -1 x3 +1 x6 = 1
  eq7: -1 x1 -1 x4 +1 x11 = 1
  eq8: -1 x2 -1 x3 +1 x9 = 1
  ie0: -1 x3 >= -1
  ie1: -1 x1 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: -1 x7 >= -1
  ie4: +1 x1 -1 x3 +1 x5 >= 0
  ie5: +1 x7 -1 x8 +1 x12 >= 0
  ie6: -1 x3 +1 x10 >= 1
  ie7: +1 x4 >= -2
  ie8: +1 x6 -1 x7 -1 x10 >= -2
  ie9: +1 x12 >= -2
  ie10: -1 x3 +1 x4 >= 0
  ie11: +1 x3 >= -2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 10

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x2 -1 x3 +1 x8 = 1
  eq5: -1 x6 +1 x7 +1 x10 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x1 -1 x3 +1 x5 >= 0
  ie3: +1 x4 >= -2
  ie4: -1 x1 -1 x4 +1 x9 >= -1
  ie5: -1 x3 +1 x10 >= 1
  ie6: +1 x6 >= -2
  ie7: -1 x6 +1 x8 >= -2
  ie8: -1 x2 -1 x3 +1 x6 >= 1
  ie9: -1 x3 +1 x12 >= 1
  ie10: -1 x1 +1 x2 -1 x4 >= -1
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 11

MAXIMIZE
Subject To
  eq0: -1 x3 +1 x5 +1 x7 = 2
  eq1: -1 x7 = 2
  eq2: -1 x2 -1 x4 +1 x9 = 1
  eq3: -1 x9 = 2
  eq4: -1 x6 +1 x7 +1 x10 = 1
  eq5: -1 x2 -1 x3 +1 x6 = 1
  eq6: +1 x8 -1 x9 = 1
  eq7: -1 x7 +1 x8 -1 x12 = 0
  eq8: +1 x7 +1 x10 -1 x11 = 1
  eq9: -1 x1 -1 x4 +1 x11 = 1
  ie0: -1 x4 >= -1
  ie1: -1 x2 >= -1
  ie2: +1 x3 -1 x8 >= -2
  ie3: +1 x1 -1 x3 +1 x5 >= 0
  ie4: +1 x4 >= -2
  ie5: +1 x1 +1 x3 -1 x6 >= -1
  ie6: -1 x3 +1 x10 >= 1
  ie7: -1 x3 +1 x4 >= -1
  ie8: -1 x3 +1 x12 >= 1
  ie9: -1 x1 +1 x2 -1 x4 >= -1
  ie10: +1 x2 >= -2
  ie11: -1 x2 -1 x3 +1 x5 +1 x7 >= 2
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end



12 polyhedra, 24 rounds, smt 0.036 sec, isect 0.298 sec, insert 0.025 sec, total 0.469 sec
Preprocessing 0.000 sec, super total 0.469 sec
Comparing... 0.110 sec.  Solution matches input.
